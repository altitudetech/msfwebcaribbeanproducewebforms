﻿
Option Explicit On
Option Strict On

Namespace com
    Public Module Conversion


        Public Function RBS_CDate(ByVal cv As Object) As DateTime
            Dim d As New DateTime
            Try
                If TypeOf cv Is System.DBNull Then

                    Return New DateTime
                End If

                d = CDate(cv)
            Catch ex As Exception
                d = New DateTime
            End Try

            Return d
        End Function

        Public Function RBS_CBool(ByVal cV As Object) As Boolean
            'This returns an Int or 0
            Dim bBool As Boolean
            Try
                If TypeOf cV Is System.DBNull Then
                    bBool = False
                    Return bBool
                End If

                bBool = CBool(cV)
            Catch ex As Exception
                bBool = False
            End Try

            Return bBool
        End Function
        Public Function RBS_CByte(ByVal cV As Object) As Byte
            'This returns an Int or 0
            Dim bByte As Byte
            Try
                bByte = CByte(cV)
            Catch ex As Exception
                bByte = 0
            End Try

            Return bByte
        End Function
        Public Function RBS_CInt(ByVal cV As Object) As Integer
            'This returns an Int or 0
            Dim iInt As Integer
            Try
                iInt = CInt(cV)
            Catch ex As Exception
                iInt = 0
            End Try

            Return iInt
        End Function
        Public Function RBS_CLng(ByVal cV As Object) As Long
            'This returns a Long or 0
            Dim lLng As Long
            Try
                lLng = CLng(cV)
            Catch ex As Exception
                lLng = 0
            End Try

            Return lLng
        End Function
        Public Function RBS_CSng(ByVal cV As Object) As Single
            'This returns a Double or 0
            Dim sSng As Single
            Try
                sSng = CSng(cV)
            Catch ex As Exception
                sSng = 0
            End Try

            Return sSng
        End Function
        Public Function RBS_CDbl(ByVal cV As Object) As Double
            'This returns a Double or 0
            Dim dDbl As Double
            Try
                dDbl = CDbl(cV)
            Catch ex As Exception
                dDbl = 0
            End Try

            Return dDbl
        End Function

        Public Function RBS_CStr(ByVal cV As Object) As String
            'This converts an object to a string using the correct format 
            Dim sV As String
            Try

                If IsNothing(cV) Then
                    sV = ""
                    Return sV
                End If
                If TypeOf cV Is System.String Then
                    sV = CStr(cV)
                    Return sV
                End If

                If TypeOf cV Is System.DBNull Then
                    sV = ""
                    Return sV
                End If
                If TypeOf cV Is System.Int32 Then
                    sV = Format(cV, "####0")
                    Return sV
                End If
                If TypeOf cV Is System.Double Then
                    sV = Format(cV, "####0.0##########################################")
                    Return sV
                End If
                If TypeOf cV Is System.DateTime Then
                    sV = Format(cV, "yyyy-MM-dd HH:mm:ss")
                    Return sV
                End If
                If TypeOf cV Is System.Int16 Then
                    sV = Format(cV, "####0")
                    Return sV
                End If
                If TypeOf cV Is System.Int64 Then
                    sV = Format(cV, "####0")
                    Return sV
                End If
                If TypeOf cV Is System.UInt32 Then
                    sV = Format(cV, "####0")
                    Return sV
                End If
                If TypeOf cV Is System.UInt16 Then
                    sV = Format(cV, "####0")
                    Return sV
                End If
                If TypeOf cV Is System.UInt64 Then
                    sV = Format(cV, "####0")
                    Return sV
                End If

                If TypeOf cV Is System.Single Then
                    sV = Format(cV, "####0.0##########################################")
                    Return sV
                End If
                If TypeOf cV Is System.Decimal Then
                    sV = Format(cV, "####0.0##########################################")
                    Return sV
                End If

                sV = CStr(cV)
                Return sV

            Catch ex As Exception
                Return ""
            End Try
        End Function
    End Module
End Namespace

