﻿

Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Configuration
Imports System.Collections

Namespace com

    Public Class ISSData
        Public pConnectString As String

        Public msSQLServerName As String
        Public msSQLUser As String
        Public msSQLPwd As String
        Public msSQLDBName As String
        Public msSQLConnect As String
        Public mbExpressEdition As Boolean = True
        Public msDescription As String = ""
        Public msConnectionType As ConnectionType '0 = SQL Server, 1 = OLEDB, 2 = ODBC 

        Enum ConnectionType
            SQL_SERVER = 0
            OLEDB = 1
            ODBC = 2
        End Enum
        Sub New()
            MyBase.New()
        End Sub
        Sub New(ByVal sDBDescription As String)
            'sets up database based on user settings
            Me.New()
            On Error GoTo ErrHand

            'loop thru my.settings.db


            msDescription = sDBDescription
            msSQLServerName = My.Settings.DatabaseServer
            msSQLUser = My.Settings.DbUserName '"sa"
            msSQLPwd = My.Settings.DbPassword
            msSQLDBName = My.Settings.DatabaseName


            SetSQLConnect()

            'setup accoringly

            Exit Sub
ErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Sub
        ''' <summary>
        ''' Loads database settings, LIS info and Backup schedule by description
        ''' </summary>
        ''' <param name="sDBDescription">Database description</param>
        Sub New(ByVal sDBDescription As String, ByVal pdsMySettings As DataSet)
            'sets up database based on user settings
            Me.New()
            On Error GoTo ErrHand

            'loop thru my.settings.db
            Dim row As DataRow
            For Each row In pdsMySettings.Tables("Databases").Rows
                If Tools.Encrypt(CStr(row("Description")), "database") = sDBDescription Then
                    msDescription = sDBDescription
                    msSQLServerName = Tools.Encrypt(CStr(row("Server")), "database")
                    msSQLUser = Tools.Encrypt(CStr(row("UID")), "database")
                    msSQLPwd = Tools.Encrypt(CStr(row("PWD")), "database")
                    msSQLDBName = Tools.Encrypt(CStr(row("Database")), "database")


                    SetSQLConnect()
                    Exit For
                End If
            Next
            'setup accoringly

            Exit Sub
ErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Sub
        Sub New(ByVal sDBDescription As String, ByVal oConnectionType As ConnectionType, ByVal pdsMySettings As DataSet)
            Me.New(sDBDescription, pdsMySettings)
            'sets up for differect connection types
            'MyBase.New()
            Me.msConnectionType = oConnectionType

        End Sub

        Public Sub SetSQLConnect()
            msSQLConnect = "Server=" & msSQLServerName & ";Database=" & msSQLDBName & ";"
            msSQLConnect &= "User ID=" & msSQLUser & ";Password=" & msSQLPwd & ";Trusted_Connection=False"
            msSQLConnect &= ";user instance=false"

            pConnectString = msSQLConnect
        End Sub
        Public Function GetConnectionString() As String

            Return pConnectString
        End Function
#Region "Multi DB"
        Public Function GetDBStringValue(ByVal sField As String, ByVal sTable As String, ByVal sWhere As String) As String

            Return GetDBStringValueSQL(sField, sTable, sWhere)

        End Function

        ''' <summary>
        ''' Executes an SQL Statement again the currnetly set database connection type/connection string
        ''' </summary>
        ''' <param name="sSQL">Statement to execute</param>
        ''' <remarks></remarks>
        Public Sub ExecSQL(ByVal sSQL As String)
            ExecSQLStatementSQL(sSQL)

        End Sub

        Public Sub ExecSQLThreaded(ByVal sSQL As String)
            Dim tSQLThread As New System.Threading.Thread(AddressOf LaunchExecSQL)
            tSQLThread.Start(sSQL)
        End Sub
        Private Sub LaunchExecSQL(ByVal oSQL As Object)
            Call ExecSQL(CStr(oSQL))
        End Sub

        Public Sub FillDataSet(ByVal dataSet As DataSet, ByVal sSQL As String, ByVal sTable As String)
            FillDataSetSQL(dataSet, sSQL, sTable)

        End Sub
        Public Sub UpdateDataSetToDB(ByVal ds As DataSet, ByVal sSQL As String, ByVal sTable As String, Optional ByVal blnBypassConcurency As Boolean = False)

            Call UpdateDataSetToDBSQL(ds, sSQL, sTable, blnBypassConcurency)

        End Sub
#End Region
#Region "SQL"

        Public Function CheckSQLConnection() As Boolean
            Try
                Dim lConn As SqlConnection = New SqlConnection(msSQLConnect & ";Connection Timeout=10")
                lConn.Open()
                lConn.Close()
                lConn.Dispose()
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Function CheckSQLConnection(ByVal oConnStr As String) As Boolean
            Try
                Dim lConn As SqlConnection = New SqlConnection(oConnStr & ";Connection Timeout=10")
                lConn.Open()
                lConn.Close()
                lConn.Dispose()
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Function ExecSQLIdentityInsert(ByVal sSQL As String) As Guid
            On Error GoTo ErrHand
            Dim lConn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(msSQLConnect)
            Dim lCMD As System.Data.SqlClient.SqlCommand = lConn.CreateCommand
            Dim iRetInt As String

            '** GET GUID
            Dim GUID As Guid? = GenerateGUID()

            sSQL = Replace(sSQL, "@ID", GUID.ToString)

            lCMD.CommandText = sSQL



            lCMD.CommandTimeout = 120
            lConn.Open()
            lCMD.ExecuteNonQuery()
            ' Dim splTableName()
            ' splTableName = Split(sSQL, " ")
            'sSQL = " SELECT IDENT_CURRENT('" & splTableName(2) & "')"
            ''sSQL = "SELECT @@IDENTITY "
            'lCMD.CommandText = sSQL
            'iRetInt = lCMD.ExecuteScalar()

            lConn.Close()
            lCMD.Dispose()
            lConn.Dispose()

            Return GUID
            Exit Function
ErrHand:
            'If ErrorClass.blnShowErrors Then
            ' ErrorClass.pfrmError.ParseException(Err.GetException())
            ' End If
            Resume Next
        End Function
        Private Function GenerateGUID() As Nullable(Of Guid)


            Dim sGUID As Guid
            sGUID = System.Guid.NewGuid
            Return sGUID


        End Function
        Public Function GetDBStringValueSQL(ByVal sField As String, ByVal sTable As String, ByVal sWhere As String) As String
            On Error GoTo ErrHand

            Dim lConn As SqlConnection = New SqlConnection(msSQLConnect)
            Dim lCMD As SqlCommand = lConn.CreateCommand
            Dim sSQL As String
            sSQL = "SELECT " & sField
            If sTable.Length > 0 Then
                sSQL &= " FROM " & sTable
            End If
            If Len(sWhere) > 0 Then
                sSQL = sSQL & " WHERE " & sWhere
            End If

            lCMD.CommandText = sSQL
            lCMD.CommandTimeout = 120
            lConn.Open()
            GetDBStringValueSQL = CStr(lCMD.ExecuteScalar())
            lConn.Close()
            lCMD.Dispose()
            lConn.Dispose()

            Exit Function
ErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next

        End Function
        Public Function GetDBStringValueSQLOnly(ByVal sSQL As String) As String
            Dim lConn As SqlConnection = New SqlConnection(msSQLConnect)
            Dim lCMD As SqlCommand = lConn.CreateCommand

            lCMD.CommandText = sSQL
            lCMD.CommandTimeout = 120
            lConn.Open()
            GetDBStringValueSQLOnly = CStr(lCMD.ExecuteScalar())
            lConn.Close()
            lCMD.Dispose()
            lConn.Dispose()

        End Function
        Private Sub ExecSQLStatementSQL(ByVal sSQL As String)
            On Error GoTo ErrHand

            Dim lConn As SqlConnection = New SqlConnection(msSQLConnect)
            Dim lCMD As SqlCommand = lConn.CreateCommand
            lCMD.CommandText = sSQL
            lCMD.CommandTimeout = 120
            lConn.Open()
            lCMD.ExecuteNonQuery()
            lConn.Close()
            lCMD.Dispose()
            lConn.Dispose()

            Exit Sub
ErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Sub
        Private Sub FillDataSetSQL(ByRef dataSet As DataSet, ByVal sSQL As String, ByVal sTable As String)
            On Error GoTo ErrHand

            'This fills a dataset with data
            Dim lConn As SqlConnection = New SqlConnection(msSQLConnect)
            Dim adapter As New SqlDataAdapter()
            lConn.Open()
            adapter.SelectCommand = New SqlCommand(sSQL, lConn)
            adapter.Fill(dataSet, sTable)
            adapter.Dispose()
            lConn.Close()
            lConn.Dispose()
            Exit Sub
ErrHand:
            'WinUIMessageBox.Show(Err.Description)
            'MsgBox(Err.Description)
            'Dispatcher.BeginInvoke(DispatcherPriority.Normal, New Action(Sub()
            'If ErrorClass.blnShowErrors Then
            '                                                                     ErrorClass.pfrmError.ParseException(Err.GetException())
            '                                                                 End If
            ' End Sub))
            Resume Next
        End Sub
        Private Sub UpdateDataSetToDBSQL(ByRef ds As DataSet, ByVal sSQL As String, ByVal sTable As String, Optional ByVal blnBypassConcurency As Boolean = False)
            On Error GoTo ErrHand

            Dim lConn As SqlConnection = New SqlConnection(msSQLConnect)
            Dim myDataAdapter As New SqlDataAdapter()
            myDataAdapter.SelectCommand = New SqlCommand(sSQL, lConn)
            Dim cb As SqlCommandBuilder = New SqlCommandBuilder(myDataAdapter)
            If blnBypassConcurency Then
                cb.ConflictOption = ConflictOption.OverwriteChanges
            End If

            lConn.Open()
            ' Without the SqlCommandBuilder this line would fail.
            If Not IsNothing(ds.GetChanges) Then
                myDataAdapter.Update(ds.GetChanges, sTable)
            End If
            ds.Dispose()
            lConn.Close()
            lConn.Dispose()
            Exit Sub
ErrHand:

            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If

            Resume Next
        End Sub
        Public Function GetDBStringValueConn(ByVal sField As String, ByVal sTable As String, ByVal sWhere As String, ByVal lConn As System.Data.SqlClient.SqlConnection) As String

            On Error GoTo ErrHand

            Dim sRet As String
            Dim lCMD As SqlCommand = lConn.CreateCommand
            Dim sSQL As String
            sSQL = "SELECT " & sField
            If sTable.Length > 0 Then
                sSQL &= " FROM " & sTable
            End If
            If Len(sWhere) > 0 Then
                sSQL = sSQL & " WHERE " & sWhere
            End If

            lCMD.CommandText = sSQL
            lCMD.CommandTimeout = 120
            sRet = CStr(lCMD.ExecuteScalar())
            lCMD.Dispose()

            Return sRet
            Exit Function
ErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next

        End Function
        Public Function ExecSQLIdentityInsertConn(ByVal sSQL As String, ByVal lConn As System.Data.SqlClient.SqlConnection) As Int32
            On Error GoTo ErrHand

            Dim lCMD As System.Data.SqlClient.SqlCommand = lConn.CreateCommand
            Dim iRetInt As Int32
            lCMD.CommandText = sSQL
            lCMD.CommandTimeout = 120

            lCMD.ExecuteNonQuery()

            sSQL = "SELECT @@IDENTITY "
            lCMD.CommandText = sSQL
            iRetInt = CInt(lCMD.ExecuteScalar())

            lCMD.Dispose()

            Return iRetInt

            Exit Function
ErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Function

        Public Function GetCurrentIdentity(ByVal sTableName As String) As Int32
            On Error GoTo ErrHand
            Dim lConn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(msSQLConnect)
            Dim lCMD As System.Data.SqlClient.SqlCommand = lConn.CreateCommand
            Dim iRetInt As Int32
            lConn.Open()
            Dim sSql As String
            sSql = " SELECT IDENT_CURRENT('" & sTableName & "')"
            'sSQL = "SELECT @@IDENTITY "
            lCMD.CommandText = sSql
            iRetInt = CInt(lCMD.ExecuteScalar())

            lConn.Close()
            lCMD.Dispose()
            lConn.Dispose()

            Return iRetInt
            Exit Function
ErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Function
        Public Sub ExecSQLConn(ByVal sSQL As String, ByVal lConn As SqlConnection)
            On Error GoTo ErrHand

            Dim lCMD As SqlCommand = lConn.CreateCommand
            lCMD.CommandText = sSQL
            lCMD.CommandTimeout = 120
            lCMD.ExecuteNonQuery()
            lCMD.Dispose()

            Exit Sub
ErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Sub
        Public Sub FillDataSetConn(ByVal dataSet As DataSet, ByVal sSQL As String, ByVal sTable As String, ByVal lConn As SqlConnection)
            On Error GoTo ErrHand

            'This fills a dataset with data
            Dim adapter As New SqlDataAdapter()
            adapter.SelectCommand = New SqlCommand(sSQL, lConn)
            adapter.Fill(dataSet, sTable)

            Exit Sub
ErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Sub
        Public Sub UpdateDataSetToDBConn(ByVal ds As DataSet, ByVal sSQL As String,
                                    ByVal sTable As String, ByVal lConn As SqlConnection)

            On Error GoTo ErrHand

            Dim myDataAdapter As New SqlDataAdapter()
            myDataAdapter.SelectCommand = New SqlCommand(sSQL, lConn)
            Dim cb As SqlCommandBuilder = New SqlCommandBuilder(myDataAdapter)

            If Not IsNothing(ds.GetChanges) Then
                myDataAdapter.Update(ds.GetChanges, sTable) ' Without the SqlCommandBuilder this line would fail.
            End If

            Exit Sub
ErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next

        End Sub

#End Region


    End Class
End Namespace

'************************ MODULE DATA ********************************
Namespace com.db

    Public Class DataFunctions
        '        Public Shared Function CreateDatatableFromDataGridView(ByVal dgv As DataGridView) As DataTable
        '            On Error GoTo ErrHand

        '            Dim dc As DataColumn
        '            Dim dt As New DataTable
        '            Dim dr As DataRow
        '            Dim iRow As Integer, iCol As Integer

        '            'create all datacolumns
        '            For iCol = 0 To dgv.ColumnCount - 1
        '                dc = New DataColumn(dgv.Columns(iCol).Name)
        '                dt.Columns.Add(dc)
        '            Next

        '            'all rows (data)
        '            For iRow = 0 To dgv.RowCount - 1

        '                dr = dt.NewRow()

        '                For iCol = 0 To dgv.ColumnCount - 1
        '                    dr(iCol) = dgv.Rows(iRow).Cells(iCol).Value
        '                Next

        '                dt.Rows.Add(dr)
        '            Next

        '            Return dt
        '            Exit Function
        'ErrHand:
        '            If ErrorClass.blnShowErrors Then
        '                ErrorClass.pfrmError.ParseException(Err.GetException())
        '            End If
        '            Resume Next
        '        End Function
        Public Shared Function RemoveDuplicatesFromDataTable(ByVal dt As DataTable, ByVal DistinctColumn As String) As DataTable
            Dim dtclone As DataTable = dt.Clone
            Dim dv As New DataView(dt)
            dv.Sort = DistinctColumn
            Dim SelOld As String = ""
            For i As Integer = 0 To dv.Count - 1
                If SelOld <> dv(i)(DistinctColumn).ToString Then
                    Dim drn As DataRow = dtclone.NewRow
                    For y As Integer = 0 To drn.ItemArray.Length - 1
                        drn(y) = dv(i)(y)
                    Next
                    SelOld = dv(i)(DistinctColumn).ToString
                    dtclone.Rows.Add(drn)
                End If
            Next
            Return dtclone
        End Function
        Public Shared Function GetFirstRecordFromDataTable(ByVal dt As DataTable, ByVal OrderColumn As String, ByVal AscendingSort As Boolean) As DataTable
            Dim dtclone As DataTable = dt.Clone
            Dim dv As New DataView(dt)
            dv.Sort = OrderColumn & CStr(IIf(AscendingSort, " ASC", " DESC"))
            Dim SelOld As String = ""
            For i As Integer = 0 To dv.Count - 1
                If SelOld <> dv(i)(OrderColumn).ToString Then
                    Dim drn As DataRow = dtclone.NewRow
                    For y As Integer = 0 To drn.ItemArray.Length - 1
                        drn(y) = dv(i)(y)
                    Next
                    SelOld = dv(i)(OrderColumn).ToString
                    dtclone.Rows.Add(drn)
                    Exit For
                End If
            Next
            Return dtclone
        End Function



        ''' <summary>
        ''' Adds or Edits a records to the internet (my.Settings) database for the database created
        ''' </summary>
        ''' <param name="blnAddNew">Add new database (no = edit)</param>
        ''' <param name="sDescription">Description of database</param>
        ''' <param name="sServer">Database Server</param>
        ''' <param name="sDB">Database Name</param>
        ''' <param name="sUID">UserID</param>
        ''' <param name="sPWD">Password</param>
        ''' <param name="iBackupFrequency">Backup Scheduler Frequency</param>
        ''' <param name="sBackupPath">Database backup path</param>
        ''' <param name="blnBackupEnabled">Enable Backup Scheduler</param>
        Public Shared Function MySettingsDatabases_Update(ByRef oMySettings As DataSet, ByVal blnAddNew As Boolean, ByVal sDescription As String, ByVal sServer As String, ByVal sDB As String, ByVal sUID As String,
            ByVal sPWD As String, ByVal iBackupFrequency As Integer, ByVal sBackupPath As String,
            ByVal blnBackupEnabled As Boolean, ByVal filename As String) As Boolean
            On Error GoTo ErrHand

            MySettingsDatabases_Validate(oMySettings)

            Dim dr As DataRow = Nothing
            Dim blnContinue As Boolean = False

            If blnAddNew Then
                'check the description name...
                blnContinue = True
                For Each dr In oMySettings.Tables("Databases").Rows
                    If Tools.Encrypt(CStr(dr("Description")), "database").ToUpper = sDescription.ToUpper Then

                        'description name exists, please rename
                        'MessageBox.Show(com.GetMessage(161), cProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)

                        Return False
                    End If
                Next
                dr = oMySettings.Tables("Databases").NewRow

            Else
                Dim iL As Integer = 0
                For iL = 0 To oMySettings.Tables("Databases").Rows.Count - 1

                    If Tools.Encrypt(CStr(oMySettings.Tables("Databases").Rows(iL)("Description")), "database") = sDescription Then
                        dr = oMySettings.Tables("Databases").Rows(iL)
                        blnContinue = True
                        Exit For
                    End If
                Next
            End If

            If blnContinue Then
                dr("Server") = Tools.Encrypt(sServer, "database")
                dr("Database") = Tools.Encrypt(sDB, "database")
                dr("UID") = Tools.Encrypt(sUID, "database")
                dr("PWD") = Tools.Encrypt(sPWD, "database")

                'dr("ExpressEdition") = bExpress
                'dr("LISConfig") = iLISConfig
                'dr("BackupScheduleEnabled") = blnBackupEnabled
                'dr("BackupSchedulePath") = tools.Encrypt(sBackupPath, "database")
                'dr("BackupScheduleFrequency") = IIf(iBackupFrequency = 0, 1, iBackupFrequency)
                If blnAddNew Then
                    dr("Description") = Tools.Encrypt(sDescription, "database")
                    oMySettings.Tables("Databases").Rows.Add(dr)
                End If

                com.db.MySettings_Save(oMySettings, filename)



            End If

            Return True
            Exit Function
ErrHand:

            Resume Next
        End Function
        Public Shared Sub MySettings_Load(ByRef oMySettings As DataSet)
            On Error GoTo ErrHand

            If IO.File.Exists("config.bio") Then
                oMySettings.ReadXml("config.bio")
            End If


            Exit Sub
ErrHand:

            Resume Next
        End Sub

        Public Shared Sub MySettings_Save(ByVal oMySettings As DataSet)
            On Error GoTo ErrHand

            If IO.File.Exists("config.bio") Then IO.File.Delete("config.bio")

            oMySettings.WriteXml("config.bio", XmlWriteMode.WriteSchema)

            Exit Sub
ErrHand:

            Resume Next
        End Sub

        ''' <summary>
        ''' Deletes a database information setup from my.settings database
        ''' </summary>
        ''' <param name="sDescription">Database description</param>
        Public Shared Sub MySettingsDatabases_Delete(ByRef pdsMySettings As DataSet, ByVal sDescription As String)
            On Error GoTo ErrHand
            'deletes the datarow (settings)
            Dim dr As DataRow
            For Each dr In pdsMySettings.Tables("Databases").Rows
                If Tools.Encrypt(CStr(dr("Description")), "database") = sDescription Then
                    pdsMySettings.Tables("Databases").Rows.Remove(dr)
                    'My.Settings.Save()
                    MySettings_Save(pdsMySettings)
                    Exit For
                End If
            Next

            Exit Sub
ErrHand:

            Resume Next
        End Sub
        ''' <summary>
        ''' Validates to make sure the internatl storage database has the correct columns, if not it creates it.
        ''' </summary>
        Public Shared Sub MySettingsDatabases_Validate(ByRef oDS As DataSet) 'As DataTable

            Dim dt As DataTable
            Dim dc As DataColumn
            'Dim ds As New DataSet

            Try
                If IsNothing(oDS.Tables("Databases")) Then
                    'call Validate routine afterward, so exit out after this point.
                    MySettingsDatabases_Create(oDS)
                    Exit Sub
                End If

                'ds = My.Settings.DataBases()
                dt = oDS.Tables("Databases")


                If Not dt.Columns.Contains("Description") Then
                    dc = New DataColumn("Description")              'create column headers
                    dc.DataType = System.Type.GetType("System.String")
                    dt.Columns.Add(dc)
                    dc = Nothing
                End If

                If Not dt.Columns.Contains("Server") Then
                    dc = New DataColumn("Server")
                    dc.DataType = System.Type.GetType("System.String")
                    dt.Columns.Add(dc)
                    dc = Nothing
                End If

                If Not dt.Columns.Contains("Database") Then
                    dc = New DataColumn("Database")
                    dc.DataType = System.Type.GetType("System.String")
                    dt.Columns.Add(dc)
                    dc = Nothing
                End If

                If Not dt.Columns.Contains("UID") Then
                    dc = New DataColumn("UID")
                    dc.DataType = System.Type.GetType("System.String")
                    dt.Columns.Add(dc)
                    dc = Nothing
                End If

                If Not dt.Columns.Contains("PWD") Then
                    dc = New DataColumn("PWD")
                    dc.DataType = System.Type.GetType("System.String")
                    dt.Columns.Add(dc)
                    dc = Nothing
                End If

                'If Not dt.Columns.Contains("ExpressEdition") Then
                '    dc = New DataColumn("ExpressEdition")
                '    dc.DataType = System.Type.GetType("System.Boolean")
                '    dt.Columns.Add(dc)
                '    dc = Nothing
                'End If


                oDS.Tables.Remove(dt)
                oDS.Tables.Add(dt)
                'My.Settings.DataBases = ds                      'save to settings

            Catch ex As Exception
                Throw ex
            End Try


            '            Exit Sub
            'ErrHand:
            '            If blnShowErrors Then
            '                frmError.ParseException(Err.GetException())
            '            End If
            '            Resume Next
        End Sub
        ''' <summary>
        ''' simply creates the dataset for internal use.
        ''' </summary>
        Public Shared Sub MySettingsDatabases_Create(ByRef oDS As DataSet) 'As DataTable
            On Error GoTo ErrHand
            Dim ds As New DataSet("Config")                 'create dataset
            Dim dt As New DataTable("Databases")            'create datatable

            ds.Tables.Add(dt)
            oDS = ds                      'save to settings

            MySettingsDatabases_Validate(oDS)
            MySettings_Save(oDS)
            Exit Sub
ErrHand:

            Resume Next
        End Sub
        ''' <summary>
        ''' Replaces single quotes in a string with two single quotes
        ''' </summary>
        ''' <param name="strSQL">Value</param>
        ''' <returns>String</returns>
        Public Shared Function Sq(ByVal strSQL As String) As String
            Sq = Replace(strSQL, "'", "''", 1)
        End Function
        ''' <summary>
        ''' Replaces single quotes in a string with two single quotes This also adds the single quote delimeter as well.
        ''' </summary>
        ''' <param name="strSQL">Value</param>
        ''' <returns>String</returns>
        Public Shared Function dSq(ByVal strSQL As String) As String
            dSq = "'" & Replace(strSQL, "'", "''", 1) & "'"
        End Function
        Public Shared Function dSq(ByVal dDate As Date) As String
            Return dSq(Format(dDate, "yyyy-MM-dd HH:mm:ss.fff"))
        End Function


        ''' <summary>
        ''' Adds the And to the end of a where condition if the condition is not blank, else it leaves it alone.
        ''' </summary>
        ''' <param name="strSQL">Value</param>
        ''' <returns>String</returns>
        ''' <remarks>This is to build dynamic wheres</remarks>
        Public Shared Function fcnAnd(ByVal strSQL As String) As String
            If strSQL = "" Then
                fcnAnd = ""
            Else
                fcnAnd = strSQL & " and "
            End If
        End Function
        ''' <summary>
        ''' This function adds a space to zero lenght strings
        ''' </summary>
        ''' <param name="strSQL">Value</param>
        ''' <returns>String</returns>
        Public Shared Function fcnBlank(ByVal strSQL As String) As String
            If Len(strSQL) = 0 Then
                fcnBlank = " "
            Else
                fcnBlank = strSQL
            End If
        End Function
        ''' <summary>
        ''' This function adds a space to zero lenght strings
        ''' </summary>
        ''' <param name="strSQL">Value</param>
        ''' <returns>String</returns>
        Public Function fcnDBNULL(ByVal strSQL As Object) As String
            If strSQL Is DBNull.Value Then
                Return ""
            Else
                Return CStr(strSQL)
            End If
        End Function
        ''' <summary>
        ''' This function will replace the WildCard * with the % wildcard
        ''' </summary>
        ''' <param name="sSQL">Value</param>
        ''' <returns>String</returns>
        Public Shared Function fcnWild(ByVal sSQL As String) As String
            Return Replace(sSQL, "*", "%", 1)
        End Function
        ''' <summary>
        ''' This will set 2 datatables equal If rows are missing then it will add the missing rows
        '''         ''' </summary>
        ''' <param name="dtTarget">Target Table</param>
        ''' <param name="dtSource">Source Table</param>
        ''' <param name="aExcludedColumns">Columns whos data is left alone</param>
        ''' <param name="blnAcceptChanges">Set the target table as accepting all changes upon leaving</param>
        ''' <remarks>This does not remove rows from the existing table, so it will not perform deletes</remarks>
        Public Shared Sub SetDataTablesEqualWithExcluded(ByRef dtTarget As DataTable, ByRef dtSource As DataTable, ByRef aExcludedColumns As ArrayList, Optional ByVal blnAcceptChanges As Boolean = True)
            On Error GoTo ErrHand

            Dim iCount As Integer
            Dim drT As DataRow

            For iCount = 0 To dtSource.Rows.Count - 1
                If dtTarget.Rows.Count - 1 < iCount Then
                    'add new row
                    drT = dtTarget.NewRow()
                Else
                    drT = dtTarget.Rows(iCount)
                End If
                SetDataRowsEqualWExclude(drT, dtSource.Rows(iCount), aExcludedColumns)

                'If drT.RowState <> DataRowState.Added Then dtTarget.Rows(iCount).SetAdded()
                If drT.RowState = DataRowState.Detached Then
                    dtTarget.Rows.Add(drT)
                End If

            Next
            If blnAcceptChanges Then
                dtTarget.AcceptChanges()
            End If

            Exit Sub
ErrHand:

            Resume Next
        End Sub
        Public Shared Sub SetDataTablesEqualAudit(ByRef dtTarget As DataTable, ByRef dtSource As DataTable)

            Dim iCount As Integer
            Dim drT As DataRow
            'Dim blnChanged As Boolean = False

            For iCount = 0 To dtSource.Rows.Count - 1
                If dtTarget.Rows.Count - 1 < iCount Then
                    'add new row
                    drT = dtTarget.NewRow()
                Else
                    drT = dtTarget.Rows(iCount)
                End If


                SetDataRowsEqual(drT, dtSource.Rows(iCount))

                'If drT.RowState <> DataRowState.Added Then dtTarget.Rows(iCount).SetAdded()
                If drT.RowState = DataRowState.Detached Then
                    dtTarget.Rows.Add(drT)
                End If

                'blnChanged = True
            Next

            'Check to make sure that the same number of rows exists
            If dtSource.Rows.Count > 0 Then
                While dtSource.Rows.Count <> dtTarget.Rows.Count
                    'remove last row until equal
                    dtTarget.Rows.RemoveAt(dtTarget.Rows.Count - 1)
                    '   blnChanged = True
                End While

            End If

            'If blnChanged Then dtTarget.AcceptChanges()

        End Sub
        Public Shared Sub SetDataRowsEqualWExclude(ByVal dRTarget As System.Data.DataRow, ByVal dRSource As System.Data.DataRow, ByVal sExclude As ArrayList, Optional ByVal blnDoNotSetDBNulls As Boolean = False)
            Dim iL As Integer
            Dim iL2 As Integer
            Dim blnSet As Boolean
            Dim sName As String
            For iL = 0 To dRTarget.Table.Columns.Count - 1
                sName = dRTarget.Table.Columns(iL).ColumnName
                blnSet = True

                For iL2 = 0 To sExclude.Count - 1
                    If CType(sExclude(iL2), String) = sName Then
                        blnSet = False
                        Exit For
                    End If
                Next

                If blnSet Then
                    If blnDoNotSetDBNulls Then
                        If dRSource(sName) IsNot DBNull.Value Then
                            If dRSource.Table.Columns.Contains(sName) Then
                                dRTarget(sName) = dRSource(sName)
                            End If
                        End If
                    Else
                        If dRSource.Table.Columns.Contains(sName) Then
                            dRTarget(sName) = dRSource(sName)
                        End If
                    End If
                End If
            Next
        End Sub
        Public Shared Sub RenameIP_Columns(ByRef dT As System.Data.DataTable)
            Dim iL As Integer
            Dim sName As String
            For iL = 0 To dT.Columns.Count - 1
                sName = dT.Columns(iL).ColumnName
                sName = sName.Substring(3, sName.Length - 3)
                dT.Columns(iL).ColumnName = sName
            Next
        End Sub
        Public Shared Sub RenameIP_ColumnsBack(ByRef dT As System.Data.DataTable)
            Dim iL As Integer
            Dim sName As String
            For iL = 0 To dT.Columns.Count - 1
                sName = dT.Columns(iL).ColumnName
                sName = "IP_" & sName
                dT.Columns(iL).ColumnName = sName
            Next
        End Sub
        Public Shared Sub SetDataRowsEqual(ByVal drTarget As System.Data.DataRow, ByVal drSource As System.Data.DataRow)
            Dim iL As Integer
            Dim sName As String
            For iL = 0 To drTarget.Table.Columns.Count - 1
                sName = drTarget.Table.Columns(iL).ColumnName
                If drSource.Table.Columns.Contains(sName) Then
                    drTarget(sName) = drSource(sName)
                End If
            Next
        End Sub
        Public Shared Function AreDataRowsEqual(ByRef dR1 As System.Data.DataRow, ByRef dR2 As System.Data.DataRow) As Boolean
            Dim iL As Integer
            Dim sName As String
            For iL = 0 To dR1.Table.Columns.Count - 1
                sName = dR1.Table.Columns(iL).ColumnName

                If Not dR1(sName).Equals(dR2(sName)) Then
                    If Not CStr(dR1(sName)) = CStr(dR2(sName)) Then
                        Return False
                    End If
                End If

            Next
            Return True
        End Function
        Public Shared Function AreDataRowsEqualExclude(ByRef dR1 As System.Data.DataRow, ByRef dR2 As System.Data.DataRow, ByVal ExcludedList As List(Of String)) As Boolean
            Dim iL As Integer
            Dim sName As String
            For iL = 0 To dR1.Table.Columns.Count - 1
                sName = dR1.Table.Columns(iL).ColumnName
                If Not ExcludedList.Contains(sName) Then
                    If Not dR1(sName).Equals(dR2(sName)) Then
                        If Not CStr(dR1(sName)) = CStr(dR2(sName)) Then
                            Return False
                        End If
                    End If
                End If
            Next
            Return True
        End Function
        Public Shared Function AreDataTablesEqual(ByRef dT1 As System.Data.DataTable, ByRef dT2 As System.Data.DataTable) As Boolean
            Dim iRows1 As Integer
            Dim iRows2 As Integer
            Dim iL As Integer

            iRows1 = dT1.Rows.Count
            iRows2 = dT2.Rows.Count

            If iRows1 <> iRows2 Then 'Tables are diffrent as the # of rows are diffrent
                Return False
            End If

            For iL = 0 To iRows1 - 1
                If DataFunctions.AreDataRowsEqual(dT1.Rows(iL), dT2.Rows(iL)) = False Then
                    Return False
                End If
            Next

            Return True

        End Function
        Public Shared Function AreDataTablesEqualExclude(ByRef dT1 As System.Data.DataTable, ByRef dT2 As System.Data.DataTable, ByVal ExcludedFields As List(Of String)) As Boolean
            Dim iRows1 As Integer
            Dim iRows2 As Integer
            Dim iL As Integer

            iRows1 = dT1.Rows.Count
            iRows2 = dT2.Rows.Count

            If iRows1 <> iRows2 Then 'Tables are diffrent as the # of rows are diffrent
                Return False
            End If

            For iL = 0 To iRows1 - 1
                If DataFunctions.AreDataRowsEqualExclude(dT1.Rows(iL), dT2.Rows(iL), ExcludedFields) = False Then
                    Return False
                End If
            Next

            Return True

        End Function


        ''' <summary>
        ''' Performs a column audit, to identify which columns was modified. Will return a Datatable with a list of columns with the values that were changed.
        ''' </summary>
        ''' <param name="drOld">Original values as datarow</param>
        ''' <param name="drNew">New values as datarow</param>
        ''' <returns>Datatable (Field, ValueBefore, ValueAfter, Type)</returns>
        ''' <remarks></remarks>
        Public Shared Function DataRowAudit(ByRef drOld As System.Data.DataRow, ByRef drNew As System.Data.DataRow, ByRef sExclusionList As String) As DataTable
            On Error GoTo ErrHand
            'todo: DataRowAudit (make sure this is working correctly...
            Dim iL As Integer, iExclude As Integer
            Dim sName As String
            Dim sExcludeList() As String = sExclusionList.Split(CChar(","))
            Dim dt As New DataTable
            Dim blnSet As Boolean = True
            Dim dr As DataRow

            dt.Columns.Add("Field")
            dt.Columns.Add("ValueBefore")
            dt.Columns.Add("ValueAfter")
            dt.Columns.Add("Type")

            For iL = 0 To drNew.Table.Columns.Count - 1
                sName = drNew.Table.Columns(iL).ColumnName
                blnSet = True


                For iExclude = 0 To sExcludeList.Length - 1

                    If (sExcludeList(iExclude) = "") Or (sExcludeList(iExclude) = sName) Then
                        blnSet = False
                        Exit For
                    End If

                Next
                If (blnSet = True) And Not drOld(sName).Equals(drNew(sName)) Then
                    dr = dt.NewRow
                    dr("Field") = sName
                    dr("ValueBefore") = CStr(drOld(sName))
                    dr("ValueAfter") = CStr(drNew(sName))
                    dr("Type") = drOld(sName).GetType.ToString
                    dt.Rows.Add(dr)
                    'Return False
                End If

            Next
            Return dt
            Exit Function
ErrHand:

            Resume Next
        End Function
        Public Shared Function AddConstraintToDS(ByRef lds As System.Data.DataSet, ByVal sParTable As String,
                            ByVal sChildTable As String, ByVal sParField As String, ByVal sChildField As String) As Boolean
            Dim fkCons As System.Data.ForeignKeyConstraint
            Dim parCol As System.Data.DataColumn
            Dim childCol As System.Data.DataColumn

            parCol = lds.Tables(sParTable).Columns(sParField)
            Try
                childCol = lds.Tables(sChildTable).Columns(sChildField)
            Catch ex As Exception
                Return False
                Exit Function
            End Try

            Try
                fkCons = New System.Data.ForeignKeyConstraint(sParTable & "_" & sChildTable & "_FK", parCol, childCol)
                fkCons.UpdateRule = System.Data.Rule.Cascade
                fkCons.AcceptRejectRule = System.Data.AcceptRejectRule.Cascade
                lds.Tables(sChildTable).Constraints.Add(fkCons)
            Catch ex As Exception
                Return False
                Exit Function
            End Try

            Return True
        End Function


    End Class

End Namespace