﻿
Imports System.IO
Imports System.Drawing
Imports System.Data
'Imports System.Drawing.Imaging

Namespace com
    Public Class Tools

        Public Shared Function ConvertStringToTimeSpanFormat(InputTime As String) As String
            Return Format(CDate("1/1/1980 " & InputTime), "HH:mm:ss")
        End Function
        Public Shared Function ConvertTimeToShortHMM(InputTime As String) As String
            If InStr(InputTime, " ") <> -1 Then
                Return Format(CDate(InputTime), "H:mm:ss")
            Else
                Return Format(CDate("1/1/1980 " & InputTime), "H:mm:ss")
            End If

        End Function


        Public Shared Function Encrypt(ByVal sString As String, ByVal sEncryptPassWord As String) As String
            On Error GoTo NPEErrHand

            ' sString = the string you wish to encrypt or decrypt.
            ' sEncryptPassWord = the password with which to encrypt the string.
            Dim iLen As Integer, iL As Integer
            Dim sChar As String
            iLen = Len(sEncryptPassWord)
            For iL = 1 To Len(sString)
                sChar = CStr(Asc(Mid$(sEncryptPassWord, (iL Mod iLen) - iLen * CInt(CBool((iL Mod iLen) = 0)), 1)))
                Mid$(sString, iL, 1) = Chr(CInt(Asc(Mid$(sString, iL, 1)) Xor CLng(sChar)))
            Next iL
            Return sString

            Exit Function
NPEErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Function
        Public Shared Function dSq(ByVal strSQL As String) As String
            dSq = "'" & Replace(strSQL, "'", "''", 1) & "'"
        End Function
        Public Shared Function dSq(ByVal dDate As Date) As String
            Return dSq(Format(dDate, "yyyy-MM-dd HH:mm:ss.fff"))
        End Function




        Public Function XMLtoDS(inputXml As String) As DataSet
            Dim RetDS As New DataSet
            If inputXml <> "" Then
                Dim sr As System.IO.StringReader = New System.IO.StringReader(inputXml)

                RetDS.ReadXml(sr)
                Return RetDS

            Else
                Return Nothing
            End If
        End Function
    End Class
End Namespace
