﻿Imports System.Data

Namespace com.db
    Public Module config
        ''' <summary>
        ''' Adds or Edits a records to the internet (my.Settings) database for the database created
        ''' </summary>
        ''' <param name="blnAddNew">Add new database (no = edit)</param>
        ''' <param name="sDescription">Description of database</param>
        ''' <param name="sServer">Database Server</param>
        ''' <param name="sDB">Database Name</param>
        ''' <param name="sUID">UserID</param>
        ''' <param name="sPWD">Password</param>
        Public Function MySettingsDatabases_Update(ByRef oMySettings As DataSet, ByVal blnAddNew As Boolean, ByVal sDescription As String, ByVal sServer As String, ByVal sDB As String, ByVal sUID As String,
            ByVal sPWD As String, ByVal filename As String) As Boolean
            On Error GoTo NPEErrHand

            MySettingsDatabases_Validate(oMySettings, filename)

            Dim dr As DataRow = Nothing
            Dim blnContinue As Boolean = False

            If blnAddNew Then
                'check the description name...
                blnContinue = True
                For Each dr In oMySettings.Tables("Databases").Rows
                    If Tools.Encrypt(CStr(dr("Description")), "database").ToUpper = sDescription.ToUpper Then

                        'description name exists, please rename
                        'MessageBox.Show(NPE.GetMessage(161), cProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)

                        Return False
                    End If
                Next
                dr = oMySettings.Tables("Databases").NewRow

            Else
                Dim iL As Integer = 0
                For iL = 0 To oMySettings.Tables("Databases").Rows.Count - 1

                    If Tools.Encrypt(CStr(oMySettings.Tables("Databases").Rows(iL)("Description")), "database") = sDescription Then
                        dr = oMySettings.Tables("Databases").Rows(iL)
                        blnContinue = True
                        Exit For
                    End If
                Next
            End If

            If blnContinue Then
                dr("Server") = Tools.Encrypt(sServer, "database")
                dr("Database") = Tools.Encrypt(sDB, "database")
                dr("UID") = Tools.Encrypt(sUID, "database")
                dr("PWD") = Tools.Encrypt(sPWD, "database")

                If blnAddNew Then
                    dr("Description") = Tools.Encrypt(sDescription, "database")
                    oMySettings.Tables("Databases").Rows.Add(dr)
                End If

                db.MySettings_Save(oMySettings, filename)



            End If

            Return True
            Exit Function
NPEErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Function
        Public Sub MySettings_Load(ByRef oMySettings As DataSet, ByVal Filename As String)
            On Error GoTo NPEErrHand

            If IO.File.Exists(Filename) Then
                oMySettings.ReadXml(Filename)
            End If


            Exit Sub
NPEErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Sub

        Public Sub MySettings_Save(ByVal oMySettings As DataSet, ByVal filename As String)
            On Error GoTo NPEErrHand

            If IO.File.Exists(filename) Then IO.File.Delete(filename)

            oMySettings.WriteXml(filename, XmlWriteMode.WriteSchema)

            Exit Sub
NPEErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Sub

        ''' <summary>
        ''' Deletes a database information setup from my.settings database
        ''' </summary>
        ''' <param name="sDescription">Database description</param>
        Public Sub MySettingsDatabases_Delete(ByRef pdsMySettings As DataSet, ByVal sDescription As String, ByVal filename As String)
            On Error GoTo NPEErrHand
            'deletes the datarow (settings)
            Dim dr As DataRow
            For Each dr In pdsMySettings.Tables("Databases").Rows
                If Tools.Encrypt(CStr(dr("Description")), "database") = sDescription Then
                    pdsMySettings.Tables("Databases").Rows.Remove(dr)
                    'My.Settings.Save()
                    MySettings_Save(pdsMySettings, filename)
                    Exit For
                End If
            Next

            Exit Sub
NPEErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Sub
        ''' <summary>
        ''' Validates to make sure the internatl storage database has the correct columns, if not it creates it.
        ''' </summary>
        Public Sub MySettingsDatabases_Validate(ByRef oDS As DataSet, ByVal filename As String) 'As DataTable

            Dim dt As DataTable
            Dim dc As DataColumn
            'Dim ds As New DataSet

            Try
                If IsNothing(oDS.Tables("Databases")) Then
                    'call Validate routine afterward, so exit out after this point.
                    MySettingsDatabases_Create(oDS, filename)
                    Exit Sub
                End If

                'ds = My.Settings.DataBases()
                dt = oDS.Tables("Databases")


                If Not dt.Columns.Contains("Description") Then
                    dc = New DataColumn("Description")              'create column headers
                    dc.DataType = System.Type.GetType("System.String")
                    dt.Columns.Add(dc)
                    dc = Nothing
                End If

                If Not dt.Columns.Contains("Server") Then
                    dc = New DataColumn("Server")
                    dc.DataType = System.Type.GetType("System.String")
                    dt.Columns.Add(dc)
                    dc = Nothing
                End If

                If Not dt.Columns.Contains("Database") Then
                    dc = New DataColumn("Database")
                    dc.DataType = System.Type.GetType("System.String")
                    dt.Columns.Add(dc)
                    dc = Nothing
                End If

                If Not dt.Columns.Contains("UID") Then
                    dc = New DataColumn("UID")
                    dc.DataType = System.Type.GetType("System.String")
                    dt.Columns.Add(dc)
                    dc = Nothing
                End If

                If Not dt.Columns.Contains("PWD") Then
                    dc = New DataColumn("PWD")
                    dc.DataType = System.Type.GetType("System.String")
                    dt.Columns.Add(dc)
                    dc = Nothing
                End If



                oDS.Tables.Remove(dt)
                oDS.Tables.Add(dt)
                'My.Settings.DataBases = ds                      'save to settings

            Catch ex As Exception
                Throw ex
            End Try


            '            Exit Sub
            'NPEErrHand:
            '            If blnShowErrors Then
            '                frmError.ParseException(Err.GetException())
            '            End If
            '            Resume Next
        End Sub
        ''' <summary>
        ''' simply creates the dataset for internal use.
        ''' </summary>
        Public Sub MySettingsDatabases_Create(ByRef oDS As DataSet, ByVal Filename As String) 'As DataTable
            On Error GoTo NPEErrHand
            Dim ds As New DataSet("Config")                 'create dataset
            Dim dt As New DataTable("Databases")            'create datatable

            ds.Tables.Add(dt)
            oDS = ds                      'save to settings

            MySettingsDatabases_Validate(oDS, Filename)
            MySettings_Save(oDS, Filename)
            Exit Sub
NPEErrHand:
            'If ErrorClass.blnShowErrors Then
            '    ErrorClass.pfrmError.ParseException(Err.GetException())
            'End If
            Resume Next
        End Sub
    End Module
End Namespace