﻿
Option Strict Off
Imports System.Data
Imports System.Collections
Imports System.Data.SqlClient

Namespace com
    Partial Class ISSData
        'Private Shared _CONNECTIONSTRING As String = oData.GetConnectionString
        Private Function getConnection() As System.Data.SqlClient.SqlConnection
            'returns connection string.
            Return New System.Data.SqlClient.SqlConnection(GetConnectionString)
        End Function
        Public Function Execute(ByVal objConnStr As String, ByVal strQuery As String) As DataTable
            'To be used in places where a different connection object is required.
            Try
                Dim objC As New SqlClient.SqlConnection
                objC.ConnectionString = objConnStr
                Return Execute(objC, strQuery, "table1")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function Execute(ByVal objConn As System.Data.SqlClient.SqlConnection, ByVal strProcedureName As String, ByVal strDateTableName As String, ByVal parameters As ArrayList) As DataTable

            'Dim conn As System.Data.SqlClient.SqlConnection = getConnection()
            Dim conn As System.Data.SqlClient.SqlConnection = objConn
            Dim cmd As System.Data.SqlClient.SqlCommand = conn.CreateCommand()


            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = strProcedureName

            If parameters IsNot Nothing Then
                Dim obj As Object
                For Each obj In parameters
                    If obj.Value Is Nothing Then
                        obj.Value = DBNull.Value
                    End If
                    cmd.Parameters.Add(CType(obj, System.Data.SqlClient.SqlParameter))


                Next
            End If


            Dim Table As New DataTable
            Dim result As Integer
            Try
                conn.Open()
                Dim reader As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader
                If (Table.Columns.Count = 0) Then
                    Table.TableName = strDateTableName
                    Dim i As Integer = 0
                    Do While (i < reader.FieldCount)
                        Dim type As System.Type = reader.GetFieldType(i)
                        Dim name As String = reader.GetName(i)
                        Table.Columns.Add(name, type)
                        i = i + 1
                    Loop
                End If
                Table.Clear()

                Do While reader.Read
                    Dim row As System.Data.DataRow = Table.NewRow
                    Dim rowdata() As Object = New Object((reader.FieldCount) - 1) {}
                    reader.GetValues(rowdata)
                    row.ItemArray = rowdata
                    Table.Rows.Add(row)
                    result = result + 1
                Loop
                reader.Close()

                Return Table

            Catch ex As System.Data.SqlClient.SqlException
                Throw ex
            Finally
                If Not conn Is Nothing Then
                    If conn.State = ConnectionState.Open Then conn.Close()
                End If

            End Try


        End Function
        Public Sub Execute(ByVal objConn As System.Data.SqlClient.SqlConnection, ByVal strQuery As String)
            'Just executes a query with no results.

            Dim cmd As System.Data.SqlClient.SqlCommand = objConn.CreateCommand()
            Dim rowsAffected As Integer
            cmd.CommandType = CommandType.Text
            cmd.CommandText = strQuery

            Try

                objConn.Open()
                rowsAffected = cmd.ExecuteNonQuery()


            Catch ex As System.Data.SqlClient.SqlException
                Throw ex
            Finally
                If Not objConn Is Nothing Then
                    If objConn.State = ConnectionState.Open Then objConn.Close()
                End If

            End Try

        End Sub

        Public Function Execute(ByVal objConn As System.Data.SqlClient.SqlConnection, ByVal strQuery As String, ByVal strTableName As String) As DataTable

            Dim cmd As System.Data.SqlClient.SqlCommand = objConn.CreateCommand()


            cmd.CommandType = CommandType.Text
            cmd.CommandText = strQuery
            cmd.CommandTimeout = 360

            Dim Table As New DataTable
            Dim result As Integer
            Try
                objConn.Open()
                Dim reader As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader
                If (Table.Columns.Count = 0) Then
                    Table.TableName = strTableName
                    Dim i As Integer = 0
                    Do While (i < reader.FieldCount)
                        Dim type As System.Type = reader.GetFieldType(i)
                        Dim name As String = reader.GetName(i)
                        Table.Columns.Add(name, type)
                        i = i + 1
                    Loop
                End If
                Table.Clear()

                Do While reader.Read
                    Dim row As System.Data.DataRow = Table.NewRow
                    Dim rowdata() As Object = New Object((reader.FieldCount) - 1) {}
                    reader.GetValues(rowdata)
                    row.ItemArray = rowdata
                    Table.Rows.Add(row)
                    result = result + 1
                Loop
                reader.Close()
                reader = Nothing
                Return Table

            Catch ex As System.Data.SqlClient.SqlException
                Throw ex
            Finally
                If Not objConn Is Nothing Then
                    If objConn.State = ConnectionState.Open Then objConn.Close()
                End If

                cmd.Dispose()
                cmd = Nothing
                objConn.Dispose()
            End Try


        End Function
        Public Function Execute(ByVal strQueryString As String) As DataTable
            Dim conn As System.Data.SqlClient.SqlConnection = getConnection()
            Try
                Return Execute(conn, strQueryString, String.Empty)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub ExecuteSQL(ByVal strQueryString As String)
            Dim conn As System.Data.SqlClient.SqlConnection = getConnection()
            Try
                Execute(conn, strQueryString)
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Function Execute(ByVal strProcedureName As String, ByVal strDataTableName As String, ByVal parameters As ArrayList) As DataTable
            Dim conn As System.Data.SqlClient.SqlConnection

            Try
                conn = getConnection()

                Return Execute(conn, strProcedureName, strDataTableName, parameters)

            Catch ex As Exception

                Throw ex

            End Try


        End Function

        Public Function Execute(ByVal strProcedureName As String, ByVal strDataTableName As String, ByVal parameters As ArrayList, ByRef WarningOutput As String) As DataTable
            Dim conn As System.Data.SqlClient.SqlConnection

            Try
                conn = getConnection()

                Return Execute(conn, strProcedureName, strDataTableName, parameters)

            Catch ex As Exception

                Throw ex

            End Try


        End Function

        Public Shared Function DBNullCast(ByRef obj As Object) As Object
            'THIS FUNCTION SERVER AdS A CONVERSION TOOL FOR NULL VALUES THAT GO TO THE DATABASE. 
            'DATES CANNOT BE EMPTY STRINGS FOR EXAMPLE

            If TypeOf obj Is DateTime AndAlso obj = #12:00:00 AM# Then
                Return Nothing 'DBNull.Value
            End If


            If obj Is Nothing Or obj Is DBNull.Value Then
                Return Nothing 'DBNull.Value
            Else
                Return obj
            End If

        End Function

    End Class

End Namespace

