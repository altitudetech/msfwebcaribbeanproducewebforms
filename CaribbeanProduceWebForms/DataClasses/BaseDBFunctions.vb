﻿Imports System.ComponentModel
Imports System.Data
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Text
Imports DevExpress.Xpf.Editors.Helpers

Namespace com.data
    Public Class BaseDBFunctions
        Inherits Base
        Implements INotifyPropertyChanged

        Private disposed As Boolean = False
        Private umResource As IntPtr
        'Dim oDataClass As Object = Me
        '**** Get Class Name - Excluding 
        'Public type As Type = oDataClass.[GetType]()

        Public Sub New()
            MyBase.New()


            ' Save the instance name as an unmanaged resource
            umResource = Marshal.StringToCoTaskMemAuto(InstanceName)
        End Sub


        Protected Overloads Overrides Sub Dispose(disposing As Boolean)
            If disposed = False Then
                If disposing Then
                    Console.WriteLine("[{0}].Derived.Dispose(true)", InstanceName)
                    ' Release managed resources.
                Else
                    Console.WriteLine("[{0}].Derived.Dispose(false)", InstanceName)
                End If
                ' Release unmanaged resources.
                If umResource <> IntPtr.Zero Then
                    Marshal.FreeCoTaskMem(umResource)
                    Console.WriteLine("[{0}] Unmanaged memory freed at {1:x16}",
                    InstanceName, umResource.ToInt64())
                    umResource = IntPtr.Zero
                End If
                disposed = True
            End If
            ' Call Dispose in the base class.
            MyBase.Dispose(disposing)
        End Sub



#Region "Data Management"
        Public dt As DataTable
        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        Public Sub FillDataTable(Optional WhereClause As String = "")
            On Error GoTo NPEErrHand
            Dim oDataClass As Object = Me
            '**** Get Class Name - Excluding 
            Dim type As Type = oDataClass.[GetType]()
            Dim sSQL As String = String.Format("SELECT * FROM {0}", type.Name)


            'fill ds from ssql
            If WhereClause <> "" Then
                sSQL += " Where " & WhereClause
            End If

            dt = oDataManagement.Execute(sSQL)

            'if exists
            'If dt.Rows.Count > 0 Then

            '    Dim properties As PropertyInfo() = type.GetProperties()

            '    For Each [property] As PropertyInfo In properties

            '        If dt.Rows(0)([property].Name) IsNot Nothing Then
            '            If TypeOf (dt.Rows(0)([property].Name)) IsNot System.DBNull Then
            '                [property].SetValue(oDataClass, dt.Rows(0)([property].Name), Nothing)
            '            End If
            '        End If

            '    Next

            'End If
            oDataClass = Nothing
            type = Nothing
            sSQL = Nothing

            Exit Sub
NPEErrHand:
            ' If ErrorClass.blnShowErrors Then ErrorClass.pfrmError.ParseException(Err.GetException())
            Resume Next
        End Sub

        Public Sub ClearObjectData()
            On Error GoTo NPEErrHand
            Dim oDataClass As Object = Me

            If oDataClass.dt IsNot Nothing Then oDataClass.dt.clear : oDataClass.dt = Nothing
            '**** Get Class Name - Excluding 
            Dim type As Type = oDataClass.[GetType]()



            'fill ds from ssql


            Dim properties As PropertyInfo() = type.GetProperties()

            For Each [property] As PropertyInfo In properties
                If [property].Name <> "InstanceName" Then

                    [property].SetValue(oDataClass, Nothing, Nothing)

                End If
            Next
            properties = Nothing

            oDataClass = Nothing
            type = Nothing

            Exit Sub
NPEErrHand:
            'If ErrorClass.blnShowErrors Then ErrorClass.pfrmError.ParseException(Err.GetException())
            Resume Next
        End Sub


        Public Sub FillByID(ByVal ID As String)
            On Error GoTo NPEErrHand
            Dim oDataClass As Object = Me
            '**** Get Class Name - Excluding 
            Dim type As Type = oDataClass.[GetType]()
            Dim sSQL As String = String.Format("SELECT * FROM {0} WHERE ID = '{1}'", type.Name, ID.ToString)


            'fill ds from ssql

            dt = oDataManagement.Execute(sSQL)

            'if exists
            If dt.Rows.Count > 0 Then

                Dim properties As PropertyInfo() = type.GetProperties()

                For Each [property] As PropertyInfo In properties
                    If [property].Name <> "InstanceName" Then
                        If dt.Rows(0)([property].Name) IsNot Nothing Then
                            If TypeOf (dt.Rows(0)([property].Name)) IsNot System.DBNull Then
                                [property].SetValue(oDataClass, dt.Rows(0)([property].Name), Nothing)
                            End If
                        End If
                    End If
                Next
                properties = Nothing
            End If

            oDataClass = Nothing
            type = Nothing
            sSQL = Nothing
            Exit Sub
NPEErrHand:
            'If ErrorClass.blnShowErrors Then ErrorClass.pfrmError.ParseException(Err.GetException())
            Resume Next
        End Sub

        Public Sub FillByColumnArrayAndValueArray(ByVal Column() As String, ByVal Value() As String)
            On Error GoTo NPEErrHand
            Dim oDataClass As Object = Me
            '**** Get Class Name - Excluding 
            Dim type As Type = oDataClass.[GetType]()
            Dim sSQL As String = String.Format("SELECT * FROM {0} Where ", type.Name)
            Dim WhereClause As String = ""

            For cCount = 0 To Column.Count - 1
                WhereClause += String.Format("{0} = '{1}'", Column(cCount).ToString, Value(cCount).ToString)
                If cCount < Column.Count - 1 Then
                    WhereClause += " and "
                End If
            Next

            '"WHERE {2} = '{1}'", type.Name, ID.ToString, Column.ToString)
            sSQL += WhereClause

            'fill ds from ssql

            dt = oDataManagement.Execute(sSQL)
            If dt Is Nothing Then Exit Sub
            'if exists
            If dt.Rows.Count > 0 Then

                Dim properties As PropertyInfo() = type.GetProperties()

                For Each [property] As PropertyInfo In properties
                    If [property].Name <> "InstanceName" Then
                        If dt.Rows(0)([property].Name) IsNot Nothing Then
                            If Not TypeOf (dt.Rows(0)([property].Name)) Is System.DBNull Then
                                [property].SetValue(oDataClass, dt.Rows(0)([property].Name), Nothing)
                            End If
                        End If
                    End If
                Next
                properties = Nothing
            End If
            oDataClass = Nothing
            type = Nothing
            sSQL = Nothing
            Exit Sub
NPEErrHand:
            'If ShowErrors Then
            '    frmError.ParseException(Err.GetException)
            'End If

            Resume Next
        End Sub
        Public Sub FillBySpecificColumnAndID(ByVal Column As String, ByVal ID As String)
            On Error GoTo NPEErrHand
            Dim oDataClass As Object = Me
            '**** Get Class Name - Excluding 
            Dim type As Type = oDataClass.[GetType]()
            Dim sSQL As String = String.Format("SELECT * FROM {0} WHERE {2} = '{1}'", type.Name, ID.ToString, Column.ToString)


            'fill ds from ssql

            dt = oDataManagement.Execute(sSQL)
            If dt Is Nothing Then Exit Sub
            'if exists
            If dt.Rows.Count > 0 Then

                Dim properties As PropertyInfo() = type.GetProperties()

                For Each [property] As PropertyInfo In properties
                    If [property].Name <> "InstanceName" Then
                        If dt.Rows(0)([property].Name) IsNot Nothing Then
                            If Not TypeOf (dt.Rows(0)([property].Name)) Is System.DBNull Then
                                [property].SetValue(oDataClass, dt.Rows(0)([property].Name), Nothing)
                            End If
                        End If
                    End If
                Next
                properties = Nothing
            End If
            oDataClass = Nothing
            type = Nothing
            sSQL = Nothing
            Exit Sub
NPEErrHand:
            'If ShowErrors Then
            '    frmError.ParseException(Err.GetException)
            'End If

            Resume Next
        End Sub

        Public Sub FillBySpecificColumnAndID(ByVal Column As String, ByVal ID As String, ByVal SortBy As String)
            On Error GoTo NPEErrHand
            Dim oDataClass As Object = Me
            '**** Get Class Name - Excluding 
            Dim type As Type = oDataClass.[GetType]()
            Dim sSQL As String = String.Format("SELECT * FROM {0} WHERE {2} = '{1}'", type.Name, ID.ToString, Column.ToString)

            If SortBy <> "" Then
                sSQL += " order by " & SortBy
            End If

            'fill ds from ssql

            dt = oDataManagement.Execute(sSQL)
            If dt Is Nothing Then Exit Sub
            'if exists
            If dt.Rows.Count > 0 Then

                Dim properties As PropertyInfo() = type.GetProperties()

                For Each [property] As PropertyInfo In properties
                    If [property].Name <> "InstanceName" Then
                        If dt.Rows(0)([property].Name) IsNot Nothing Then
                            If Not TypeOf (dt.Rows(0)([property].Name)) Is System.DBNull Then
                                [property].SetValue(oDataClass, dt.Rows(0)([property].Name), Nothing)
                            End If
                        End If
                    End If
                Next
                properties = Nothing
            End If
            oDataClass = Nothing
            type = Nothing
            sSQL = Nothing
            Exit Sub
NPEErrHand:
            'If ShowErrors Then
            '    frmError.ParseException(Err.GetException)
            'End If

            Resume Next
        End Sub
        Public Sub DeleteByID(ByVal ID As String)
            Try
                Dim oDataClass As Object = Me
                '**** Get Class Name - Excluding 
                Dim type As Type = oDataClass.[GetType]()
                Dim sSQL As String = "Delete  from " + type.Name + " where id = '" & ID & "'"
                oDataManagement.ExecSQL(sSQL)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub DeleteByColumnAndID(ByVal Column As String, ByVal ID As String)
            Try
                Dim oDataClass As Object = Me
                '**** Get Class Name - Excluding 
                Dim type As Type = oDataClass.[GetType]()
                Dim sSQL As String = "Delete  from " + type.Name + " where " & Column & " = '" & ID & "'"
                oDataManagement.ExecSQL(sSQL)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        '#Region "IDisposable Support"
        '        Private disposedValue As Boolean ' To detect redundant calls

        '        ' IDisposable
        '        Protected Overridable Sub Dispose(disposing As Boolean)
        '            If Not disposedValue Then
        '                If disposing Then
        '                    ' TODO: dispose managed state (managed objects).
        '                End If

        '                ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
        '                ' TODO: set large fields to null.
        '            End If
        '            disposedValue = True
        '        End Sub

        '        ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
        '        'Protected Overrides Sub Finalize()
        '        '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        '        '    Dispose(False)
        '        '    MyBase.Finalize()
        '        'End Sub

        '        ' This code added by Visual Basic to correctly implement the disposable pattern.
        '        Public Sub Dispose() Implements IDisposable.Dispose
        '            ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        '            Dispose(True)
        '            ' TODO: uncomment the following line if Finalize() is overridden above.
        '            GC.SuppressFinalize(Me)
        '        End Sub
        '#End Region

        Function VerifyVarchar(input As String) As String

            Return Replace(input, "'", "''")
        End Function


        Public Function Update() As Boolean
            Try
                Dim oDataClass As Object = Me
                '**** Get Class Name - Excluding 
                Dim type As Type = oDataClass.[GetType]()
                Dim properties As PropertyInfo() = type.GetProperties()
                'updates all variable within
                Dim _blnReturn As Boolean = True
                Dim dsL As New DataSet

                If oDataClass.ID.ToString = Guid.Empty.ToString Then

                    'new id, creates a new id, need to make sure registryid is also a unique value
                    Dim sSQL As String = "INSERT INTO " + type.Name + " (ID) VALUES ('@ID')"

                    'this routine returns the unique identifier in the statement above.
                    If oDataClass.ID.GetType.ToString = "System.Guid" Then
                        oDataClass.ID = oDataManagement.ExecSQLIdentityInsert(sSQL)
                    Else
                        oDataClass.ID = oDataManagement.ExecSQLIdentityInsert(sSQL).ToString
                    End If

                End If

                oDataManagement.FillDataSet(dsL, "SELECT * FROM  " + type.Name + " WHERE id = '" & oDataClass.ID.ToString & "'", type.Name)


                With dsL.Tables(type.Name).Rows(0)

                    For Each [property] As PropertyInfo In properties
                        If [property].Name <> "InstanceName" Then
                            .Item([property].Name) = IIf([property].GetValue(oDataClass, Nothing) Is Nothing, DBNull.Value, [property].GetValue(oDataClass, Nothing))
                            ' Console.WriteLine("Name: " + [property].Name + ", Value: " + [property].GetValue(DataClass, Nothing))
                        End If
                    Next
                End With
                oDataManagement.UpdateDataSetToDB(dsL, "SELECT * FROM  " + type.Name + " WHERE id = '" & oDataClass.ID.ToString & "'", type.Name)
                'create update statement for all variables.

                dsL.Clear()
                dsL.Dispose()
                dsL = Nothing
                oDataClass = Nothing
                type = Nothing
                properties = Nothing
                Return _blnReturn

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function Search(Optional ByVal whereQuery As String = "") As DataTable

            Try
                Dim oDataClass As Object = Me
                '**** Get Class Name - Excluding 
                Dim type As Type = oDataClass.[GetType]()
                'what to query on?

                Dim sSQL As New StringBuilder

                sSQL.AppendFormat("SELECT * FROM {0} ", type.Name)

                If Not String.IsNullOrWhiteSpace(whereQuery) Then sSQL.AppendFormat(" WHERE {0}", whereQuery)

                dt = oDataManagement.Execute(sSQL.ToString)

                Return dt

            Catch ex As Exception
                Throw ex
            End Try

        End Function
#End Region


    End Class
End Namespace
