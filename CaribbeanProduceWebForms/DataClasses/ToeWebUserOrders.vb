﻿
Imports System.ComponentModel
Namespace com.data

    Public Class ToeWebUserOrders
        Inherits BaseDBFunctions
        Implements System.ComponentModel.INotifyPropertyChanged

        Public Event PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs) Implements ComponentModel.INotifyPropertyChanged.PropertyChanged
        Private Sub NotifyPropertyChanged(Optional ByVal propertyName As String = Nothing)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End Sub



        Public Property id As Guid
            Get
                Return _id
            End Get
            Set(value As Guid)
                _id = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _id As Guid


        Public Property Username As String
            Get
                Return _username
            End Get
            Set(value As String)
                _username = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _username As String


        Public Property Order_No As String
            Get
                Return _order_no
            End Get
            Set(value As String)
                _order_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _order_no As String


        Public Property Product_No As String
            Get
                Return _product_no
            End Get
            Set(value As String)
                _product_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _product_no As String


        Public Property QTY As String
            Get
                Return _qty
            End Get
            Set(value As String)
                _qty = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _qty As String


        Public Property Unit_Price As String
            Get
                Return _unit_price
            End Get
            Set(value As String)
                _unit_price = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _unit_price As String


        Public Property Ext_Price As String
            Get
                Return _ext_price
            End Get
            Set(value As String)
                _ext_price = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _ext_price As String


        Public Property UM As String
            Get
                Return _um
            End Get
            Set(value As String)
                _um = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _um As String


        Public Property Description As String
            Get
                Return _description
            End Get
            Set(value As String)
                _description = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _description As String


        Public Property ShipDate As DateTime?
            Get
                Return _shipdate
            End Get
            Set(value As DateTime?)
                _shipdate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _shipdate As DateTime?


        Public Property ShipTo As String
            Get
                Return _shipto
            End Get
            Set(value As String)
                _shipto = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _shipto As String


        Public Property ShipToName As String
            Get
                Return _shiptoname
            End Get
            Set(value As String)
                _shiptoname = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _shiptoname As String

    End Class

End Namespace
