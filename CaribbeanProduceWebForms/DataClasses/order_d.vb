﻿
Imports System.ComponentModel
Namespace com.data

    Public Class order_d
        Inherits BaseDBFunctions
        Implements System.ComponentModel.INotifyPropertyChanged

        Public Event PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs) Implements ComponentModel.INotifyPropertyChanged.PropertyChanged
        Private Sub NotifyPropertyChanged(Optional ByVal propertyName As String = Nothing)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End Sub



        Public Property compid As Integer
            Get
                Return _compid
            End Get
            Set(value As Integer)
                _compid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _compid As Integer


        Public Property route As String
            Get
                Return _route
            End Get
            Set(value As String)
                _route = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _route As String


        Public Property order_no As Integer
            Get
                Return _order_no
            End Get
            Set(value As Integer)
                _order_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _order_no As Integer


        Public Property product_no As String
            Get
                Return _product_no
            End Get
            Set(value As String)
                _product_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _product_no As String


        Public Property qty As Integer?
            Get
                Return _qty
            End Get
            Set(value As Integer?)
                _qty = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _qty As Integer?


        Public Property unit_price As Decimal?
            Get
                Return _unit_price
            End Get
            Set(value As Decimal?)
                _unit_price = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _unit_price As Decimal?


        Public Property ext_price As Decimal?
            Get
                Return _ext_price
            End Get
            Set(value As Decimal?)
                _ext_price = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _ext_price As Decimal?


        Public Property lotnumber As String
            Get
                Return _lotnumber
            End Get
            Set(value As String)
                _lotnumber = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _lotnumber As String


        Public Property linenumber As String
            Get
                Return _linenumber
            End Get
            Set(value As String)
                _linenumber = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _linenumber As String


        Public Property presupuesto As Boolean?
            Get
                Return _presupuesto
            End Get
            Set(value As Boolean?)
                _presupuesto = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _presupuesto As Boolean?


        Public Property um As String
            Get
                Return _um
            End Get
            Set(value As String)
                _um = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _um As String


        Public Property freegoods As Boolean?
            Get
                Return _freegoods
            End Get
            Set(value As Boolean?)
                _freegoods = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _freegoods As Boolean?


        Public Property freegoodsval As Short
            Get
                Return _freegoodsval
            End Get
            Set(value As Short)
                _freegoodsval = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _freegoodsval As Short


        Public Property creditcommentid As Integer?
            Get
                Return _creditcommentid
            End Get
            Set(value As Integer?)
                _creditcommentid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _creditcommentid As Integer?


        Public Property nochargeqty As Decimal?
            Get
                Return _nochargeqty
            End Get
            Set(value As Decimal?)
                _nochargeqty = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _nochargeqty As Decimal?


        Public Property expirationdate As DateTime?
            Get
                Return _expirationdate
            End Get
            Set(value As DateTime?)
                _expirationdate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _expirationdate As DateTime?


        Public Property tableRowCount As Integer?
            Get
                Return _tablerowcount
            End Get
            Set(value As Integer?)
                _tablerowcount = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _tablerowcount As Integer?


        Public Property shippedProduct As Boolean
            Get
                Return _shippedproduct
            End Get
            Set(value As Boolean)
                _shippedproduct = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _shippedproduct As Boolean


        Public Property shippedQuantity As Integer
            Get
                Return _shippedquantity
            End Get
            Set(value As Integer)
                _shippedquantity = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _shippedquantity As Integer


        Public Property salesTax As Decimal
            Get
                Return _salestax
            End Get
            Set(value As Decimal)
                _salestax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _salestax As Decimal


        Public Property UnitPriceDecimal As Decimal?
            Get
                Return _unitpricedecimal
            End Get
            Set(value As Decimal?)
                _unitpricedecimal = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _unitpricedecimal As Decimal?


        Public Property ammount_change As Decimal
            Get
                Return _ammount_change
            End Get
            Set(value As Decimal)
                _ammount_change = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _ammount_change As Decimal


        Public Property Discount As Decimal
            Get
                Return _discount
            End Get
            Set(value As Decimal)
                _discount = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _discount As Decimal


        Public Property OrgPrice As Decimal
            Get
                Return _orgprice
            End Get
            Set(value As Decimal)
                _orgprice = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _orgprice As Decimal


        Public Property PriceAdjustment As Boolean
            Get
                Return _priceadjustment
            End Get
            Set(value As Boolean)
                _priceadjustment = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _priceadjustment As Boolean


        Public Property equipment_no As String
            Get
                Return _equipment_no
            End Get
            Set(value As String)
                _equipment_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _equipment_no As String


        Public Property serial_no As String
            Get
                Return _serial_no
            End Get
            Set(value As String)
                _serial_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _serial_no As String


        Public Property itemremaining As Integer?
            Get
                Return _itemremaining
            End Get
            Set(value As Integer?)
                _itemremaining = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _itemremaining As Integer?


        Public Property itemdispatched As Integer?
            Get
                Return _itemdispatched
            End Get
            Set(value As Integer?)
                _itemdispatched = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _itemdispatched As Integer?


        Public Property modelno As String
            Get
                Return _modelno
            End Get
            Set(value As String)
                _modelno = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _modelno As String


        Public Property commentcredit As String
            Get
                Return _commentcredit
            End Get
            Set(value As String)
                _commentcredit = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _commentcredit As String


        Public Property updatedate As DateTime
            Get
                Return _updatedate
            End Get
            Set(value As DateTime)
                _updatedate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _updatedate As DateTime


        Public Property updateuser As String
            Get
                Return _updateuser
            End Get
            Set(value As String)
                _updateuser = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _updateuser As String


        Public Property hasPreCooperatives As Integer?
            Get
                Return _hasprecooperatives
            End Get
            Set(value As Integer?)
                _hasprecooperatives = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _hasprecooperatives As Integer?


        Public Property hasPreContractPrices As Integer?
            Get
                Return _hasprecontractprices
            End Get
            Set(value As Integer?)
                _hasprecontractprices = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _hasprecontractprices As Integer?


        Public Property hasMessages As Boolean
            Get
                Return _hasmessages
            End Get
            Set(value As Boolean)
                _hasmessages = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _hasmessages As Boolean


        Public Property hasErrorMessages As Boolean
            Get
                Return _haserrormessages
            End Get
            Set(value As Boolean)
                _haserrormessages = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _haserrormessages As Boolean


        Public Property contract_no As Decimal?
            Get
                Return _contract_no
            End Get
            Set(value As Decimal?)
                _contract_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _contract_no As Decimal?


        Public Property freeCaseContract_no As Decimal?
            Get
                Return _freecasecontract_no
            End Get
            Set(value As Decimal?)
                _freecasecontract_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _freecasecontract_no As Decimal?


        Public Property cityTax As Decimal?
            Get
                Return _citytax
            End Get
            Set(value As Decimal?)
                _citytax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _citytax As Decimal?


        Public Property stateTax As Decimal?
            Get
                Return _statetax
            End Get
            Set(value As Decimal?)
                _statetax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _statetax As Decimal?


        Public Property badCreditExtPrice As Decimal?
            Get
                Return _badcreditextprice
            End Get
            Set(value As Decimal?)
                _badcreditextprice = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _badcreditextprice As Decimal?


        Public Property badCreditStateTax As Decimal?
            Get
                Return _badcreditstatetax
            End Get
            Set(value As Decimal?)
                _badcreditstatetax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _badcreditstatetax As Decimal?


        Public Property badCreditMunicipalTax As Decimal?
            Get
                Return _badcreditmunicipaltax
            End Get
            Set(value As Decimal?)
                _badcreditmunicipaltax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _badcreditmunicipaltax As Decimal?


        Public Property goodCreditExtPrice As Decimal?
            Get
                Return _goodcreditextprice
            End Get
            Set(value As Decimal?)
                _goodcreditextprice = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _goodcreditextprice As Decimal?


        Public Property goodCreditStateTax As Decimal?
            Get
                Return _goodcreditstatetax
            End Get
            Set(value As Decimal?)
                _goodcreditstatetax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _goodcreditstatetax As Decimal?


        Public Property goodCreditMunicipalTax As Decimal?
            Get
                Return _goodcreditmunicipaltax
            End Get
            Set(value As Decimal?)
                _goodcreditmunicipaltax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _goodcreditmunicipaltax As Decimal?

    End Class

End Namespace
