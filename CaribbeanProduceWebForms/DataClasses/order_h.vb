﻿
Imports System.ComponentModel
Namespace com.data

    Public Class order_h
        Inherits BaseDbFunctions
        Implements System.ComponentModel.INotifyPropertyChanged

        Public Event PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs) Implements ComponentModel.INotifyPropertyChanged.PropertyChanged
        Private Sub NotifyPropertyChanged(Optional ByVal propertyName As String = Nothing)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End Sub



        Public Property compid As Integer
            Get
                Return _compid
            End Get
            Set(value As Integer)
                _compid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _compid As Integer


        Public Property route As String
            Get
                Return _route
            End Get
            Set(value As String)
                _route = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _route As String


        Public Property order_no As Integer
            Get
                Return _order_no
            End Get
            Set(value As Integer)
                _order_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _order_no As Integer


        Public Property customer_no As String
            Get
                Return _customer_no
            End Get
            Set(value As String)
                _customer_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _customer_no As String


        Public Property orderdate As DateTime?
            Get
                Return _orderdate
            End Get
            Set(value As DateTime?)
                _orderdate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _orderdate As DateTime?


        Public Property orderpo As String
            Get
                Return _orderpo
            End Get
            Set(value As String)
                _orderpo = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _orderpo As String


        Public Property ship_to As String
            Get
                Return _ship_to
            End Get
            Set(value As String)
                _ship_to = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _ship_to As String


        Public Property ship_date As DateTime?
            Get
                Return _ship_date
            End Get
            Set(value As DateTime?)
                _ship_date = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _ship_date As DateTime?


        Public Property comins As String
            Get
                Return _comins
            End Get
            Set(value As String)
                _comins = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _comins As String


        Public Property creditnote As String
            Get
                Return _creditnote
            End Get
            Set(value As String)
                _creditnote = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _creditnote As String


        Public Property totalproductos As Integer?
            Get
                Return _totalproductos
            End Get
            Set(value As Integer?)
                _totalproductos = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _totalproductos As Integer?


        Public Property totalcajas As Integer?
            Get
                Return _totalcajas
            End Get
            Set(value As Integer?)
                _totalcajas = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _totalcajas As Integer?


        Public Property totalweight As Decimal?
            Get
                Return _totalweight
            End Get
            Set(value As Decimal?)
                _totalweight = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _totalweight As Decimal?


        Public Property totalvolume As Decimal?
            Get
                Return _totalvolume
            End Get
            Set(value As Decimal?)
                _totalvolume = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _totalvolume As Decimal?


        Public Property amount As Decimal?
            Get
                Return _amount
            End Get
            Set(value As Decimal?)
                _amount = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _amount As Decimal?


        Public Property signature As String
            Get
                Return _signature
            End Get
            Set(value As String)
                _signature = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _signature As String


        Public Property signname As String
            Get
                Return _signname
            End Get
            Set(value As String)
                _signname = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _signname As String


        Public Property status As Integer?
            Get
                Return _status
            End Get
            Set(value As Integer?)
                _status = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _status As Integer?


        Public Property trans_status As Integer?
            Get
                Return _trans_status
            End Get
            Set(value As Integer?)
                _trans_status = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _trans_status As Integer?


        Public Property newname As String
            Get
                Return _newname
            End Get
            Set(value As String)
                _newname = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newname As String


        Public Property newaddress1 As String
            Get
                Return _newaddress1
            End Get
            Set(value As String)
                _newaddress1 = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newaddress1 As String


        Public Property newaddress2 As String
            Get
                Return _newaddress2
            End Get
            Set(value As String)
                _newaddress2 = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newaddress2 As String


        Public Property newcity As String
            Get
                Return _newcity
            End Get
            Set(value As String)
                _newcity = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newcity As String


        Public Property newstate As String
            Get
                Return _newstate
            End Get
            Set(value As String)
                _newstate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newstate As String


        Public Property newzipcode As String
            Get
                Return _newzipcode

            End Get
            Set(value As String)
                _newzipcode = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newzipcode As String


        Public Property newphone As String
            Get
                Return _newphone
            End Get
            Set(value As String)
                _newphone = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newphone As String


        Public Property transtype As Integer?
            Get
                Return _transtype
            End Get
            Set(value As Integer?)
                _transtype = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _transtype As Integer?


        Public Property whsid As String
            Get
                Return _whsid
            End Get
            Set(value As String)
                _whsid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _whsid As String


        Public Property fobid As String
            Get
                Return _fobid
            End Get
            Set(value As String)
                _fobid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _fobid As String


        Public Property supplierNumber As String
            Get
                Return _suppliernumber
            End Get
            Set(value As String)
                _suppliernumber = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _suppliernumber As String


        Public Property supplierCustomerNumber As String
            Get
                Return _suppliercustomernumber
            End Get
            Set(value As String)
                _suppliercustomernumber = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _suppliercustomernumber As String


        Public Property createdDate As DateTime?
            Get
                Return _createddate
            End Get
            Set(value As DateTime?)
                _createddate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _createddate As DateTime?


        Public Property currencyId As Integer
            Get
                Return _currencyid
            End Get
            Set(value As Integer)
                _currencyid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _currencyid As Integer


        Public Property currencyRate As Decimal
            Get
                Return _currencyrate
            End Get
            Set(value As Decimal)
                _currencyrate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _currencyrate As Decimal


        Public Property printCounter As Integer
            Get
                Return _printcounter
            End Get
            Set(value As Integer)
                _printcounter = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _printcounter As Integer


        Public Property paymentTermId As Integer
            Get
                Return _paymenttermid
            End Get
            Set(value As Integer)
                _paymenttermid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _paymenttermid As Integer


        Public Property exportOrderCounter As Integer?
            Get
                Return _exportordercounter
            End Get
            Set(value As Integer?)
                _exportordercounter = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _exportordercounter As Integer?


        Public Property shippedComplete As Boolean
            Get
                Return _shippedcomplete
            End Get
            Set(value As Boolean)
                _shippedcomplete = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _shippedcomplete As Boolean


        Public Property creditno As Integer
            Get
                Return _creditno
            End Get
            Set(value As Integer)
                _creditno = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _creditno As Integer


        Public Property vigenciaLicencia As DateTime
            Get
                Return _vigencialicencia
            End Get
            Set(value As DateTime)
                _vigencialicencia = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _vigencialicencia As DateTime


        Public Property NumLicencia As String
            Get
                Return _numlicencia
            End Get
            Set(value As String)
                _numlicencia = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _numlicencia As String


        Public Property totalSalesTax As Decimal
            Get
                Return _totalsalestax
            End Get
            Set(value As Decimal)
                _totalsalestax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _totalsalestax As Decimal


        Public Property amountpaid As Decimal
            Get
                Return _amountpaid
            End Get
            Set(value As Decimal)
                _amountpaid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _amountpaid As Decimal


        Public Property collectionid As Integer
            Get
                Return _collectionid
            End Get
            Set(value As Integer)
                _collectionid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _collectionid As Integer


        Public Property transdate As DateTime
            Get
                Return _transdate
            End Get
            Set(value As DateTime)
                _transdate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _transdate As DateTime


        Public Property dueDate As DateTime?
            Get
                Return _duedate
            End Get
            Set(value As DateTime?)
                _duedate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _duedate As DateTime?


        Public Property signatureImage As Byte()
            Get
                Return _signatureimage
            End Get
            Set(value As Byte())
                _signatureimage = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _signatureimage As Byte()


        Public Property Hold As String
            Get
                Return _hold
            End Get
            Set(value As String)
                _hold = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _hold As String


        Public Property Prevtranstype As Integer?
            Get
                Return _prevtranstype
            End Get
            Set(value As Integer?)
                _prevtranstype = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _prevtranstype As Integer?


        Public Property Msfclientuser As String
            Get
                Return _msfclientuser
            End Get
            Set(value As String)
                _msfclientuser = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _msfclientuser As String


        Public Property Approvedate As DateTime?
            Get
                Return _approvedate
            End Get
            Set(value As DateTime?)
                _approvedate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _approvedate As DateTime?


        Public Property cityTax As Decimal
            Get
                Return _citytax
            End Get
            Set(value As Decimal)
                _citytax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _citytax As Decimal


        Public Property stateTax As Decimal
            Get
                Return _statetax
            End Get
            Set(value As Decimal)
                _statetax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _statetax As Decimal


        Public Property discountPercent As String
            Get
                Return _discountpercent
            End Get
            Set(value As String)
                _discountpercent = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _discountpercent As String


        Public Property systemid As Integer?
            Get
                Return _systemid
            End Get
            Set(value As Integer?)
                _systemid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _systemid As Integer?


        Public Property interruptiondetail As String
            Get
                Return _interruptiondetail
            End Get
            Set(value As String)
                _interruptiondetail = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _interruptiondetail As String


        Public Property interruptioncode As String
            Get
                Return _interruptioncode
            End Get
            Set(value As String)
                _interruptioncode = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _interruptioncode As String


        Public Property wostatus As String
            Get
                Return _wostatus
            End Get
            Set(value As String)
                _wostatus = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _wostatus As String


        Public Property workordernum As String
            Get
                Return _workordernum
            End Get
            Set(value As String)
                _workordernum = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _workordernum As String


        Public Property comentariocliente As String
            Get
                Return _comentariocliente
            End Get
            Set(value As String)
                _comentariocliente = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _comentariocliente As String


        Public Property paymenttype As String
            Get
                Return _paymenttype
            End Get
            Set(value As String)
                _paymenttype = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _paymenttype As String


        Public Property payreferenceno As String
            Get
                Return _payreferenceno
            End Get
            Set(value As String)
                _payreferenceno = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _payreferenceno As String


        Public Property authorizationcode As String
            Get
                Return _authorizationcode
            End Get
            Set(value As String)
                _authorizationcode = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _authorizationcode As String


        Public Property cancelreasonid As Integer
            Get
                Return _cancelreasonid
            End Get
            Set(value As Integer)
                _cancelreasonid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _cancelreasonid As Integer


        Public Property dsinvoiceid As String
            Get
                Return _dsinvoiceid
            End Get
            Set(value As String)
                _dsinvoiceid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _dsinvoiceid As String


        Public Property exportdate As DateTime
            Get
                Return _exportdate
            End Get
            Set(value As DateTime)
                _exportdate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _exportdate As DateTime


        Public Property exportuser As String
            Get
                Return _exportuser
            End Get
            Set(value As String)
                _exportuser = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _exportuser As String


        Public Property dsbatchid As String
            Get
                Return _dsbatchid
            End Get
            Set(value As String)
                _dsbatchid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _dsbatchid As String


        Public Property newDBA As String
            Get
                Return _newdba
            End Get
            Set(value As String)
                _newdba = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newdba As String


        Public Property newregion As String
            Get
                Return _newregion
            End Get
            Set(value As String)
                _newregion = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newregion As String


        Public Property newzone As String
            Get
                Return _newzone
            End Get
            Set(value As String)
                _newzone = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newzone As String


        Public Property newcertestatal As String
            Get
                Return _newcertestatal
            End Get
            Set(value As String)
                _newcertestatal = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newcertestatal As String


        Public Property newcertmunicipal As String
            Get
                Return _newcertmunicipal
            End Get
            Set(value As String)
                _newcertmunicipal = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _newcertmunicipal As String


        Public Property departamento As String
            Get
                Return _departamento
            End Get
            Set(value As String)
                _departamento = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _departamento As String


        Public Property usopersonal As String
            Get
                Return _usopersonal
            End Get
            Set(value As String)
                _usopersonal = value

                NotifyPropertyChanged()
            End Set
        End Property
        Private _usopersonal As String


        Public Property creditmode As String
            Get
                Return _creditmode
            End Get
            Set(value As String)
                _creditmode = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _creditmode As String


        Public Property creditType As String
            Get
                Return _credittype
            End Get
            Set(value As String)
                _credittype = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _credittype As String


        Public Property creditReference As String
            Get
                Return _creditreference
            End Get
            Set(value As String)
                _creditreference = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _creditreference As String


        Public Property serverPricingStatus As Integer?
            Get
                Return _serverpricingstatus
            End Get
            Set(value As Integer?)
                _serverpricingstatus = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _serverpricingstatus As Integer?


        Public Property erpReference1 As String
            Get
                Return _erpreference1
            End Get
            Set(value As String)
                _erpreference1 = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _erpreference1 As String


        Public Property hasMessages As Boolean
            Get
                Return _hasmessages
            End Get
            Set(value As Boolean)
                _hasmessages = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _hasmessages As Boolean


        Public Property hasErrorMessages As Boolean
            Get
                Return _haserrormessages
            End Get
            Set(value As Boolean)
                _haserrormessages = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _haserrormessages As Boolean


        Public Property imgTrans_Status As Integer?
            Get
                Return _imgtrans_status
            End Get
            Set(value As Integer?)
                _imgtrans_status = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _imgtrans_status As Integer?


        Public Property visitid As String
            Get
                Return _visitid
            End Get
            Set(value As String)
                _visitid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _visitid As String


        Public Property ncfnumber As String
            Get
                Return _ncfnumber
            End Get
            Set(value As String)
                _ncfnumber = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _ncfnumber As String


        Public Property badCreditSubTotal As Decimal?
            Get
                Return _badcreditsubtotal
            End Get
            Set(value As Decimal?)
                _badcreditsubtotal = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _badcreditsubtotal As Decimal?


        Public Property badCreditTotalStateTax As Decimal?
            Get
                Return _badcredittotalstatetax
            End Get
            Set(value As Decimal?)
                _badcredittotalstatetax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _badcredittotalstatetax As Decimal?


        Public Property badCreditTotalMunicipalTax As Decimal?
            Get
                Return _badcredittotalmunicipaltax
            End Get
            Set(value As Decimal?)
                _badcredittotalmunicipaltax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _badcredittotalmunicipaltax As Decimal?


        Public Property goodCreditSubTotal As Decimal?
            Get
                Return _goodcreditsubtotal
            End Get
            Set(value As Decimal?)
                _goodcreditsubtotal = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _goodcreditsubtotal As Decimal?


        Public Property goodCreditTotalStateTax As Decimal?
            Get
                Return _goodcredittotalstatetax
            End Get
            Set(value As Decimal?)
                _goodcredittotalstatetax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _goodcredittotalstatetax As Decimal?


        Public Property goodCreditTotalMunicipalTax As Decimal?
            Get
                Return _goodcredittotalmunicipaltax
            End Get
            Set(value As Decimal?)
                _goodcredittotalmunicipaltax = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _goodcredittotalmunicipaltax As Decimal?


        Public Property invoiceGrossTotal As Decimal?
            Get
                Return _invoicegrosstotal
            End Get
            Set(value As Decimal?)
                _invoicegrosstotal = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _invoicegrosstotal As Decimal?

    End Class

End Namespace
