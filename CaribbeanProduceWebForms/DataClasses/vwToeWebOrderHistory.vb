﻿
Imports System.ComponentModel
Namespace com.data

    Public Class vwToeWebOrderHistory
        Inherits BaseDBFunctions
        Implements System.ComponentModel.INotifyPropertyChanged

        Public Event PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs) Implements ComponentModel.INotifyPropertyChanged.PropertyChanged
        Private Sub NotifyPropertyChanged(Optional ByVal propertyName As String = Nothing)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End Sub



        Public Property compid As Integer
            Get
                Return _compid
            End Get
            Set(value As Integer)
                _compid = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _compid As Integer


        Public Property route As String
            Get
                Return _route
            End Get
            Set(value As String)
                _route = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _route As String


        Public Property Order_No As Integer
            Get
                Return _order_no
            End Get
            Set(value As Integer)
                _order_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _order_no As Integer


        Public Property Customer_No As String
            Get
                Return _customer_no
            End Get
            Set(value As String)
                _customer_no = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _customer_no As String


        Public Property ship_to As String
            Get
                Return _ship_to
            End Get
            Set(value As String)
                _ship_to = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _ship_to As String


        Public Property OrderDate As DateTime?
            Get
                Return _orderdate
            End Get
            Set(value As DateTime?)
                _orderdate = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _orderdate As DateTime?


        Public Property CustomerName As String
            Get
                Return _customername
            End Get
            Set(value As String)
                _customername = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _customername As String


        Public Property ShipToName As String
            Get
                Return _shiptoname
            End Get
            Set(value As String)
                _shiptoname = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _shiptoname As String

    End Class

End Namespace
