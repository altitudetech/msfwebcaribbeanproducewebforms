﻿
Imports System.ComponentModel
Namespace com.data

    Public Class vwToeWebProducts
        Inherits BaseDbFunctions
        Implements System.ComponentModel.INotifyPropertyChanged

        Public Event PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs) Implements ComponentModel.INotifyPropertyChanged.PropertyChanged
        Private Sub NotifyPropertyChanged(Optional ByVal propertyName As String = Nothing)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End Sub

        Public Property Customer As String
            Get
                Return _customer
            End Get
            Set(value As String)
                _customer = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _customer As String


        Public Property ShipTo As String
            Get
                Return _shipto
            End Get
            Set(value As String)
                _shipto = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _shipto As String


        Public Property Product As String
            Get
                Return _product
            End Get
            Set(value As String)
                _product = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _product As String


        Public Property ProdDesc As String
            Get
                Return _proddesc
            End Get
            Set(value As String)
                _proddesc = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _proddesc As String


        Public Property UM As String
            Get
                Return _um
            End Get
            Set(value As String)
                _um = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _um As String


        Public Property UPC As String
            Get
                Return _upc
            End Get
            Set(value As String)
                _upc = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _upc As String


        Public Property DivCode As String
            Get
                Return _divcode
            End Get
            Set(value As String)
                _divcode = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _divcode As String


        Public Property DivDesc As String
            Get
                Return _divdesc
            End Get
            Set(value As String)
                _divdesc = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _divdesc As String


        Public Property DivSortIndex As Integer
            Get
                Return _divsortindex
            End Get
            Set(value As Integer)
                _divsortindex = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _divsortindex As Integer


        Public Property ClassCode As String
            Get
                Return _classcode
            End Get
            Set(value As String)
                _classcode = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _classcode As String


        Public Property ClassDesc As String
            Get
                Return _classdesc
            End Get
            Set(value As String)
                _classdesc = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _classdesc As String


        Public Property ProductLine As String
            Get
                Return _productline
            End Get
            Set(value As String)
                _productline = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _productline As String


        Public Property LineDesc As String
            Get
                Return _linedesc
            End Get
            Set(value As String)
                _linedesc = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _linedesc As String


        Public Property ToeUser As String
            Get
                Return _toeuser
            End Get
            Set(value As String)
                _toeuser = value
                NotifyPropertyChanged()
            End Set
        End Property
        Private _toeuser As String

    End Class

End Namespace
