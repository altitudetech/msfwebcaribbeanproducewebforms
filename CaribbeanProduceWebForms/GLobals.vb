﻿Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Web
Imports Microsoft.Win32.SafeHandles

Module Globals

    Public vwToeWebProductsClass As New com.data.vwToeWebProducts
    Public vwToeWebProductsForDetailClass As New com.data.vwToeWebProducts
    Public vwToeWebProductsButtons As New com.data.vwToeWebProducts

    Public oDataManagement As New com.ISSData(My.Settings.DatabaseName)
    Public oSettings As New DataSet
    Public MainConfigFile As String = "config.iss"


    <System.Runtime.CompilerServices.Extension>
    Public Function DataTableToList(Of T As {Class, New})(table As DataTable) As List(Of T)
        Try
            Dim list As New List(Of T)()

            For Each row In table.AsEnumerable()
                Dim obj As New T()

                For Each prop In obj.[GetType]().GetProperties()
                    Try
                        Dim [propInfo] As PropertyInfo = obj.[GetType]().GetProperty(prop.Name)
                        ' propertyInfo.SetValue(obj, Convert.ChangeType(row(prop.Name), propertyInfo.PropertyType), Nothing)
                        If row(prop.Name) IsNot DBNull.Value Then [propInfo].SetValue(obj, row(prop.Name), Nothing)
                    Catch
                        Continue For
                    End Try
                Next

                list.Add(obj)
            Next

            Return list
        Catch
            Return Nothing
        End Try
    End Function



    'Public Sub ExecuteQueryNoResults(ByVal cmdIn As String)
    '    'SQLConnect()
    '    Dim connectionString As String = My.Settings. 'ConfigurationManager.ConnectionStrings("ISS").ConnectionString
    '    Dim cmd As New System.Data.SqlClient.SqlCommand(cmdIn, New System.Data.SqlClient.SqlConnection(connectionString))
    '    cmd.CommandType = CommandType.Text
    '    cmd.CommandTimeout = 600
    '    cmd.Connection.Open()
    '    cmd.ExecuteNonQuery()
    '    cmd.Connection.Close()
    '    cmd.Dispose()
    'End Sub

    'Public Function ExecuteQueryGetResults(ByVal cmdIn As String) As DataTable
    '    'SQLConnect()
    '    Dim connectionString As String = ConfigurationManager.ConnectionStrings("ISS").ConnectionString
    '    Dim cmd As New System.Data.SqlClient.SqlCommand(cmdIn, New System.Data.SqlClient.SqlConnection(connectionString))
    '    cmd.CommandType = CommandType.Text
    '    cmd.CommandTimeout = 600
    '    cmd.Connection.Open()
    '    cmd.ExecuteReader()
    '    cmd
    '    cmd.Connection.Close()
    '    cmd.Dispose()
    'End Function
End Module


' Design pattern for a base class.
Public MustInherit Class Base
    Implements IDisposable
    Dim handle As SafeHandle = New SafeFileHandle(IntPtr.Zero, True)
    Private disposed As Boolean = False
    Private instName As String
    'Private trackingList As List(Of Object)

    Public Sub New()
        Dim oDataClass As Object = Me
        '**** Get Class Name - Excluding 
        Dim type As Type = oDataClass.[GetType]()
        MyClass.instName = type.Name
        'trackingList = tracking
        'trackingList.Add(Me)
    End Sub

    Public ReadOnly Property InstanceName() As String
        Get
            Return instName
        End Get
    End Property

    'Implement IDisposable.
    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        '  Console.WriteLine(vbNewLine + "[{0}].Base.Dispose()", instName)
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(disposing As Boolean)
        If disposed = False Then
            If disposing Then
                handle.Dispose()
                ' Free other state (managed objects).
                '  Console.WriteLine("[{0}].Base.Dispose(true)", instName)
                ' trackingList.Remove(Me)
                ' Console.WriteLine("[{0}] Removed from tracking list: {1:x16}",
                'InstanceName, MyClass.GetHashCode())
            Else
                '  Console.WriteLine("[{0}].Base.Dispose(false)", instName)
            End If
            disposed = True
        End If
    End Sub

    Protected Overrides Sub Finalize()
        ' Simply call Dispose(False).
        ' Console.WriteLine(vbNewLine + "[{0}].Base.Finalize()", instName)
        Dispose(False)
    End Sub
End Class
