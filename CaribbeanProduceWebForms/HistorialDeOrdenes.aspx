﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.master" CodeBehind="HistorialDeOrdenes.aspx.vb" Inherits="CaribbeanProduceWebForms.HistorialDeOrdenes" %>

<%@ Register Assembly="DevExpress.Web.Bootstrap.v17.1, Version=17.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>



<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <p align="center">
        <h2 class="form-signin-heading" style="text-align: center; background-color: #2182C9; color: white; height: 60px; vertical-align: central; text-align: center; padding-top: 10px">Historial de Órdenes</h2>
    </p>
    </div>
   
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Historial de Ordenes" ShowCollapseButton="true" Width="100%" HorizontalAlign="Center" Theme="iOS" View="GroupBox" ShowHeader="False">


        <PanelCollection>
            <dx:PanelContent runat="server">

                <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" EnableTheming="True" Theme="iOS" KeyFieldName="Order_No" OnCustomButtonCallback="ASPxGridView1_CustomButtonCallback">
                   <ClientSideEvents
                        EndCallback="function OnEndCallback(s, e) {
                      
                       }"
                       />
                   
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Fecha" FieldName="OrderDate" ShowInCustomizationForm="True" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="No. de Orden" FieldName="Order_No" ShowInCustomizationForm="True" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Tienda" FieldName="ShipToName" ShowInCustomizationForm="True" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
<%--                        <dx:GridViewCommandColumn VisibleIndex="10" Caption="Download PDF" ButtonType="Image" Width="5px">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton ID="PDF" Visibility="AllDataRows" Image-Width="20" Image-Url="~/images/pdf.png"
                                    Image-Height="20">
                                    <Image Height="20px" Width="20px">
                                    </Image>
                                    <Styles>
                                        <Style HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                            </CustomButtons>
                            <%-- <HeaderStyle BackColor="White" Border-BorderColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />--%>
                          <%--  <CellStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle">
                            </CellStyle>
                        </dx:GridViewCommandColumn>--%>


                          <dx:GridViewCommandColumn VisibleIndex="11" Caption=" " ButtonType="Button" Width="5px" ButtonRenderMode="Button">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton ID="ViewOrder" Visibility="AllDataRows" Text="Ver Orden"  >
                                    <Styles>
                                        <Style HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                            </CustomButtons>
                            <%-- <HeaderStyle BackColor="White" Border-BorderColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />--%>
                            <CellStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle">
                            </CellStyle>
                        </dx:GridViewCommandColumn>


                        <dx:GridViewCommandColumn VisibleIndex="11" Caption="Copiar Productos a Orden" ButtonType="Button" Width="5px" ButtonRenderMode="Button">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton ID="CopyToOrder" Visibility="AllDataRows" Text="Copiar Productos"  >
                                    <Styles>
                                        <Style HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                            </CustomButtons>
                            <%-- <HeaderStyle BackColor="White" Border-BorderColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />--%>
                            <CellStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle">
                            </CellStyle>
                        </dx:GridViewCommandColumn>
                    </Columns>
                </dx:ASPxGridView>

                <%--    <asp:GridView ID="GridView1" Width="100%" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center">
        <Columns>
            <asp:BoundField HeaderText="Fecha de Orden" DataField="FechaDeOrden" />
            <asp:BoundField HeaderText="Numero de Orden" DataField="NumeroDeOrden"  />
            <asp:ButtonField ButtonType="Image" ImageUrl="~/Images/pdf.png">
           
            </asp:ButtonField>
            <asp:ButtonField ButtonType="Button" Text="Añadir Productos" />
        </Columns>
    </asp:GridView>--%>
            </dx:PanelContent>
        </PanelCollection>


    </dx:ASPxRoundPanel>
     <dx:ASPxPopupControl ID="ASPxPopupControl1" CloseAction="None" ShowCloseButton="false" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server" Font-Size="20px" HeaderText="Confirmacion" Text="Usted desea agregar productos a orden nueva o orden actual?" ClientInstanceName="popup" Width="550px">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <table style="margin-top:30px">
                    <tr>
                        <td>
                            <dx:ASPxButton Style="margin: auto; align-content: center; align-items: center; align-self: center" ID="btnNewOrder" HorizontalAlign="Center" Width="250px" Font-Size="18px" runat="server" Text="Orden Nueva" AutoPostBack="false">
                            </dx:ASPxButton>
                        </td>
                        <td></td>
                        <td>
                            <dx:ASPxButton Style="margin: auto; align-content: center; align-items: center; align-self: center" ID="btnCurrentOrder" HorizontalAlign="Center" Width="250px" Font-Size="18px" runat="server" Text="Orden Actual" AutoPostBack="false">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>


            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>


     <dx:ASPxPopupControl ID="AspxOrderViewPopUp"  CloseAction="CloseButton" ShowCloseButton="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server" Font-Size="20px" HeaderText="Orden" Text="" ClientInstanceName="ViewOrderPopup" Width="800px">
        <ContentCollection>
            <dx:PopupControlContentControl>
               <asp:Literal ID="MyLiteral" runat="server" />

            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
