﻿Imports DevExpress.Web

Public Class HistorialDeOrdenes
    Inherits System.Web.UI.Page
    Public SelectedOrder As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.User.Identity.IsAuthenticated Then
            FormsAuthentication.RedirectToLoginPage()
        End If
        Session("PreviousPage") = My.Settings.Root + "Options.aspx"


        If Session("CurrentAction") = "ShowPopUp" Then
            ASPxPopupControl1.ShowOnPageLoad = True
            Session("CurrentAction") = ""
        End If

        If Session("ViewOrderData") <> "" Then
            MyLiteral.Text = Session("ViewOrderData")
            AspxOrderViewPopUp.ShowOnPageLoad = True
            Session("ViewOrderData") = ""
        End If
        'Dim dt As New DataTable
        'dt.Columns.Add(New DataColumn("FechaDeOrden"))
        'dt.Columns.Add(New DataColumn("NumeroDeOrden"))
        'dt.Columns.Add(New DataColumn("CopiaPDF"))


        'For i = 1 To 10
        '    Dim dRow As DataRow
        '    dRow = dt.NewRow
        '    dRow("FechaDeOrden") = Now.ToShortDateString
        '    dRow("NumeroDeOrden") = i
        '    dt.Rows.Add(dRow)
        '    dt.AcceptChanges()
        'Next i
        'now bind datatable to gridview... 
        Dim orderHistClass As New com.data.vwToeWebOrderHistory
        orderHistClass.FillBySpecificColumnAndID("route", Session("CurrentUser"))
        Dim dt As DataTable = orderHistClass.dt

        ASPxGridView1.DataSource = dt
        ASPxGridView1.DataBind()

        'With GridView1
        '    For i = 0 To .Rows.Count - 1
        '        Dim Roll_No As Label = DirectCast(.Rows(i).Cells(1).FindControl("txtRoll_No"), Label)
        '        Dim ID_Number As Label = DirectCast(.Rows(i).Cells(2).FindControl("txtID_Number"), Label)
        '        Roll_No.Text = i + 1
        '        ID_Number.Text = "2014-01-" & i + 1
        '    Next
        'End With
    End Sub

    Protected Sub ASPxGridView1_CustomButtonCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs)

        ' Dim btn As GridViewCommandColumnCustomButton
        ' btn = sender
        If e.ButtonID = "CopyToOrder" Then
            Dim CurrentSelectedItem = sender.GetRowValues(e.VisibleIndex, "Order_No")
            SelectedOrder = CurrentSelectedItem
            Session("SelectedOrder") = SelectedOrder
            If Session("OrderDetails") IsNot Nothing Then
                ASPxPopupControl1.ShowOnPageLoad = True
                Session("CurrentAction") = "ShowPopUp"
                Try
                    Response.Redirect(My.Settings.Root + "HistorialDeOrdenes.aspx")
                Catch ex As Exception
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback(My.Settings.Root + "HistorialDeOrdenes.aspx")
                End Try
            Else
                btnNewOrder_Click(Nothing, Nothing)
            End If
        Else
            Dim CurrentSelectedItem = sender.GetRowValues(e.VisibleIndex, "Order_No")
            Using sql As New SqlClient.SqlConnection(oDataManagement.GetConnectionString)
                sql.Open()
                Using cmd As New SqlClient.SqlCommand()
                    cmd.Connection = sql

                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = "Select  dbo.GetMSFWebOrderConfirmation('1','" & Session("CurrentUser") & "','" & CurrentSelectedItem & "') from Order_h"
                    Dim myReturnValue As String = cmd.ExecuteScalar
                    Session("ViewOrderData") = myReturnValue

                    Try

                        Response.Redirect(My.Settings.Root + "HistorialDeOrdenes.aspx")
                    Catch ex As Exception
                        DevExpress.Web.ASPxWebControl.RedirectOnCallback(My.Settings.Root + "HistorialDeOrdenes.aspx")
                    End Try
                    'MyLiteral.Text = myReturnValue
                    'AspxOrderViewPopUp.ShowOnPageLoad = True
                End Using
            End Using
        End If
    End Sub

    Protected Sub btnNewOrder_Click(sender As Object, e As EventArgs) Handles btnNewOrder.Click

        Dim order As New com.data.ToeWebUserOrders
        order.DeleteByColumnAndID("Username", Session("CurrentUser"))
        order.Dispose()

        Dim CopyOrder As New com.data.vwToeWebOrderHistoryProducts

        Session("OrderDetails") = Nothing
        Session("ShipTo") = Nothing

        Session("ShipToName") = Nothing
        Session("ShipDate") = Nothing
        DirectCast(Session("WS"), wsToeWeb).SetCounter(0)
        'Session.Clear()
        Session("PreviousPage") = My.Settings.Root + "Options.aspx"
        'Response.Redirect(My.Settings.Root + "Options.aspx")

        '********* Get all Products from Order_D
        '**** LOAD DATA IF PREVIOUS ORDER IS ON DB
        'Dim Order As New com.data.ToeWebUserOrders
        Dim SumQty As Integer = 0
        CopyOrder.FillByColumnArrayAndValueArray({"Username", "Order_No"}, {Session("CurrentUser"), Session("SelectedOrder")})
        If CopyOrder.dt Is Nothing Then Exit Sub
        If CopyOrder.dt.Rows.Count <> 0 Then
            Dim dt As New DataTable




            'dt.Columns.Add(New DataColumn("ID"))
            dt.Columns.Add(New DataColumn("Order_No"))
            dt.Columns.Add(New DataColumn("Product_No"))
            dt.Columns.Add(New DataColumn("QTY"))
            dt.Columns.Add(New DataColumn("Unit_Price"))
            dt.Columns.Add(New DataColumn("Ext_Price"))
            dt.Columns.Add(New DataColumn("UM"))
            dt.Columns.Add(New DataColumn("Description"))
            Dim primaryKey(1) As DataColumn
            primaryKey(0) = dt.Columns("Product_No")
            dt.PrimaryKey = primaryKey
            If Session("OrderDetails") Is Nothing Then
                Session("OrderDetails") = dt
                'For i = 1 To 10
                '    Dim dRow As DataRow
                '    dRow = dt.NewRow
                '    dRow("FechaDeOrden") = Now.ToShortDateString
                '    dRow("NumeroDeOrden") = i
                '    dt.Rows.Add(dRow)
                '    dt.AcceptChanges()
                'Next i
            Else
                'Dim cDt As DataTable = Session("OrderDetails")
                dt = Session("OrderDetails")
                dt.Clear()

            End If

            For I = 0 To CopyOrder.dt.Rows.Count - 1
                Dim OrdRow As DataRow = dt.NewRow
                OrdRow("Order_No") = CopyOrder.dt.Rows(I)("Order_No").ToString
                OrdRow("Product_No") = CopyOrder.dt.Rows(I)("Product_No").ToString
                OrdRow("Qty") = CopyOrder.dt.Rows(I)("QTY").ToString
                OrdRow("Description") = CopyOrder.dt.Rows(I)("Description").ToString
                SumQty += CInt(OrdRow("Qty").ToString)
                OrdRow("Unit_Price") = CopyOrder.dt.Rows(I)("Unit_Price").ToString
                OrdRow("Ext_Price") = CopyOrder.dt.Rows(I)("Ext_Price").ToString
                OrdRow("UM") = CopyOrder.dt.Rows(I)("UM").ToString
                'Session("ShipDate") = CDate(CopyOrder.dt.Rows(I)("ShipDate").ToString)

                'Session("ShipTo") = CopyOrder.dt.Rows(I)("ShipTo").ToString


                'Session("ShipToName") = CopyOrder.dt.Rows(I)("ShipToName").ToString


                dt.Rows.Add(OrdRow)

                dt.AcceptChanges()

            Next
            If Session("WS") Is Nothing Then
                Session("WS") = New wsToeWeb    'Master.Pages.Root.webServiceCounter
            End If
            DirectCast(Session("WS"), wsToeWeb).SetCounter(SumQty)
            Session("OrderDetails") = dt
            Try
                Response.Redirect(My.Settings.Root + "OrderHeader.aspx")
            Catch ex As Exception
                DevExpress.Web.ASPxWebControl.RedirectOnCallback(My.Settings.Root + "OrderHeader.aspx")
            End Try

            '    Response.Redirect(My.Settings.Root + "OrderHeader.aspx")
        End If


    End Sub

    Protected Sub btnCurrentOrder_Click(sender As Object, e As EventArgs) Handles btnCurrentOrder.Click



        Dim CopyOrder As New com.data.vwToeWebOrderHistoryProducts


        DirectCast(Session("WS"), wsToeWeb).SetCounter(0)
        'Session.Clear()

        'Response.Redirect(My.Settings.Root + "Options.aspx")

        '********* Get all Products from Order_D
        '**** LOAD DATA IF PREVIOUS ORDER IS ON DB
        'Dim Order As New com.data.ToeWebUserOrders
        Dim SumQty As Integer = 0
        CopyOrder.FillByColumnArrayAndValueArray({"Username", "Order_No"}, {Session("CurrentUser"), Session("SelectedOrder")})
        If CopyOrder.dt Is Nothing Then Exit Sub




        If CopyOrder.dt.Rows.Count <> 0 Then
            Dim dt As New DataTable



            dt = Session("OrderDetails")

            For I = 0 To CopyOrder.dt.Rows.Count - 1

                Dim OrdRow As DataRow
                Dim tmpRow() As DataRow
                tmpRow = dt.Select("Product_No= '" & CopyOrder.dt.Rows(I)("Product_No").ToString & "'")
                If tmpRow Is Nothing Then
                    OrdRow = dt.NewRow
                    OrdRow("Qty") = CopyOrder.dt.Rows(I)("QTY").ToString
                Else
                    OrdRow = tmpRow(0)
                    OrdRow("Qty") = CInt(OrdRow("Qty")) + CInt(CopyOrder.dt.Rows(I)("QTY").ToString)
                End If
                OrdRow("Order_No") = CopyOrder.dt.Rows(I)("Order_No").ToString
                OrdRow("Product_No") = CopyOrder.dt.Rows(I)("Product_No").ToString

                OrdRow("Description") = CopyOrder.dt.Rows(I)("Description").ToString

                OrdRow("Unit_Price") = CopyOrder.dt.Rows(I)("Unit_Price").ToString
                OrdRow("Ext_Price") = CopyOrder.dt.Rows(I)("Ext_Price").ToString
                OrdRow("UM") = CopyOrder.dt.Rows(I)("UM").ToString
                Session("ShipDate") = CDate(CopyOrder.dt.Rows(I)("ShipDate").ToString)

                Session("ShipTo") = CopyOrder.dt.Rows(I)("ShipTo").ToString


                Session("ShipToName") = CopyOrder.dt.Rows(I)("ShipToName").ToString

                If tmpRow Is Nothing Then
                    dt.Rows.Add(OrdRow)

                Else

                End If

                dt.AcceptChanges()

            Next
            For x = 0 To dt.Rows.Count - 1
                SumQty += CInt(dt(x)("Qty").ToString)
            Next

            If Session("WS") Is Nothing Then
                Session("WS") = New wsToeWeb    'Master.Pages.Root.webServiceCounter
            End If
            DirectCast(Session("WS"), wsToeWeb).SetCounter(SumQty)
            Session("OrderDetails") = dt
            Session("PreviousPage") = My.Settings.Root + "Options.aspx"
            ASPxPopupControl1.ShowOnPageLoad = False
            'Response.Redirect(My.Settings.Root + "ProductSearch.aspx")
            Try
                Response.Redirect(My.Settings.Root + "OrderHeader.aspx")
            Catch ex As Exception
                DevExpress.Web.ASPxWebControl.RedirectOnCallback(My.Settings.Root + "OrderHeader.aspx")
            End Try

        End If
    End Sub
End Class