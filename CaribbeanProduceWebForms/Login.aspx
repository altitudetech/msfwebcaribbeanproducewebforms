﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="CaribbeanProduceWebForms.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
.content {
    max-width: 500px;
    margin: auto;
}
</style>
</head>

<body>
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css'
    media="screen" />
<form id="form1" runat="server">
<div style="max-width: 400px;margin:0 auto;margin-top:100px ">
    <div style="text-align: center"> <img alt=""  class="auto-style1" src="Images/Logo.jpg" /></div>
    <br />

    <h2 class="form-signin-heading" style="text-align: center">
        </h2>
    <br />
     <br />
     <br />
     <br />
     <br />
    <label for="txtUsername" style="font-size:22px">
        Username</label>
    <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" placeholder="Enter Username" Height="60px" Font-Size="22px"
        required />
    <br />
    <label for="txtPassword"  style="font-size:22px">
        Password</label>
    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control" Height="60px" Font-Size="22px"
        placeholder="Enter Password" required />
   <%-- <div class="checkbox">
        <asp:CheckBox ID="chkRememberMe"  Text="Remember Me" runat="server" Font-Size="22px" Height="60px" />
    </div>--%>
    <br />
    <br />
    <br />
    
    <asp:Button ID="btnLogin" Text="Login" runat="server" Class="btn btn-primary" OnClick="ValidateUser" Font-Size="22px" Height="60px" Width="400px" />
    <br />
    <br />
    <div id="dvMessage" runat="server" visible="false" class="alert alert-danger">
        <strong>Error!</strong>
        <asp:Label ID="lblMessage" runat="server" />
    </div>
</div>
</form>
</body>
</html>
