﻿Imports System.Data.SqlClient

Public Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.Clear()
        'If Me.Page.User.Identity.IsAuthenticated Then
        '    Response.Redirect(FormsAuthentication.DefaultUrl)
        'End If
    End Sub

    Protected Sub ValidateUser(sender As Object, e As EventArgs)
        Dim userId As String = ""
        Dim username As String = txtUsername.Text.Trim()
        Dim password As String = txtPassword.Text.Trim()
        Dim constr As String = "Data Source=" & My.Settings.DatabaseServer + ";Initial Catalog=" & My.Settings.DatabaseName + ";Persist Security Info=True;User ID=" & My.Settings.DbUserName + ";Password=" + My.Settings.DbPassword + ";" 'ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("Validate_User")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Username", username)
                cmd.Parameters.AddWithValue("@Password", password)
                cmd.Connection = con
                con.Open()
                userId = cmd.ExecuteScalar() ' Convert.ToInt32(cmd.ExecuteScalar())
                con.Close()
            End Using

            Select Case userId
                Case -1
                    dvMessage.Visible = True
                    lblMessage.Text = "Username and/or password is incorrect."
                    Exit Select
                Case -2
                    dvMessage.Visible = True
                    lblMessage.Text = "Account has not been activated."
                    Exit Select
                Case Else
                    Session("CurrentUser") = username

                    'AJCO - se incluyo campo UserID en el query para poder usarlo en el momento que se grabe la orden.
                    '*** GET USER ROLE
                    Dim dt As DataTable = oDataManagement.Execute("Select Role, UserID from Toewebusers where username = '" & username & "'")

                    'If Not String.IsNullOrEmpty(Request.QueryString("ReturnUrl")) Then
                    ' FormsAuthentication.SetAuthCookie(username, False) 'chkRememberMe.Checked)
                    ' Response.Redirect(Request.QueryString("ReturnUrl"))
                    ' Else
                    FormsAuthentication.RedirectFromLoginPage(username, True) ' chkRememberMe.Checked)

                    'AJCO - Se asigna el UserID del usuario a un Session variable para posterior referencia al grabar la orden.
                    Session("UserID") = dt.Rows(0)("UserID")

                    If dt.Rows(0)("Role") = "D" Then
                        Response.Redirect(My.Settings.Root + "UserManagement.aspx")
                    Else
                        Response.Redirect(My.Settings.Root + "Options.aspx")
                    End If

                    'End If
                    Exit Select
            End Select
            'Select Case userId
            '    Case -1
            '        Login1.FailureText = "Username and/or password is incorrect."
            '        Exit Select
            '    Case -2
            '        Login1.FailureText = "Account has not been activated."
            '        Exit Select
            '    Case Else
            '        FormsAuthentication.RedirectFromLoginPage(userId, Login1.RememberMeSet)
            '        Exit Select
            'End Select
        End Using
    End Sub



    'Protected Sub ValidateUser(sender As Object, e As EventArgs)
    '    Dim username As String = txtUsername.Text.Trim()
    '    Dim password As String = txtPassword.Text.Trim()
    '    Dim userId As Integer = 0
    '    Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
    '    Using con As New SqlConnection(constr)
    '        Using cmd As New SqlCommand("Validate_User")
    '            cmd.CommandType = CommandType.StoredProcedure
    '            cmd.Parameters.AddWithValue("@Username", username)
    '            cmd.Parameters.AddWithValue("@Password", password)
    '            cmd.Connection = con
    '            con.Open()
    '            userId = Convert.ToInt32(cmd.ExecuteScalar())
    '            con.Close()
    '        End Using
    '        Select Case userId
    '            Case -1
    '                dvMessage.Visible = True
    '                lblMessage.Text = "Username and/or password is incorrect."
    '                Exit Select
    '            Case -2
    '                dvMessage.Visible = True
    '                lblMessage.Text = "Account has not been activated."
    '                Exit Select
    '            Case Else
    '                If Not String.IsNullOrEmpty(Request.QueryString("ReturnUrl")) Then
    '                    FormsAuthentication.SetAuthCookie(username, chkRememberMe.Checked)
    '                    Response.Redirect(Request.QueryString("ReturnUrl"))
    '                Else
    '                    FormsAuthentication.RedirectFromLoginPage(username, chkRememberMe.Checked)
    '                End If
    '                Exit Select
    '        End Select
    '    End Using
    'End Sub

End Class