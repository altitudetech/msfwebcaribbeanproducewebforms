﻿Imports System.Data.SqlClient

Public Class Options
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.User.Identity.IsAuthenticated Then
            FormsAuthentication.RedirectToLoginPage()
        End If
        If Session("CurrentUser") Is Nothing Then
            FormsAuthentication.SignOut()
            FormsAuthentication.RedirectToLoginPage()
        End If
        If Session("GlobalQty") Is Nothing Then
            Session("GlobalQty") = 0

        End If

        'AJCO - se cambio el Previous Page Login.aspx, para funcionalidad de Logout.
        'Session("PreviousPage") = My.Settings.Root + "Options.aspx"
        'Session.Clear()
        Session("PreviousPage") = My.Settings.Root + "login.aspx"

        '**** LOAD DATA IF PREVIOUS ORDER IS ON DB
        Dim Order As New com.data.ToeWebUserOrders
        Dim SumQty As Integer = 0
        Order.FillBySpecificColumnAndID("Username", Session("CurrentUser"))
        If Order.dt Is Nothing Then Exit Sub
        If Order.dt.Rows.Count <> 0 Then
            Dim dt As New DataTable




            'dt.Columns.Add(New DataColumn("ID"))
            dt.Columns.Add(New DataColumn("Order_No"))
            dt.Columns.Add(New DataColumn("Product_No"))
            dt.Columns.Add(New DataColumn("QTY"))
            dt.Columns.Add(New DataColumn("Unit_Price"))
            dt.Columns.Add(New DataColumn("Ext_Price"))
            dt.Columns.Add(New DataColumn("UM"))
            dt.Columns.Add(New DataColumn("Description"))
            Dim primaryKey(1) As DataColumn
            primaryKey(0) = dt.Columns("Product_No")
            dt.PrimaryKey = primaryKey
            If Session("OrderDetails") Is Nothing Then
                Session("OrderDetails") = dt
                'For i = 1 To 10
                '    Dim dRow As DataRow
                '    dRow = dt.NewRow
                '    dRow("FechaDeOrden") = Now.ToShortDateString
                '    dRow("NumeroDeOrden") = i
                '    dt.Rows.Add(dRow)
                '    dt.AcceptChanges()
                'Next i
            Else
                'Dim cDt As DataTable = Session("OrderDetails")
                dt = Session("OrderDetails")
                dt.Clear()

            End If

            For I = 0 To Order.dt.Rows.Count - 1
                Dim OrdRow As DataRow = dt.NewRow
                OrdRow("Order_No") = Order.dt.Rows(I)("Order_No").ToString
                OrdRow("Product_No") = Order.dt.Rows(I)("Product_No").ToString
                OrdRow("Qty") = Order.dt.Rows(I)("QTY").ToString
                OrdRow("Description") = Order.dt.Rows(I)("Description").ToString
                SumQty += CInt(OrdRow("Qty").ToString)
                OrdRow("Unit_Price") = Order.dt.Rows(I)("Unit_Price").ToString
                OrdRow("Ext_Price") = Order.dt.Rows(I)("Ext_Price").ToString
                OrdRow("UM") = Order.dt.Rows(I)("UM").ToString
                Session("ShipDate") = CDate(Order.dt.Rows(I)("ShipDate").ToString)

                Session("ShipTo") = Order.dt.Rows(I)("ShipTo").ToString


                Session("ShipToName") = Order.dt.Rows(I)("ShipToName").ToString


                dt.Rows.Add(OrdRow)

                dt.AcceptChanges()

            Next
            If Session("WS") Is Nothing Then
                Session("WS") = New wsToeWeb    'Master.Pages.Root.webServiceCounter
            End If
            DirectCast(Session("WS"), wsToeWeb).SetCounter(SumQty)
            Session("OrderDetails") = dt
            Response.Redirect(My.Settings.Root + "OrderHeader.aspx")
        End If


        'Dim lbl As Object = Page.Master.FindControl("Counter")
        'lbl.Value = Session("GlobalQty")

    End Sub

    Protected Sub btnCrearOrdenNueva2_Click(sender As Object, e As EventArgs) Handles btnCrearOrdenNueva2.Click
        Response.Redirect(My.Settings.Root + "OrderHeader.aspx")
    End Sub

    Protected Sub btnHistorialDeOrdenes2_Click(sender As Object, e As EventArgs) Handles btnHistorialDeOrdenes2.Click
        Response.Redirect(My.Settings.Root + "HistorialDeOrdenes.aspx")
    End Sub

End Class