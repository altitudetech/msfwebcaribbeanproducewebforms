﻿Imports System.Data.SqlClient

Public Class OrderHeader
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.Page.User.Identity.IsAuthenticated Then
            FormsAuthentication.RedirectToLoginPage()

        End If

        If ShipTo.SelectedItem IsNot Nothing Then
            If Session("ShipToName") <> ShipTo.SelectedItem.Text Then

            End If
        End If
        'If Not (IsPostBack) Then
        Dim qry As String = "Select  Distinct Shipto as ship_to, ShipTo.name as name, Shipto.Customer_no as customer_no from vwToeWebProducts inner join shipto On shipto.Ship_to = vwToeWebProducts.ShipTo
  where ToeUser = '" & Session("CurrentUser") & "'"
            Dim dt As DataTable = oDataManagement.Execute(qry)
        ShipTo.ValueField = "ship_to"
        ShipTo.TextField = "name"
        ShipTo.DataSource = dt
            ShipTo.DataBind()
        'End If
        If Session("PreviousPage") = My.Settings.Root + "ProductsForOrder.aspx" Or Session("PreviousPage") = My.Settings.Root + "OrderHeader.aspx" Then
            If Session("OrderDetails") IsNot Nothing Then
                ASPxPopupControl1.CloseOnEscape = False
                ASPxPopupControl1.ShowOnPageLoad = True
            End If
        End If

        If Session("GlobalQty") Is Nothing Then
            Session("GlobalQty") = 0

        End If
        Dim sundayDate = GetNextWeekday(Now, DayOfWeek.Sunday) 'DateTime.Today.AddDays((DateTime.Today.DayOfWeek - DayOfWeek.Monday))
        ShipDate.MaxDate = sundayDate
        If sundayDate.Date = Today.Date Then
            ShipDate.MaxDate = DateAdd(DateInterval.Day, 1, ShipDate.MaxDate)
        ElseIf Today.DayOfWeek = DayOfWeek.Saturday Then
            ShipDate.MaxDate = DateAdd(DateInterval.Day, 2, Today.Date)
        End If
        'Dim lbl As Object = Page.Master.FindControl("Counter")
        'lbl.Value = Session("GlobalQty")
        Session("PreviousPage") = My.Settings.Root + "Options.aspx"
        ShipDate.MinDate = DateTime.Now
        Dim TheTime As DateTime = Now.ToLongTimeString

        qry = "Select value from globalcontrol2 where name = 'WEB_CUTOFF_TIME';"
        Dim gc2 As DataTable = oDataManagement.Execute(qry)
        Dim cutOffTime As DateTime = Convert.ToDateTime("2:00:00 PM").ToLongTimeString
        If gc2 IsNot Nothing And gc2.Rows.Count > 0 Then
            cutOffTime = Convert.ToDateTime(gc2.Rows(0)("value")).ToLongTimeString
        End If

        If TheTime > cutOffTime Then
            ShipDate.MinDate = DateAdd(DateInterval.Day, 1, DateTime.Now)
        End If

        If ShipDate.MinDate = ShipDate.MaxDate Then
            ShipDate.MaxDate = DateAdd(DateInterval.Day, 1, ShipDate.MaxDate)
        End If


        If Not IsPostBack Then
            If Session("ShipDate") IsNot Nothing Then
                ShipDate.Value = Session("ShipDate")
            End If
            If Session("ShipTo") IsNot Nothing Then
                ShipTo.Value = Session("ShipTo")
                ShipTo.Text = Session("ShipToName")
            End If

        End If

    End Sub
    Public Shared Function GetNextWeekday(ByVal start As DateTime, ByVal day As DayOfWeek) As DateTime
        Dim daysToAdd As Integer = (CInt(day) - CInt(start.DayOfWeek) + 7) Mod 7
        Return start.AddDays(daysToAdd)
    End Function
    Private Sub ShipDate_ValueChanged(sender As Object, e As EventArgs) Handles ShipDate.ValueChanged
        Session("ShipDate") = ShipDate.Value
    End Sub

    Private Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        If ShipTo.Text = "" Then Exit Sub

        Session("ShipDate") = ShipDate.Value
        If ShipTo.SelectedItem IsNot Nothing Then
            Session("ShipTo") = ShipTo.Value 'ShipTo.SelectedItem.GetFieldValue("ship_to")
            Session("ShipToName") = ShipTo.SelectedItem.GetFieldValue("name")
            Session("CustomerNo") = ShipTo.SelectedItem.GetFieldValue("customer_no")
        End If
        Response.Redirect("~/ProductsForOrder.aspx")
    End Sub

    Protected Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        ASPxPopupControl1.ShowOnPageLoad = False
    End Sub

    Protected Sub btnBorrar_Click(sender As Object, e As EventArgs) Handles btnBorrar.Click

        Dim order As New com.data.ToeWebUserOrders
        order.DeleteByColumnAndID("Username", Session("CurrentUser"))
        order.Dispose()
        Session("OrderDetails") = Nothing
        Session("ShipTo") = Nothing

        Session("ShipToName") = Nothing
        Session("ShipDate") = Nothing
        DirectCast(Session("WS"), wsToeWeb).SetCounter(0)
        'Session.Clear()
        Session("PreviousPage") = My.Settings.Root + "Options.aspx"
        Response.Redirect(My.Settings.Root + "Options.aspx")
    End Sub



    Sub ExecuteQuery(sSql As String)
        Dim constr As String = "Data Source=" & My.Settings.DatabaseServer & ";Initial Catalog=" & My.Settings.DatabaseName & ";Persist Security Info=True;User ID=" & My.Settings.DbUserName & ";Password=" & My.Settings.DbPassword & ";" 'ConfigurationManager.ConnectionStrings("constr").ConnectionString

        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(sSql)
                cmd.CommandType = CommandType.Text

                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
                cmd.Dispose()

            End Using
            con.Dispose()
        End Using

    End Sub
    Function GetQueryResults(sSql As String) As String
        Dim constr As String = "Data Source=" & My.Settings.DatabaseServer & ";Initial Catalog=" & My.Settings.DatabaseName & ";Persist Security Info=True;User ID=" & My.Settings.DbUserName & ";Password=" & My.Settings.DbPassword & ";" 'ConfigurationManager.ConnectionStrings("constr").ConnectionString

        ' Dim constr As String = "Data Source=70.35.204.129,1475;Initial Catalog=msfservercaribbeanproducetest;Persist Security Info=True;User ID=sa;Password=Itg2016;" 'ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Dim retval As String
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(sSql)
                cmd.CommandType = CommandType.Text

                cmd.Connection = con
                con.Open()
                retval = cmd.ExecuteScalar()
                con.Close()
                cmd.Dispose()

            End Using
            con.Dispose()
        End Using
        Return retval
    End Function


    Protected Sub ShipTo_ValueChanged1(sender As Object, e As EventArgs)
        Session("ShipTo") = ShipTo.SelectedItem.GetFieldValue("ship_to")
        Session("ShipToName") = ShipTo.SelectedItem.GetFieldValue("name")
        Session("CustomerNo") = ShipTo.SelectedItem.GetFieldValue("customer_no")
    End Sub


    Protected Sub ShipTo_SelectedIndexChanged1(sender As Object, e As EventArgs)
        Session("ShipTo") = ShipTo.SelectedItem.GetFieldValue("ship_to")
        Session("ShipToName") = ShipTo.SelectedItem.GetFieldValue("name")
        Session("CustomerNo") = ShipTo.SelectedItem.GetFieldValue("customer_no")
    End Sub
    'Private Sub ShipTo_SelectedIndexChanged(sender As Object, e As EventArgs) 'Handles ShipTo.SelectedIndexChanged

    'End Sub
End Class