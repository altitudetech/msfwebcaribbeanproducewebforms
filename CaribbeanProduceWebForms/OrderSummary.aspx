﻿<%@ Page Language="vb" MasterPageFile="~/Root.master" AutoEventWireup="false" CodeBehind="OrderSummary.aspx.vb" Inherits="CaribbeanProduceWebForms.OrderSummary" %>

<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript">
     var visibleIndex;
     function OnCustomButtonClick(s, e) {
         visibleIndex = e.visibleIndex;
         popup.Show();
     }
     function OnClickYes(s, e) {
        /// grid.DeleteRow(visibleIndex);
         popup.Hide();
         window.location('Options.aspx')
     }
     function OnClickNo(s, e) {
         popup.Hide();
     }
        </script>
    <div style="max-width: 630px; margin: 0 auto;">

        <p align="center">
            <h2 class="form-signin-heading" style="margin:auto;align-content: center;width:630px; background-color: #2182C9; color: white; height: 60px; vertical-align: central; text-align: center; padding-top: 10px;align-self:center;align-self:center">RESUMEN DE ORDEN</h2>
        </p>
    </div>
    <%--<dx:ASPxRoundPanel Border-BorderStyle="None" Width="600px" 
        style="align-content:center;align-items:center;align-self:center;margin:auto" ID="ASPxRoundPanel1" runat="server" HeaderText="TEST" ShowCollapseButton="true" 
         HorizontalAlign="Center" Theme="iOS" View="Standard" ShowHeader="True">


        <PanelCollection>
            <dx:PanelContent runat="server">--%>
                <dx:ASPxGridView ClientInstanceName="GridView" style="align-content:center;align-items:center;align-self:center;margin:auto" ID="ASPxGridView1" runat="server" Theme="iOS" Width="630px" AutoGenerateColumns="False" KeyFieldName="Product_No" Border-BorderStyle="None" OnCustomButtonCallback="ASPxGridView1_CustomButtonCallback">
                         <ClientSideEvents CustomButtonClick="function(s, e) {
                        GridView.UpdateEdit();
                        
                        }
                        " />

                    <SettingsPager Mode="ShowPager" ShowNumericButtons="True" PageSize="9">
                        <Summary Visible="False" />
                    </SettingsPager>
                    <Settings ShowStatusBar="Hidden" />
                        <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
                    <Columns>
                        <dx:GridViewDataTextColumn Width="80px" FieldName="Product_No" ShowInCustomizationForm="True" VisibleIndex="0" Caption="Producto" ReadOnly="true">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Width="200px" FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="1" Caption="Descripcion" ReadOnly="true">
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataSpinEditColumn FieldName="QTY" Caption="Cantidad" VisibleIndex="3" Width="30px" CellStyle-HorizontalAlign="Center" >
                            <PropertiesSpinEdit MinValue="0" MaxValue="10000" NumberType="Integer" />
                        </dx:GridViewDataSpinEditColumn>
                        <dx:GridViewDataTextColumn Caption="UM" FieldName="UM" Width="10px" ShowInCustomizationForm="True" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                          <dx:GridViewCommandColumn Caption=" ">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton  Image-Height="20" Image-Url="~/images/Actualizar.png" Image-Width="20" Visibility="AllDataRows">
                                                            <Image Height="30px" Width="100px">
                                                            </Image>
                                    
                                                        </dx:GridViewCommandColumnCustomButton>
                            
                            </CustomButtons>
                              
                        </dx:GridViewCommandColumn>
                       <%--   <dx:GridViewCommandColumn VisibleIndex="10" Caption=" " ButtonType="Image" Width="1px">
                                <CustomButtons>
                                    <dx:GridViewCommandColumnCustomButton ID="Add" Visibility="AllDataRows" Image-Width="20" Image-Url="~/images/AddItem-CPE.png"
                                        Image-Height="20">
                                        <Image Height="20px" Width="20px">
                                        </Image>
                                        <Styles>
                                            <Style HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </Style>
                                        </Styles>
                                    </dx:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                                <HeaderStyle BackColor="White" Border-BorderColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dx:GridViewCommandColumn>
                            <dx:GridViewCommandColumn VisibleIndex="11" Caption=" " ButtonType="Image"  >
                                <CustomButtons>
                                    <dx:GridViewCommandColumnCustomButton ID="Remove" Visibility="AllDataRows" Image-Width="20px" Image-Url="~/images/RemoveItem-CPE.png"
                                        Image-Height="20">
                                        <Image Height="20px" Width="20px" />
                                    </dx:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                                <HeaderStyle BackColor="White" Border-BorderColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="True">
                                </CellStyle>
                            </dx:GridViewCommandColumn>--%>
                    </Columns>
                     <SettingsEditing Mode="Batch" />
                </dx:ASPxGridView>

        <%--    </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>--%>
    <dx:ASPxRoundPanel ID="ASPxRoundPanel2"  style="align-content:center;align-items:center;align-self:center;margin:auto" runat="server" HeaderText="Notes" ShowCollapseButton="true" Width="630px" HorizontalAlign="Center" Theme="iOS" View="Standard" ShowHeader="True">
        <HeaderTemplate>
                <dx:ASPxImage ID="headerImage" runat="server" ImageUrl="~/Images/Notes.png" Height="40px"></dx:ASPxImage>
                <dx:ASPxLabel ID="headerText" runat="server" Text='NOTAS' Font-Size="20px" >
                </dx:ASPxLabel>
            </HeaderTemplate>

        <HeaderStyle Font-Size="20px" />


        <PanelCollection>
            <dx:PanelContent runat="server">
               <%-- <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Width="170px"></dx:ASPxTextBox>--%>
                <dx:ASPxMemo runat="server" style="width: 100%;resize:none;" id="Comments" rows="5" Theme="iOS" ></dx:ASPxMemo>
            </dx:PanelContent>
        </PanelCollection>

    </dx:ASPxRoundPanel>

    <dx:ASPxRoundPanel ID="ASPxRoundPanel3" style="align-content:center;align-items:center;align-self:center;margin:auto" runat="server" HeaderText="Notes" ShowCollapseButton="true" Width="630px" HorizontalAlign="Center" Theme="iOS" View="Standard" ShowHeader="False">


        <HeaderStyle Font-Size="20px" />


        <PanelCollection>
            <dx:PanelContent runat="server">
                <div>
                    <div style="float: left;">
                       <%--<dx:ASPxButton ID="btnEmailOrder" runat="server" Text="E-Mail Orden"  Width="200px" Height="60px" Font-Size="24pt"></dx:ASPxButton>--%>
                        <dx:ASPxTextBox runat="server" ID="txtAutorizadoPor" NullText="Autorizado Por"  Width="300px" Height="60px" Font-Size="18pt"></dx:ASPxTextBox>
                    </div>
                    <div style="float: right">
                        <dx:ASPxButton ID="btnSubmitOrder" runat="server" Text="Someter Orden" Width="200px" Height="60px" Font-Size="24pt"></dx:ASPxButton>
                    </div>
                    <div style="clear: both"></div>
                </div>
                <%--  <dx:ASPxButton ID="btnEmailOrder" runat="server" Text="Email Order"></dx:ASPxButton>
                <dx:ASPxButton ID="btnSubmitOrder" runat="server" Text="Submit Order"></dx:ASPxButton>--%>
            </dx:PanelContent>
        </PanelCollection>

    </dx:ASPxRoundPanel>

     <dx:ASPxPopupControl ID="ASPxPopupControl1" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server" Font-Size="20px"  HeaderText="Confirmacion" Text="Su orden ha sido enviada! Gracias!" ClientInstanceName="popup" Width="400px">
                <ContentCollection >
                    <dx:PopupControlContentControl>
                        <table  style="width:100%;auto;align-content:center;align-items:center;align-self:center;margin-top:40px">
<tr>
    <td>
        <dx:ASPxButton style="margin:auto;align-content:center;align-items:center;align-self:center" ID="yesButton" HorizontalAlign="Center" runat="server" Text="Ok!" Width="100%" AutoPostBack="false">
                            <%--<ClientSideEvents Click="OnClickYes" />--%>
                        </dx:ASPxButton>
    </td>
</tr>
                        

                            
                        </table>
                       <%-- <dx:ASPxButton ID="noButton" runat="server" Text="No" AutoPostBack="false">
                            <ClientSideEvents Click="OnClickNo" />
                        </dx:ASPxButton>--%>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:ASPxPopupControl>

     <dx:ASPxPopupControl ID="ASPxPopupControl2" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server" Font-Size="20px"  HeaderText="Confirmacion" Text="Su orden ha sido enviada! Gracias!" ClientInstanceName="popup" Width="400px">
                <ContentCollection >
                    <dx:PopupControlContentControl>
                        <table  style="width:100%;auto;align-content:center;align-items:center;align-self:center;margin-top:40px">
<tr>
    <td>
        <dx:ASPxButton style="margin:auto;align-content:center;align-items:center;align-self:center" ID="btnFechaInvalida" HorizontalAlign="Center" runat="server" Text="Ok!" Width="100%" AutoPostBack="false">
                            <%--<ClientSideEvents Click="OnClickYes" />--%>
                        </dx:ASPxButton>
    </td>
</tr>
                        

                            
                        </table>
                       <%-- <dx:ASPxButton ID="noButton" runat="server" Text="No" AutoPostBack="false">
                            <ClientSideEvents Click="OnClickNo" />
                        </dx:ASPxButton>--%>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:ASPxPopupControl>
</asp:Content>
