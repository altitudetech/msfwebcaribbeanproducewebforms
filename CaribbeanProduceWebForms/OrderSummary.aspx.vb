﻿Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.Data

Public Class OrderSummary
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.User.Identity.IsAuthenticated Then
            FormsAuthentication.RedirectToLoginPage()
        End If
        If Session("ShipTo") Is Nothing Or Session("ShipTo") = "" Or Session("ShipDate") Is Nothing Then Response.Redirect(My.Settings.Root + "OrderHeader.aspx")
        Session("PreviousPage") = My.Settings.Root + "ProductsForOrder.aspx"
        If Session("GlobalQty") Is Nothing Then
            Session("GlobalQty") = 0

        End If
        'Dim lbl As HiddenField = Page.Master.FindControl("Counter")
        'lbl.Value = Session("GlobalQty")

        'Dim lbl As HtmlGenericControl = Page.Master.FindControl("itemcount")


        'lbl.Attributes("data-count") = Session("GlobalQty")

        If Session("OrderDetails") IsNot Nothing Then
            ASPxGridView1.DataSource = Session("OrderDetails")
            ASPxGridView1.DataBind()
        End If

    End Sub


    Protected Sub ASPxGridView1_CustomButtonCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs)

        If Session("GlobalQty") Is Nothing Then Session("GlobalQty") = 0
        'Dim CurrentSelectedItem = sender.GetRowValues(e.VisibleIndex, "description")
        Dim CurrentSelectedItemKey = sender.GetRowValues(e.VisibleIndex, "Product_No")
        Dim CurrentSelectedItemQty As Integer = sender.GetRowValues(e.VisibleIndex, "QTY")
        'Dim CurrentSelectedItemUM As String = sender.GetRowValues(e.VisibleIndex, "UnitOfMeasure")
        'Session("SelectedClass") = CurrentSelectedItem
        If e.ButtonID = "Add" Then
            CurrentSelectedItemQty += 1
            Session("GlobalQty") += 1

        ElseIf e.ButtonID = "Remove" Then
            If CurrentSelectedItemQty > 0 Then CurrentSelectedItemQty -= 1
            Session("GlobalQty") -= 1
        End If
        DirectCast(Session("WS"), wsToeWeb).SetCounter(Session("GlobalQty"))


        Dim s As DevExpress.Web.ASPxGridView

        Dim gridView As DevExpress.Web.ASPxGridView = DirectCast(sender, DevExpress.Web.ASPxGridView)

        Dim dataTable As DataTable = Session("OrderDetails")
        'Dim row As DataRow = dataTable.Rows.Find(CurrentSelectedItemKey)

        'Row(2) = CurrentSelectedItemQty


        '**** Verify Item In OrderDetail Datatable

        Dim OrdRow As DataRow = dataTable.Rows.Find(CurrentSelectedItemKey)
        If OrdRow IsNot Nothing Then
            OrdRow(2) = CurrentSelectedItemQty
            If CurrentSelectedItemQty = 0 Then
                OrdRow.Delete()
            End If
            'Else

            '    OrdRow = Dt.NewRow
            '    OrdRow("Order_No") = ""
            '    OrdRow("Product_No") = CurrentSelectedItemKey
            '    OrdRow("Qty") = CurrentSelectedItemQty
            '    OrdRow("Description") = CurrentSelectedItem

            '    OrdRow("Unit_Price") = 0
            '    OrdRow("Ext_Price") = 0
            '    OrdRow("UM") = CurrentSelectedItemUM

            '    Dt.Rows.Add(OrdRow)
        End If
        dataTable.AcceptChanges()
        'sender.DataSource = Session("GridData")

        '*** REAPPLY FILTERS
        'If Session("SearchVariable") <> "FINDALL" Or txtSearch2.Text <> "" Then txtSearch2_TextChanged(Nothing, Nothing)
        sender.DataBind()
        SavePersistentOrderChangesToDB()
        'Dim lbl As HiddenField = Page.Master.FindControl("Counter")
        'lbl.Value = Session("GlobalQty")
        'Dim lbl As HtmlGenericControl = Page.Master.FindControl("itemcount")


        'lbl.Attributes("data-count") = Session("GlobalQty")

        '//Set the value to CONTENT PAGE label control.  
        'lbl.Attributes("data-count") = Session("GlobalQty")
        'Page.ClientScript.RegisterStartupScript(Me.Page.GetType(), "CallMyFunction", " $('#itemcount').attr('data-count', " & Session("GlobalQty").ToString & ");", True)

        'ClientScript.RegisterClientScriptBlock(Page.GetType, "DisableTimer", "$('#itemcount').attr('data-count',  " & Session("GlobalQty").ToString & ");")
        ''System.Web.UI.ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Script", "setCartQty(" & Session("GlobalQty") & ")", True)
        'System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page.Master, Page.Master.GetType(), "Script", "$('#itemcount').attr('data-count', " & Session("GlobalQty").ToString & ");", True)
        '' System.Web.UI.ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Script", "$('#itemcount').attr('data-count',  " & Session("GlobalQty").ToString & ");", True)

    End Sub

    Public Sub SavePersistentOrderChangesToDB()
        Dim Order As New com.data.ToeWebUserOrders
        Order.DeleteByColumnAndID("Username", Session("CurrentUser"))
        Dim Dt = Session("OrderDetails")
        For Each DR As DataRow In Dt.Rows
            Order = New com.data.ToeWebUserOrders
            Order.Order_No = DR("Order_No").ToString
            Order.Product_No = DR("Product_No").ToString
            Order.QTY = DR("Qty").ToString
            Order.Description = DR("Description").ToString

            Order.Unit_Price = DR("Unit_Price").ToString
            Order.Ext_Price = DR("Ext_Price").ToString
            Order.UM = DR("UM").ToString
            Order.Username = Session("CurrentUser").ToString
            Order.ShipDate = Session("ShipDate").ToString
            Order.ShipTo = Session("ShipTo").ToString
            Order.ShipToName = Session("ShipToName").ToString
            Order.Update()
            Order.Dispose()
        Next
    End Sub
    Private Sub btnSubmitOrder_Click(sender As Object, e As EventArgs) Handles btnSubmitOrder.Click
        Dim tdt As DataTable = Session("OrderDetails")
        If tdt.Rows.Count = 0 Then
            ASPxPopupControl2.HeaderText = "Productos"
            ASPxPopupControl2.Text = "No hay productos en la orden! Por favor seleccione al menos un producto!"
            ASPxPopupControl2.ShowOnPageLoad = True
            Exit Sub
        End If

        Dim theTime As DateTime
        theTime = Now.ToLongTimeString

        If theTime > #3:00:00 PM# And DateAdd(DateInterval.Day, 1, Today) = Session("ShipDate") Then
            ASPxPopupControl2.HeaderText = "Fecha de Entrega"
            ASPxPopupControl2.Text = "La fecha de entrega ya no es valida. Favor de seleccionar otra fecha!"
            ASPxPopupControl2.ShowOnPageLoad = True
            Exit Sub
        End If
        If txtAutorizadoPor.Text = "" Then
            ASPxPopupControl2.HeaderText = "Autorizado Por:"
            ASPxPopupControl2.Text = "Favor de entrar la persona que autoriza en el campo correspondiente!"
            ASPxPopupControl2.ShowOnPageLoad = True
            Exit Sub
        End If
        'AJCO - Nomenclatura para numero de orden de acuerdo a formato de iMSF (cuuuoooooo: c=#compania, u=userid registrado al usuario, o=consecutivo de la orden)
        Session("OrderNo") = GetQueryResults(" Select isnull((select max(order_no) + 1 from order_h where compid=1 And route='" & Session("CurrentUser") & "'), 1" & Session("UserID") & "000001)")

        Dim qry As String = "insert into order_h (compid, route, order_no, customer_no, orderdate, ship_to, ship_date, comins, totalproductos, amount, transtype,signname, trans_status) 
                    select
                    compid=1,
                    route='" & Session("CurrentUser") & "',
                    order_no=" + Session("OrderNo") + ",
                    customer_no='" + Session("CustomerNo") + "',
                    orderdate=getdate(),
                    ship_to='" + Session("ShipTo") + "',
                    ship_date='" + Session("ShipDate").ToString + "',
                    comins='" & Comments.Text & "',
                    totalproductos='" & Session("GlobalQty") & "',
                    amount=0,
                    transtype=90,
                    signname='" & txtAutorizadoPor.Text & "',
                    trans_status=150;"

        ExecuteQuery(qry)

        Dim dt As DataTable = Session("OrderDetails")
        For Each row As DataRow In dt.Rows
            qry = "insert into order_d (compid, route, order_no, product_no, qty, unit_price, ext_price, lotnumber, um, freegoodsval, shippedProduct, shippedQuantity, salesTax, ammount_change, Discount, OrgPrice, PriceAdjustment, modelno,
                commentcredit, updatedate, updateuser, hasMessages, hasErrorMessages)
                Select 
                compid = 1,
                route ='" & Session("CurrentUser") & "',
                order_no = " & Session("OrderNo") & ",
                product_no ='" & row("Product_No") & "',
                qty = " & row("QTY") & ",
                unit_price = 0,
                ext_price = 0,
                lotnumber ='',
                um = '" & row("UM") & "',
                freegoodsval = -1,
                shippedProduct = 0,
                shippedQuantity = 0,
                salesTax = 0,
                ammount_change = 0,
                Discount = 0,
                OrgPrice = 0,
                PriceAdjustment = 0,
                modelno ='',
                commentcredit ='',
                updatedate = getdate(),
                updateuser ='',
                hasMessages = 0,
                hasErrorMessages = 0;"

            ExecuteQuery(qry)
        Next

        qry = "update order_h set trans_status=200 where compid=1 and route='" & Session("CurrentUser") & "' and order_no=" & Session("OrderNo") & ";"
        ExecuteQuery(qry)

        ASPxPopupControl1.Text = "Su orden ha sido sometida! Gracias!"
        ASPxPopupControl1.ShowOnPageLoad = True
        Dim order As New com.data.ToeWebUserOrders
        order.DeleteByColumnAndID("Username", Session("CurrentUser"))
        order.Dispose()
        'Session.Clear()
        Session("OrderDetails") = Nothing
        Session("ShipTo") = Nothing

        Session("ShipToName") = Nothing
        Session("ShipDate") = Nothing

        DirectCast(Session("WS"), wsToeWeb).SetCounter(0)

    End Sub

    Sub ExecuteQuery(sSql As String)
        Dim constr As String = "Data Source=" & My.Settings.DatabaseServer & ";Initial Catalog=" & My.Settings.DatabaseName & ";Persist Security Info=True;User ID=" & My.Settings.DbUserName & ";Password=" & My.Settings.DbPassword & ";" 'ConfigurationManager.ConnectionStrings("constr").ConnectionString

        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(sSql)
                cmd.CommandType = CommandType.Text

                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
                cmd.Dispose()

            End Using
            con.Dispose()
        End Using

    End Sub
    Function GetQueryResults(sSql As String) As String
        Dim constr As String = "Data Source=" & My.Settings.DatabaseServer & ";Initial Catalog=" & My.Settings.DatabaseName & ";Persist Security Info=True;User ID=" & My.Settings.DbUserName & ";Password=" & My.Settings.DbPassword & ";" 'ConfigurationManager.ConnectionStrings("constr").ConnectionString

        ' Dim constr As String = "Data Source=70.35.204.129,1475;Initial Catalog=msfservercaribbeanproducetest;Persist Security Info=True;User ID=sa;Password=Itg2016;" 'ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Dim retval As String
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(sSql)
                cmd.CommandType = CommandType.Text

                cmd.Connection = con
                con.Open()
                retval = cmd.ExecuteScalar()
                con.Close()
                cmd.Dispose()

            End Using
            con.Dispose()
        End Using
        Return retval
    End Function

    Protected Sub yesButton_Click(sender As Object, e As EventArgs) Handles yesButton.Click
        Response.Redirect(My.Settings.Root + "Options.aspx")
    End Sub

    'Private Sub btnEmailOrder_Click(sender As Object, e As EventArgs) Handles btnEmailOrder.Click
    '    SendMail()
    '    ASPxPopupControl1.Text = "Su orden ha sido enviada al correo electronico especificado!"
    '    ASPxPopupControl1.ShowOnPageLoad = True
    'End Sub

    Public MessageTo As String
    Public Message As String
    Public Attachment1 As Byte(), Attachment2 As Byte()
    Public Location As String
    Public SendEmailThread As System.Threading.Thread
    Public SendEmailThread2 As System.Threading.Thread

    Sub SendMailThreaded()
        SendEmailThread = New System.Threading.Thread(AddressOf SendMail)
        SendEmailThread.Start()
    End Sub

    Public Sub SendMail()
        ' Exit Sub
        Try
            Dim SmtpServer As New Net.Mail.SmtpClient()
            Dim mail As New Net.Mail.MailMessage()
            SmtpServer.Credentials = New _
        Net.NetworkCredential("alerts@easycoppr.com", "escremin1234")
            SmtpServer.Port = 2500 '80 '25 '587
            SmtpServer.Host = "smtp.easycoppr.com"
            ' SmtpServer.EnableSsl = True



            mail = New Net.Mail.MailMessage()

            mail.From = New Net.Mail.MailAddress("alerts@easycoppr.com", "TOE WEB")



            mail.To.Add("7879086620@mms.att.net")
            mail.To.Add("vmaldonado@altitudetechnologies.net")
            mail.To.Add("achinea120@gmail.com")
            mail.Subject = "Informacion de Orden #" & Session("OrderNo")

            Dim plainView As Net.Mail.AlternateView = Net.Mail.AlternateView.CreateAlternateViewFromString("", Nothing, "text/plain")



            ''*** Parse Message Data
            'Dim spl()
            'spl = Split(Message, vbCrLf)
            Dim OrderNumber As String = Session("OrderNo")
            Dim OrderCustNo As String = Session("CustomerNo")
            Dim OrderShipTo As String = Session("ShipTo")
            Dim OrderDate As String = Session("ShipDate")
            Dim OrderProductsQTY = Session("GlobalQty")
            '******** HTML CODING
            Dim HtmlFormatData As String = ""






            HtmlFormatData += "<table width=""400"" height=""27"" border=""0"">"
            HtmlFormatData += "<tr><td width=""325""><img src=""cid:companylogo"" alt="" width=""410"" height=""100""  /></td>"
            HtmlFormatData += "<td width=""400"" align=""center""></td>  </tr></table>"
            HtmlFormatData += "Esto es un correo automatizado del sistema TOE WEB" + vbCrLf
            HtmlFormatData += "<br/><br/>"
            HtmlFormatData += "<h1>Information de Orden: #" & OrderNumber & "</h1>"
            HtmlFormatData += "<h2>Fecha: " & OrderNumber & "</h2>"
            'width="465" border="0" style="height: 206px"

            HtmlFormatData += " <table border=""2px"" width=""60%"" style=""border-collapse:collapse;"">
                                         <tr style=""backgroundskyblue; text-align:Left();"">
                                             <th># de Producto</th>
                                             <th>Descripcion</th>
                                             <th>UM</th>
                                             <th>Cantidad</th>
                                         </tr>"

            '(***** INSERT PRODUCTS HERE
            Dim dt As DataTable = Session("OrderDetails")
            For Each row As DataRow In dt.Rows

                HtmlFormatData += "<tr>
                                    <td>" & row("Product_No") & "</td>
                                    <td>" & row("Description") & "</td>
                                    <td>" & row("UM") & "</td>
                                    <td>" & row("QTY") & "</td>
                                    </tr>"

            Next







            HtmlFormatData += "  </table><br/><br/>"
            HtmlFormatData += "<br/><br/>"
            HtmlFormatData += "Disclaimer: The information transmitted is intended only for the person or entity to which it is " + vbCrLf +
                              "addressed and may contain confidential and/or privileged material. Any review, retransmission, dissemination or " + vbCrLf +
                              "other use of, or taking of any action in reliance upon this information by persons or entities other that the " + vbCrLf +
                              "intended recipient is strictly prohibited. If you received this in error, please contact the sender and delete the material from any computer."


            Dim htmlView As Net.Mail.AlternateView = Net.Mail.AlternateView.CreateAlternateViewFromString(HtmlFormatData, Nothing, "text/html") '& vbCrLf & "<img src=cid:platepic>", Nothing, "text/html")
            ' Dim logo2 As New Net.Mail.LinkedResource()
            Dim logo As New Net.Mail.LinkedResource(System.AppDomain.CurrentDomain.BaseDirectory + "images\logo.jpg")
            logo.ContentId = "companylogo"
            'add the LinkedResource to the appropriate view
            htmlView.LinkedResources.Add(logo)

            'Dim memStream As New MemoryStream()
            'If Not Attachment1 Is Nothing Then
            '    'Dim firstData As Byte() = Attachment1.GetBuffer
            '    memStream.Write(Attachment1, 0, Attachment1.Length)
            '    memStream.Seek(0, SeekOrigin.Begin)
            'End If
            'Dim memStream2 As New MemoryStream()

            ''Dim secondData As Byte() = Attachment2.GetBuffer
            'If Not Attachment2 Is Nothing Then
            '    memStream2.Write(Attachment2, 0, Attachment2.Length)
            '    memStream2.Seek(0, SeekOrigin.Begin)
            'End If

            'If Not Attachment1 Is Nothing Then
            '    Dim face As New Net.Mail.LinkedResource(memStream)
            '    face.ContentId = "face"
            '    'add the LinkedResource to the appropriate view
            '    htmlView.LinkedResources.Add(face)
            'End If
            'If Not Attachment2 Is Nothing Then
            '    Dim plate As New Net.Mail.LinkedResource(memStream2)
            '    plate.ContentId = "plate"
            '    'add the LinkedResource to the appropriate view
            '    htmlView.LinkedResources.Add(plate)
            'End If
            'add the views
            'mail.AlternateViews.Add(plainView)
            mail.AlternateViews.Add(htmlView)



            SmtpServer.Send(mail)


        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try



    End Sub

    Private Sub btnFechaInvalida_Click(sender As Object, e As EventArgs) Handles btnFechaInvalida.Click
        If ASPxPopupControl2.HeaderText = "Autorizado Por:" Or ASPxPopupControl2.HeaderText = "Productos" Then
            ASPxPopupControl2.ShowOnPageLoad = False
            Exit Sub
        End If

        Response.Redirect(My.Settings.Root + "OrderHeader.aspx")
    End Sub

    Private Sub ASPxGridView1_RowUpdating(sender As Object, e As ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim row As DataRow = DirectCast(Session("OrderDetails"), DataTable).Rows.Find(e.Keys("Product_No"))
        row("QTY") = e.NewValues("QTY")
        row.AcceptChanges()

        DirectCast(Session("OrderDetails"), DataTable).AcceptChanges()
        'row("Age") = e.NewValues("Age")
        e.Cancel = True



        Dim gridView As DevExpress.Web.ASPxGridView = DirectCast(ASPxGridView1, DevExpress.Web.ASPxGridView)

        If Session("GlobalQty") Is Nothing Then Session("GlobalQty") = 0

        Dim CurrentSelectedItemKey = e.Keys("Product_No")
        Dim KeyIndex As String = gridView.FindVisibleIndexByKeyValue(CurrentSelectedItemKey)
        Dim CurrentSelectedItemQty As Integer = e.NewValues("QTY") 'sender.GetRowValues(KeyIndex, "Quantity")
        Dim CurrentSelectedItem = sender.GetRowValues(KeyIndex, "Description")
        Dim CurrentSelectedItemUM As String = sender.GetRowValues(KeyIndex, "UM")



        Dim s As DevExpress.Web.ASPxGridView



        ' Dim dataTable As DataTable = Session("OrderDetails")
        ' Dim _row As DataRow = dataTable.Rows.Find(CurrentSelectedItemKey)

        '_row(2) = CurrentSelectedItemQty



        '**** Verify Item In OrderDetail Datatable
        Dim Dt = Session("OrderDetails")
        Dim OrdRow As DataRow = Dt.Rows.Find(CurrentSelectedItemKey)
        If OrdRow IsNot Nothing Then
            OrdRow(2) = CurrentSelectedItemQty
            If CurrentSelectedItemQty = 0 Then
                OrdRow.Delete()
            End If
        Else

            OrdRow = Dt.NewRow
            OrdRow("Order_No") = ""
            OrdRow("Product_No") = CurrentSelectedItemKey
            OrdRow("Qty") = CurrentSelectedItemQty
            OrdRow("Description") = CurrentSelectedItem

            OrdRow("Unit_Price") = 0
            OrdRow("Ext_Price") = 0
            OrdRow("UM") = CurrentSelectedItemUM

            Dt.Rows.Add(OrdRow)
        End If
        Dt.AcceptChanges()



        Dim SumQty = 0

        For I = 0 To Dt.Rows.Count - 1

            SumQty += CInt(Dt.Rows(I)("Qty").ToString)


        Next
        If Session("WS") Is Nothing Then
            Session("WS") = New wsToeWeb    'Master.Pages.Root.webServiceCounter
        End If
        DirectCast(Session("WS"), wsToeWeb).SetCounter(SumQty)

        '***** Clear DB Order Values and Save all Again


        'sender.DataSource = Session("GridData")

        '*** REAPPLY FILTERS
        ' If Session("SearchVariable") <> "FINDALL" Or txtSearch.Text <> "" Then txtSearch_TextChanged(Nothing, Nothing)
        'gridView.DataBind()
        SavePersistentOrderChangesToDB()

        'Session("GridData") = LoadData()
        'ASPxGridView1.DataSource = Session("GridData")
        'If Session("GridData") IsNot Nothing Then
        '    If Session("OrderDetails") IsNot Nothing Then
        '        ' Session("OrderDetails")
        '        Dim DT2 As DataTable = Session("OrderDetails")
        '        For Each row1 As DataRow In DT2.Rows
        '            Dim prodSearch As DataRow = DirectCast(Session("GridData"), DataTable).Rows.Find(row1("Product_No"))
        '            If prodSearch IsNot Nothing Then
        '                prodSearch("Quantity") = row1("QTY")
        '            End If
        '        Next

        '    End If
        'End If
        ASPxGridView1.DataBind()





    End Sub
End Class