﻿Imports DevExpress.Web

Public Class ProductSearch
    Inherits System.Web.UI.Page
    Public DetailGrid As ASPxGridView
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.Page.User.Identity.IsAuthenticated Then
            FormsAuthentication.RedirectToLoginPage()
        End If
        If Session("GlobalQty") Is Nothing Then
            Session("GlobalQty") = 0
        End If
        'Dim lbl As Object = Page.Master.FindControl("Counter")
        'lbl.Value = Session("GlobalQty")

        ' GridViewFeaturesHelper.SetupGlobalGridViewBehavior(grid)
        ' MasterGrid.SettingsDetail.AllowOnlyOneMasterRowExpanded = True 'chkSingleExpanded.Checked
        ' If MasterGrid.SettingsDetail.AllowOnlyOneMasterRowExpanded Then
        ' ' MasterGrid.DetailRows.CollapseAllRows()
        '  End If
        ' ASPxImage1.ClientSideEvents.Click = "BarcodePopUp.Show"
        If Session("CurrentAction") = "Search" Then
            Session("CurrentAction") = ""
            Try
                Response.Redirect(My.Settings.Root + "ProductsForOrder.aspx")
            Catch ex As Exception
                DevExpress.Web.ASPxWebControl.RedirectOnCallback(My.Settings.Root + "ProductsForOrder.aspx")
            End Try

        End If

        Session("SearchVariable") = ""
        If Session("ShipTo") Is Nothing Or Session("ShipTo") = "" Or Session("ShipDate") Is Nothing Then Response.Redirect(My.Settings.Root + "Options.aspx")
        If Session("PreviousPage") = My.Settings.Root + "ProductSearch.aspx" Then
            Session("DivisionCode") = ""
        End If
        Session("PreviousPage") = My.Settings.Root + "OrderHeader.aspx"
        If Not (IsPostBack) Then

            '************ Compare and update

            vwToeWebProductsClass.FillByColumnArrayAndValueArray({"ToeUser"}, {Session("CurrentUser")})


            MasterGrid.DataSource = GetDistinctMasterGrid(vwToeWebProductsClass.dt)
            'Detail.SelectCommand = "SELECT * FROM [class] inner join Div_class on Class.Classcode = div_class.classcode inner join Division on Division.Divcode = Div_class.DivCode"

            MasterGrid.DataBind()
        Else
            ' If Session("Pre") Then

            If (Session("DivisionCode") <> String.Empty) Then
                    'Detail.SelectCommand = "SELECT * FROM [class] inner join Div_class on Class.Classcode = div_class.classcode inner join Division on Division.Divcode = Div_class.DivCode WHERE Division.divcode = '" + Session("DivisionCode") & "'"
                    vwToeWebProductsClass.FillByColumnArrayAndValueArray({"ToeUser", "DivCode"}, {Session("CurrentUser"), Session("DivisionCode")})
                Else
                    vwToeWebProductsClass.FillByColumnArrayAndValueArray({"ToeUser"}, {Session("CurrentUser")})
                    'Detail.SelectCommand = "SELECT * FROM [class] inner join Div_class on Class.Classcode = div_class.classcode inner join Division on Division.Divcode = Div_class.DivCode"
                End If

                MasterGrid.DataSource = GetDistinctMasterGrid(vwToeWebProductsClass.dt)
                MasterGrid.DataBind()
            End If

            Dim dv As DataView
        vwToeWebProductsButtons.FillByColumnArrayAndValueArray({"ToeUser"}, {Session("CurrentUser")})
        'dv = CType(DropDown.Select(DataSourceSelectArguments.Empty), DataView)

        ' dv = CType(, DataView)
        Dim columnNames() As String = {"DivDesc", "DivSortIndex", "DivCode"}

        Dim dtDistinct As DataTable = vwToeWebProductsButtons.dt.DefaultView.ToTable(True, columnNames)

        'String[] columnNames = {"column1", "column2"}

        ' D 'ataTable dtDistinct = dt.DefaultView.ToTable(True, columnNames);

        Dim dr() As DataRow = dtDistinct.Select("", "DivSortIndex asc")


        For Each item As DataRow In dr 'dtDistinct.Rows
            Dim x As New ASPxButton
            x.Font.Size = 22
            x.Font.Bold = True
            x.Font.Name = "Arial"
            x.Border.BorderColor = System.Drawing.Color.LightGray
            x.Text = item("DivDesc")
            x.Attributes("data") = item("DivCode").ToString
            AddHandler x.Click, AddressOf ClickFunction
            'x.Margin = New Paddings(10, 0, 10, 0)
            x.Attributes("style") = "margin-left:10px;margin-right:10px"
            x.Height = 70
            x.BackColor = System.Drawing.Color.LightGray
            x.ForeColor = System.Drawing.Color.Black
            MenuDiv.Controls.Add(x)

        Next


    End Sub
    Function GetDistinctMasterGrid(dt As DataTable) As DataTable
        If dt Is Nothing Then Return Nothing
        Dim columnNamesMasterGrid() As String = {"ClassDesc", "ClassCode", "DivCode"}

        Dim dtMasterGrid As DataTable = dt.DefaultView.ToTable(True, columnNamesMasterGrid)
        Return dtMasterGrid
    End Function

    Function GetDistinctDetailGrid(dt As DataTable) As DataTable
        If dt Is Nothing Then Return Nothing
        Dim FilteredDT() As DataRow = dt.Select("ClassCode='" & Session("ClassCode") & "'")
        'If FilteredDT.Count <> 0 Then
        '    sender.DataSource = GetDistinctDetailGrid(FilteredDT.CopyToDataTable())
        'End If

        Dim columnNamesMasterGrid() As String = {"LineDesc", "ProductLine"}

        Dim dtMasterGrid As DataTable = FilteredDT.CopyToDataTable.DefaultView.ToTable(True, columnNamesMasterGrid)
        Return dtMasterGrid
    End Function
    Sub ClickFunction(ByVal sender As Object, ByVal e As Object)
        'MsgBox(sender.text)
        Session("DivisionCode") = sender.Attributes("data")
        MasterGrid_CustomCallback(MasterGrid, Nothing)
        For Each ctrl In MenuDiv.Controls
            If ctrl.GetType.ToString = "DevExpress.Web.ASPxButton" Then
                ctrl.BackColor = System.Drawing.Color.LightGray
                ctrl.ForeColor = System.Drawing.Color.Black
            End If
        Next
        Dim btn As ASPxButton = sender
        btn.BackColor = System.Drawing.ColorTranslator.FromHtml("#1b458f")
        btn.ForeColor = System.Drawing.Color.White
    End Sub

    Protected Sub DetailGrid_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        'Session("ClassCode") = CType(sender, ASPxGridView).GetMasterRowKeyValue()
        '' Session("DivisionCode") = categoryChooser.Text
        sender.Settings.ShowColumnHeaders = False
        ''Dim grid As ASPxGridView = sender
        ''grid.DataBind()
        'If DetailGrid Is Nothing Then DetailGrid = sender
        ''Dim grid As ASPxGridView = MasterGrid
        'If (Session("DivisionCode") <> String.Empty) Then
        '    vwToeWebProductsClass.FillByColumnArrayAndValueArray({"ToeUser", "DivCode"}, {Session("CurrentUser"), Session("DivisionCode")})

        'Else
        '    vwToeWebProductsClass.FillByColumnArrayAndValueArray({"ToeUser"}, {Session("CurrentUser")})
        'End If
        ''Dim FilteredDT() As DataRow = vwToeWebProductsForDetailClass.dt.Select("ClassCode='" & Session("ClassCode") & "'")
        ''If FilteredDT.Count <> 0 Then
        ''    sender.DataSource = GetDistinctDetailGrid(FilteredDT.CopyToDataTable())
        ''    'sender.DataBind()
        ''End If

        ''Session("SelectCommand") = Detail.SelectCommand
        ''sender.DataBind()
    End Sub

    Private Sub ASPxGridView1_DetailRowExpandedChanged(sender As Object, e As ASPxGridViewDetailRowEventArgs) Handles MasterGrid.DetailRowExpandedChanged




    End Sub


    Private Sub Master_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs)

    End Sub

    Protected Sub MasterGrid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs)

        Dim grid As ASPxGridView = sender
        If (Session("DivisionCode") <> String.Empty) Then
            'Session("DivisionCode") = categoryChooser.Text
            'Detail.SelectCommand = "SELECT * FROM [Class] WHERE Description = '" + categoryChooser.Text & "'"
            vwToeWebProductsClass.FillByColumnArrayAndValueArray({"ToeUser", "DivCode"}, {Session("CurrentUser"), Session("DivisionCode")})
        Else
            vwToeWebProductsClass.FillByColumnArrayAndValueArray({"ToeUser"}, {Session("CurrentUser")})
        End If
        grid.DataSource = GetDistinctMasterGrid(vwToeWebProductsClass.dt)
        'Session("SelectCommand") = Detail.SelectCommand
        grid.DataBind()
    End Sub

    Private Sub MasterGrid_HtmlRowPrepared(sender As Object, e As ASPxGridViewTableRowEventArgs) Handles MasterGrid.HtmlRowPrepared
        If (e.RowType <> GridViewRowType.Data) Then Return
        ' Int budget = Convert.ToInt32(e.GetValue("Budget"))
        ' If (budget < 100000) Then
        e.Row.BackColor = System.Drawing.Color.White 'System.Drawing.ColorTranslator.FromHtml("#FFFFFF") ''#99CCFF") ''System.Drawing.Color.FromHex={CE,E7,FF}

    End Sub

    Protected Sub DetailGrid_CustomButtonCallback(sender As Object, e As ASPxGridViewCustomButtonCallbackEventArgs)
        Dim CurrentSelectedItem = sender.GetRowValues(e.VisibleIndex, "ProductLine")
        Session("SelectedProductLine") = CurrentSelectedItem
        CurrentSelectedItem = sender.GetRowValues(e.VisibleIndex, "LineDesc")
        Session("SelectedClass") = CurrentSelectedItem
        Session("GridData") = Nothing
        'Session("DivisionCode") = categoryChooser.Text
        DevExpress.Web.ASPxWebControl.RedirectOnCallback(My.Settings.Root + "ProductsForOrder.aspx")
        '' Response.RedirectLocation = "~/ProductsForOrder.aspx"

    End Sub

    Protected Sub DetailGrid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs)
        Dim dGrid As ASPxGridView = sender
        'dGrid.
        Dim CurrentSelectedItem = sender.GetRowValues(e.Parameters, "ProductLine")
        Session("SelectedProductLine") = CurrentSelectedItem
        CurrentSelectedItem = sender.GetRowValues(e.Parameters, "LineDesc")
        Session("SelectedClass") = CurrentSelectedItem
        Session("GridData") = Nothing
        'Session("DivisionCode") = categoryChooser.Text
        DevExpress.Web.ASPxWebControl.RedirectOnCallback(My.Settings.Root + "ProductsForOrder.aspx")
        '' Response.RedirectLocation = "~/ProductsForOrder.aspx"

    End Sub

    Protected Sub txtSearch_TextChanged(sender As Object, e As EventArgs)

        If txtSearch.Text = "" Then Session("SearchVariable") = "" : Exit Sub
        Session("SearchVariable") = txtSearch.Text
        Session("CurrentAction") = "Search"
        Try
            Response.Redirect(My.Settings.Root + "ProductsForOrder.aspx")
        Catch ex As Exception
            DevExpress.Web.ASPxWebControl.RedirectOnCallback(My.Settings.Root + "ProductsForOrder.aspx")
        End Try


    End Sub

    Protected Sub btnOKBarcode_Click(sender As Object, e As EventArgs)

        ''**** GET BARCODE RESULT FROM SPAN
        'Dim lbl As Object = Page.FindControl("dbr")
        ''lbl.Value = Session("GlobalQty")
        'MsgBox(lbl.Attributes("textContent"))
        ''lbl.Attributes("data-count") = Session("GlobalQty")
        'BarcodePopUp.ShowOnPageLoad = False

    End Sub

    Protected Sub DetailGrid_Init(sender As Object, e As EventArgs)

        DetailGrid = sender
        'Dim s = MasterGrid.GetRowValues(MasterGrid.FocusedRowIndex, "ClassCode").ToString()
        Session("ClassCode") = CType(sender, ASPxGridView).GetMasterRowKeyValue()
        'Dim FilteredDT() As DataRow = vwToeWebProductsForDetailClass.dt.Select("ClassCode='" & Session("ClassCode") & "'")
        'If FilteredDT.Count <> 0 Then
        '    sender.DataSource = GetDistinctDetailGrid(FilteredDT.CopyToDataTable())
        '    sender.DataBind()
        'End If
    End Sub

    Protected Sub DetailGrid_DataBinding(sender As Object, e As EventArgs)
        'If e.Expanded = False Then Exit Sub
        'Session("ClassCode") = CType(MasterGrid, ASPxGridView).GetMasterRowKeyValue()
        Session("ClassCode") = 0
        Dim grid As ASPxGridView = sender
        If DetailGrid IsNot Nothing Then Session("ClassCode") = CType(DetailGrid, ASPxGridView).GetMasterRowKeyValue()
        If Session("ClassCode") = 0 Then Session("ClassCode") = CType(DetailGrid, ASPxGridView).GetMasterRowKeyValue()
        If (Session("ClassCode") <> 0) Then
            'Session("DivisionCode") = categoryChooser.Text
            vwToeWebProductsForDetailClass.FillByColumnArrayAndValueArray({"ToeUser", "ClassCode"}, {Session("CurrentUser"), Session("ClassCode")})
        Else
            vwToeWebProductsForDetailClass.FillByColumnArrayAndValueArray({"ToeUser"}, {Session("CurrentUser")})
        End If
        ''Session("SelectCommand") = Detail.SelectCommand
        'If DetailGrid IsNot Nothing Then
        sender.DataSource = GetDistinctDetailGrid(vwToeWebProductsForDetailClass.dt)
        '    DetailGrid.DataBind()

        'End If
    End Sub

    Protected Sub cb_Callback(source As Object, e As CallbackEventArgs)
        Session("CurrentAction") = "Search"

        'If txtSearch.Text = "" Then Session("SearchVariable") = "" : Exit Sub
        Session("SearchVariable") = e.Parameter
        'Response.Redirect(My.Settings.Root + "ProductsForOrder.aspx")
        DevExpress.Web.ASPxWebControl.RedirectOnCallback(My.Settings.Root + "ProductsForOrder.aspx")
    End Sub


    'Private Sub btnFruits_Click(sender As Object, e As EventArgs) Handles btnFruits.Click
    '    Session("DivisionCode") = "1"
    '    MasterGrid_CustomCallback(MasterGrid, Nothing)
    'End Sub

    'Private Sub btnVegetables_Click(sender As Object, e As EventArgs) Handles btnVegetables.Click
    '    Session("DivisionCode") = "2"
    '    MasterGrid_CustomCallback(MasterGrid, Nothing)
    'End Sub

    'Private Sub btnRoots_Click(sender As Object, e As EventArgs) Handles btnRoots.Click
    '    Session("DivisionCode") = "5"
    '    MasterGrid_CustomCallback(MasterGrid, Nothing)
    'End Sub

    'Private Sub btnGroceries_Click(sender As Object, e As EventArgs) Handles btnGroceries.Click
    '    Session("DivisionCode") = "7"
    '    MasterGrid_CustomCallback(MasterGrid, Nothing)
    'End Sub

    'Private Sub btnOther_Click(sender As Object, e As EventArgs) Handles btnOther.Click
    '    Session("DivisionCode") = "4"
    '    MasterGrid_CustomCallback(MasterGrid, Nothing)
    'End Sub
End Class