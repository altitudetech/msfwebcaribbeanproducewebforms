﻿<%@ Page Language="vb" MasterPageFile="~/Root.master" AutoEventWireup="false" CodeBehind="ProductsForOrder.aspx.vb" Inherits="CaribbeanProduceWebForms.ProductsForOrder" %>


<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <script lang="javascript">
        $("body").keydown(function () {
            if (event.keyCode == 13) {
                document.activeElement.blur();
                return false;
            }
        });

    </script>
    <style type="text/css">
        div.scrollmenu {
            background-color: white;
            overflow: auto;
            white-space: nowrap;
            width: 600px;
            height: 100px;
            align-content: center;
            align-items: center;
            margin: auto;
        }

            div.scrollmenu a {
                display: inline-block;
                color: white;
                text-align: center;
                padding: 14px;
                text-decoration: none;
            }

                div.scrollmenu a:hover {
                    background-color: #777;
                }
        /*#scrolly{
           
             width: 400px; 
  overflow-x: scroll(or auto);
            /*height: 190px;*/
        /*overflow: auto;
            overflow-y: hidden;
            margin: 0 auto;
            white-space: nowrap*/


        /*img{
            width: 300px;
            height: 150px;
            margin: 20px 10px;
            display: inline;
        }*/
        .confirm_selection {
            animation: glow .5s infinite alternate;
        }

        @keyframes glow {
            to {
                text-shadow: 0 0 10px red;
            }
        }

        .confirm_selection {
            font-family: sans-serif;
            font-size: 36px;
            font-weight: bold;
        }
    </style>
    <dx:ASPxPopupControl ID="BarcodePopUp" ClientInstanceName="BarcodePopUp" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server" HeaderText="Lector de Barcodes" Text="Favor de fijar el barcode a la vista de la camara" Width="500px" Height="600px">

        <ClientSideEvents PopUp="function(s, e) { btnGo.SetEnabled(true);
                btnGo.ClientEnabled
                          LoadCamera();
               
                btnOkBarcode.SetEnabled(true);
                      }" />
        <ContentCollection>
            <dx:PopupControlContentControl>
                <script src="jquery-1.11.3.min.js"></script>
                <%--<h1>Victor's Barcode Reader - TEST!!</h1>--%>
                <div>Resultado de Lectura: <span id="dbr" class="confirm_selection"></span></div>
                <div class="select">
                    <label for="videoSource">Fuente de Video: </label>
                    <select id="videoSource"></select>
                </div>
                <%--<button id="go" style="width:300px;height:40px">Leer Barcode</button>--%>
                <dx:ASPxButton ClientInstanceName="btnGo" ClientEnabled="false" Style="margin: auto; align-content: center; align-items: center; align-self: center" ID="buttonGo" HorizontalAlign="Center" runat="server" Text="Leer Barcode" AutoPostBack="false">
                    <ClientSideEvents Click="function () {
    if (isPC) {
        canvas.style.display = 'none';
    } else {
        mobileCanvas.style.display = 'none';
    }

    isPaused = false;
    scanBarcode();
   
}" />
                </dx:ASPxButton>
                <div>
                    <video muted autoplay id="video"></video>
                    <canvas id="pcCanvas" width="640" height="480" style="display: none; float: bottom;"></canvas>
                    <canvas id="mobileCanvas" width="240" height="320" style="display: none; float: bottom;"></canvas>
                </div>
                <dx:ASPxButton ClientInstanceName="btnOkBarcode" ID="btnOKBarcode" Style="margin: auto; align-content: center; align-items: center; align-self: center" HorizontalAlign="Center" runat="server" Text="Ok!" AutoPostBack="false">
                    <ClientSideEvents Click="function () { 
                            
                            BarcodePopUp.Hide();
                            var res = document.getElementById('dbr');
                           txtSearch.SetText(res.textContent);
                            btnOkBarcode.SetEnabled(false);
                            btnGo.SetEnabled(false);
                                 cb.PerformCallback(res.textContent)
                            }" />
                </dx:ASPxButton>
                <%-- <dx:ASPxButton ID="noButton" runat="server" Text="No" AutoPostBack="false">
                            <ClientSideEvents Click="OnClickNo" />
                        </dx:ASPxButton>--%>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <table style="width: 630px; align-content: center; align-items: center; align-self: center; margin: auto">
        <tr>
            <td align="center" style="width: 70px">

                <p align="left">
                    <dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Images/barcode.png" ShowLoadingImage="true" Height="100px">
                        <ClientSideEvents Click="function(s, e) {
                        BarcodePopUp.Show();
                      }" />
                    </dx:ASPxImage>

                </p>
            </td>
            <td align="center">

                <p align="Right">
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" ShowCollapseButton="false" ShowHeader="false" Width="360px">
                        <PanelCollection>
                            <dx:PanelContent>
                                <dx:ASPxTextBox runat="server" ClientInstanceName="txtSearch" AutoPostBack="true" Font-Size="22px" ID="txtSearch2" Width="100%" NullText="Busqueda" NullTextDisplayMode="UnfocusedAndFocused" OnTextChanged="txtSearch2_TextChanged">
                                    <ClientSideEvents TextChanged="function (s, e) { cb.PerformCallback(s.GetText()); }"
                                        />

                                    <NullTextStyle>
                                    </NullTextStyle>
                                </dx:ASPxTextBox>
                                <dx:ASPxCallback ID="cb" ClientInstanceName="cb" runat="server" OnCallback="cb_Callback">
                                    <ClientSideEvents CallbackComplete="function (s, e) { txtSearch.SetText(e.result); }" />
                                </dx:ASPxCallback>
                            </dx:PanelContent>
                        </PanelCollection>

                    </dx:ASPxRoundPanel>

                </p>
            </td>
            <td>
                <dx:ASPxButton ID="btnClearFilters"  UseSubmitBehavior="false" AllowFocus="false" Width="16px"  RenderMode = "Button" Text = "" 
                     BackColor="Transparent" Border-BorderColor="Transparent"
                    runat="server" style="vertical-align:top" ShowLoadingImage="true" AutoPostBack="False" >
                    <Image Url="~/Images/close-button.png" Width="30px">
                    </Image>


<Border BorderColor="Transparent"></Border>
                </dx:ASPxButton>
                <%--<img class="dxeImage_iOS " id="ctl04" src="images/close-button.png" alt="" style="width:30px;height:30px;vertical-align:top">--%>
            </td>
        </tr>
    </table>
    <p style="align-self: center; align-content: center; align-items: center">
        <div class="scrollmenu" runat="server" id="MenuDiv">
        </div>



    </p>

    <div style="max-width: 630px; margin: 0 auto;">


        <%--<p align="center">--%>
        <%--<h2 class="form-signin-heading" style="text-align: left; background-color: #2182C9; color: white; height: 60px; vertical-align: central; text-align: center; padding-top: 10px;"><%=Session("SelectedClass") %></h2>--%>
        <%--</p>--%>
    </div>
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" Font-Names="Arial" runat="server" HeaderText="" ShowCollapseButton="true" Width="100%" HorizontalAlign="Center" Theme="iOS" View="GroupBox" ShowHeader="False">

        

        <PanelCollection>
            <dx:PanelContent runat="server">
                <dx:ASPxGridView ID="ASPxGridView1"  ClientInstanceName="GridView" Width="630px" Font-Names="Arial" runat="server" Theme="iOS" AutoGenerateColumns="False" KeyFieldName="product_no" OnCustomButtonCallback="ASPxGridView1_CustomButtonCallback">
                   <Styles BatchEditModifiedCell-BackColor ="Red" BatchEditModifiedCell-ForeColor="White"
                        
                       ></Styles>
                    <ClientSideEvents CustomButtonClick="function(s, e) {
                        GridView.UpdateEdit();
                         GridCB.PerformCallback(e.visibleIndex);
                        }
                        " />

                    <SettingsPager Mode="ShowPager" ShowNumericButtons="True" PageSize="9">
                        <Summary Visible="False" />
                    </SettingsPager>
                    <Settings ShowStatusBar="Hidden" />
                    <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="product_no" Width="80px" ShowInCustomizationForm="True" VisibleIndex="0" CellStyle-HorizontalAlign="Left" Caption="Producto" ReadOnly="true">
                            <CellStyle HorizontalAlign="Left"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="description" Width="200px" ShowInCustomizationForm="True" VisibleIndex="1" CellStyle-HorizontalAlign="Left" Caption="Descripcion" ReadOnly="true">
                            <CellStyle HorizontalAlign="Left"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <%--  <dx:GridViewDataTextColumn FieldName="unitofmeasure" ShowInCustomizationForm="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="divCode" ShowInCustomizationForm="True" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="divCode2" ShowInCustomizationForm="True" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="DivDescription" ShowInCustomizationForm="True" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="classCode" ShowInCustomizationForm="True" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="classCode2" ShowInCustomizationForm="True" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="classDescription" ShowInCustomizationForm="True" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="linenumber" ShowInCustomizationForm="True" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="prodline" ShowInCustomizationForm="True" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>--%>
                        <%-- <dx:GridViewDataTextColumn FieldName="Quantity" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataSpinEditColumn FieldName="Quantity" Caption="Cantidad" VisibleIndex="3" Width="30px">
                            <PropertiesSpinEdit MinValue="0" MaxValue="10000" NumberType="Integer" />
                        </dx:GridViewDataSpinEditColumn>
                        <dx:GridViewDataTextColumn Caption="UM" FieldName="UnitOfMeasure" ShowInCustomizationForm="True" Width="4px" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <%-- <dx:GridViewCommandColumn ButtonType="Image" Caption="IMG" VisibleIndex="10">
                                                    <CustomButtons>
                                                        <dx:GridViewCommandColumnCustomButton Image-Height="20" Image-Url="~/images/add-list1.png" Image-Width="20" Visibility="AllDataRows">
                                                            <Image Height="30px" Width="30px">
                                                            </Image>
                                                        </dx:GridViewCommandColumnCustomButton>
                                                    </CustomButtons>
                                                </dx:GridViewCommandColumn>--%>
                        <%-- <dx:GridViewCommandColumn VisibleIndex="10" Caption=" " ButtonType="Image" Width="5px">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton ID="Add" Visibility="AllDataRows" Image-Width="20" Image-Url="~/images/AddItem-CPE.png"
                                    Image-Height="20">
                                    <Image Height="20px" Width="20px">
                                    </Image>
                                    <Styles>
                                        <Style HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </Style>
                                    </Styles>
                                </dx:GridViewCommandColumnCustomButton>
                            </CustomButtons>
                            <HeaderStyle BackColor="White" Border-BorderColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                            <CellStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle">
                            </CellStyle>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewCommandColumn VisibleIndex="11" Caption=" " ButtonType="Image">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton ID="Remove" Visibility="AllDataRows" Image-Width="20px" Image-Url="~/images/RemoveItem-CPE.png"
                                    Image-Height="20">
                                    <Image Height="20px" Width="20px" />
                                </dx:GridViewCommandColumnCustomButton>
                            </CustomButtons>
                            <HeaderStyle BackColor="White" Border-BorderColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                            <CellStyle Wrap="True">
                            </CellStyle>
                        </dx:GridViewCommandColumn>--%>
                        <dx:GridViewCommandColumn Caption=" ">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton  Image-Height="20" Image-Url="~/images/Actualizar.png" Image-Width="20" Visibility="AllDataRows">
                                                            <Image Height="30px" Width="100px">
                                                            </Image>
                                    
                                                        </dx:GridViewCommandColumnCustomButton>
                            
                            </CustomButtons>
                              
                        </dx:GridViewCommandColumn>
                        <%-- <dx:GridViewDataTextColumn Caption=" " ShowInCustomizationForm="True" VisibleIndex="4" ReadOnly="true">
                             <DataItemTemplate>
                                    <dx:AspxButton ID="ASPxButton2" runat="server" 
                                        CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" 
                                        SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css" Text="Actualizar" AutoPostBack="False" OnClick="ASPxButton2_Click" >
                                    </dx:AspxButton>
                                    </DataItemTemplate>
                        </dx:GridViewDataTextColumn>--%>
                    </Columns>
                    <SettingsEditing Mode="Batch" >
                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                    </SettingsEditing>
                </dx:ASPxGridView>
                <%--                    <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" ShowCollapseButton="true" Width="400px" Theme="MaterialCompact" View="GroupBox" ShowHeader="False" HorizontalAlign="Center">


                        <PanelCollection>
                            <dx:PanelContent runat="server">

                                <button runat="server" class="btn btn-lg btn-success col-xs-12" id="btnAddToOrdker">
                                    <span class="glyphicon glyphicon-add pull-left"></span>Agregar a Orden</button>


                                <button runat="server" class="btn btn-lg btn-danger col-xs-12" id="btnCancel">
                                    <span class="glyphicon glyphicon-cancel pull-left"></span>Cancelar</button>

                            </dx:PanelContent>
                        </PanelCollection>


                    </dx:ASPxRoundPanel>--%>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>

    <dx:ASPxCallback ID="GridCallbackPanel" ClientInstanceName="GridCB" runat="server" OnCallback="GridCallbackPanel_Callback">
                                    <%--<ClientSideEvents CallbackComplete="function (s, e) { txtSearch.SetText(e.result); }" />--%>
                                </dx:ASPxCallback>

    <script async src="zxing.js"></script>
    <script src="video.js"></script>


</asp:Content>