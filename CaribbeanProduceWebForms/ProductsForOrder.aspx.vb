﻿Imports System.Data.SqlClient
Imports DevExpress.Web
Imports DevExpress.Web.Data

Public Class ProductsForOrder
    Inherits System.Web.UI.Page
    Public ClassDt As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.User.Identity.IsAuthenticated Then
            FormsAuthentication.RedirectToLoginPage()
        End If
        If Session("GlobalQty") Is Nothing Then
            Session("GlobalQty") = 0

        End If
        'Dim lbl As Object = Page.Master.FindControl("Counter")
        'lbl.Value = Session("GlobalQty")

        'If Not (IsPostBack) Then
        '    LoadData()
        'End If
        If Session("ShipTo") Is Nothing Or Session("ShipTo") = "" Or Session("ShipDate") Is Nothing Then Response.Redirect(My.Settings.Root + "Options.aspx")
        Session("PreviousPage") = My.Settings.Root + "OrderHeader.aspx"
        CheckForOrdersDatatable()
        If Session("GlobalQty") Is Nothing Then Session("GlobalQty") = 0

        If Session("GridData") Is Nothing Then
            Session("GridData") = LoadData()
        End If

        ' ASPxGridView1.DataSource = Session("GridData")
        'Dim lbl As HtmlGenericControl = Page.Master.FindControl("itemcount")


        'lbl.Attributes("data-count") = Session("GlobalQty")
        If Not IsPostBack AndAlso Not IsCallback Then
            If Session("SearchVariable") <> "" Then
                If Session("SearchVariable") <> "FINDALL" Then txtSearch2.Text = Session("SearchVariable")
                txtSearch2_TextChanged(Nothing, Nothing)
            End If

            ' Session("GridData") = LoadData()



        End If
        If Session("GridData") IsNot Nothing Then
            If Session("OrderDetails") IsNot Nothing Then
                ' Session("OrderDetails")
                Dim DT As DataTable = Session("OrderDetails")
                For Each row As DataRow In DT.Rows
                    Dim prodSearch As DataRow = DirectCast(Session("GridData"), DataTable).Rows.Find(row("Product_No"))
                    If prodSearch IsNot Nothing Then
                        prodSearch("Quantity") = row("QTY")
                    End If
                    DirectCast(Session("GridData"), DataTable).AcceptChanges()
                Next

            End If
        End If
        ASPxGridView1.DataSource = Session("GridData")
        ASPxGridView1.DataBind()


        Dim dv As DataView
        vwToeWebProductsButtons.FillByColumnArrayAndValueArray({"ToeUser"}, {Session("CurrentUser")})
        'dv = CType(DropDown.Select(DataSourceSelectArguments.Empty), DataView)

        ' dv = CType(, DataView)
        Dim columnNames() As String = {"DivDesc", "DivSortIndex", "DivCode"}

        Dim dtDistinct As DataTable = vwToeWebProductsButtons.dt.DefaultView.ToTable(True, columnNames)

        'String[] columnNames = {"column1", "column2"}

        ' D 'ataTable dtDistinct = dt.DefaultView.ToTable(True, columnNames);

        Dim dr() As DataRow = dtDistinct.Select("", "DivSortIndex asc")


        For Each item As DataRow In dr 'dtDistinct.Rows
            Dim x As New ASPxButton
            x.Font.Size = 22
            x.Font.Bold = True
            x.Font.Name = "Arial"
            x.Border.BorderColor = System.Drawing.Color.LightGray
            x.Text = item("DivDesc")
            x.Attributes("data") = item("DivCode").ToString
            AddHandler x.Click, AddressOf ClickFunction
            'x.Margin = New Paddings(10, 0, 10, 0)
            x.Attributes("style") = "margin-left:10px;margin-right:10px"
            x.Height = 70
            x.BackColor = System.Drawing.Color.LightGray
            x.ForeColor = System.Drawing.Color.Black
            MenuDiv.Controls.Add(x)

        Next



    End Sub
    Sub ClickFunction(ByVal sender As Object, ByVal e As Object)
        'MsgBox(sender.text)
        Session("DivisionCode") = sender.Attributes("data")
        'MasterGrid_CustomCallback(MasterGrid, Nothing)
        LoadData()



        ASPxGridView1.DataSource = Session("GridData")

        If Session("GridData") IsNot Nothing Then
            If Session("OrderDetails") IsNot Nothing Then
                ' Session("OrderDetails")
                Dim DT As DataTable = Session("OrderDetails")
                For Each row As DataRow In DT.Rows
                    Dim prodSearch As DataRow = DirectCast(Session("GridData"), DataTable).Rows.Find(row("Product_No"))
                    If prodSearch IsNot Nothing Then
                        prodSearch("Quantity") = row("QTY")
                    End If
                    DirectCast(Session("GridData"), DataTable).AcceptChanges()
                Next

            End If
        End If
        ASPxGridView1.DataBind()

        For Each ctrl In MenuDiv.Controls
            If ctrl.GetType.ToString = "DevExpress.Web.ASPxButton" Then
                ctrl.BackColor = System.Drawing.Color.LightGray
                ctrl.ForeColor = System.Drawing.Color.Black
            End If
        Next
        Dim btn As ASPxButton = sender
        btn.BackColor = System.Drawing.ColorTranslator.FromHtml("#1b458f")
        btn.ForeColor = System.Drawing.Color.White
    End Sub

    Function LoadData() As DataTable
        ASPxGridView1.PageIndex = 0
        '  Dim constr As String = "Data Source=70.35.204.129,1475;Initial Catalog=msfservercaribbeanproducetest;Persist Security Info=True;User ID=sa;Password=Itg2016;" 'ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Dim constr As String = "Data Source=" & My.Settings.DatabaseServer & ";Initial Catalog=" & My.Settings.DatabaseName & ";Persist Security Info=True;User ID=" & My.Settings.DbUserName & ";Password=" & My.Settings.DbPassword & ";" 'ConfigurationManager.ConnectionStrings("constr").ConnectionString

        Dim Qry As String = "SELECT      distinct  product as product_no, ProdDesc as description, '0' AS Quantity, UM as UnitOfMeasure,UPC  
                             FROM            vwToeWebProducts Where ToeUser='" & Session("CurrentUser") & "' and shipto='" & Session("ShipTo") & "'"
        If Session("SearchVariable") <> "" And Session("SelectedProductLine") = "" Then
            If Session("SearchVariable") <> "FINDALL" Then
                Qry += " and (Product like '%" & Session("SearchVariable") & "%' or ProdDesc like '%" & Session("SearchVariable") & "%' or UPC like '%" & Session("SearchVariable") & "%')"
                'txtSearch2.Text =
                Session("SelectedProductLine") = ""
            Else
                Session("SelectedProductLine") = ""
            End If
        Else
            If Session("SelectedProductLine") IsNot Nothing Then Qry += " and ProductLine = '" & Session("SelectedProductLine") & "'"

        End If

        If Session("DivisionCode") <> "" Then
            Qry += " and DivCode='" & Session("DivisionCode") & "'"
        End If


        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(Qry)



                cmd.CommandType = CommandType.Text

                cmd.Connection = con
                con.Open()
                ClassDt = New DataTable
                Using dad As New SqlDataAdapter(cmd.CommandText, con)
                    dad.Fill(ClassDt)
                End Using
                con.Close()
                Dim SumQty As Integer
                Dim tempDt As DataTable = DirectCast(Session("OrderDetails"), DataTable)
                If tempDt.Rows.Count > 0 Then
                    For I = 0 To tempDt.Rows.Count - 1
                        If I = tempDt.Rows.Count Then Exit For
                        SumQty += tempDt.Rows(I)("Qty")
                    Next
                End If
                Session("GlobalQty") = SumQty
                DirectCast(Session("WS"), wsToeWeb).SetCounter(Session("GlobalQty"))

                If ClassDt.Rows.Count > 0 Then
                    Dim primaryKey(1) As DataColumn
                    primaryKey(0) = ClassDt.Columns("product_no")
                    ClassDt.PrimaryKey = primaryKey
                    Session("GridData") = ClassDt
                    Return ClassDt

                End If

                ' Convert.ToInt32(cmd.ExecuteScalar())
                'con.Close()

            End Using

        End Using

        Session("GridData") = Nothing
        Return Nothing

        'ASPxGridView1.DataSource = ClassDt
        'ASPxGridView1.DataBind()
    End Function

    Private Sub ASPxGridView1_RowUpdating(sender As Object, e As ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim row As DataRow = DirectCast(Session("GridData"), DataTable).Rows.Find(e.Keys("product_no"))
        row("Quantity") = e.NewValues("Quantity")
        row.AcceptChanges()

        DirectCast(Session("GridData"), DataTable).AcceptChanges()
        'row("Age") = e.NewValues("Age")
        e.Cancel = True



        Dim gridView As DevExpress.Web.ASPxGridView = DirectCast(ASPxGridView1, DevExpress.Web.ASPxGridView)

        If Session("GlobalQty") Is Nothing Then Session("GlobalQty") = 0

        Dim CurrentSelectedItemKey = e.Keys("product_no")
        Dim KeyIndex As String = gridView.FindVisibleIndexByKeyValue(CurrentSelectedItemKey)
        Dim CurrentSelectedItemQty As Integer = e.NewValues("Quantity") 'sender.GetRowValues(KeyIndex, "Quantity")
        Dim CurrentSelectedItem = sender.GetRowValues(KeyIndex, "description")
        Dim CurrentSelectedItemUM As String = sender.GetRowValues(KeyIndex, "UnitOfMeasure")



        Dim s As DevExpress.Web.ASPxGridView



        Dim dataTable As DataTable = Session("GridData")
        ' Dim _row As DataRow = dataTable.Rows.Find(CurrentSelectedItemKey)

        '_row(2) = CurrentSelectedItemQty



        '**** Verify Item In OrderDetail Datatable
        Dim Dt = Session("OrderDetails")
        Dim OrdRow As DataRow = Dt.Rows.Find(CurrentSelectedItemKey)
        If OrdRow IsNot Nothing Then
            OrdRow(2) = CurrentSelectedItemQty
            If CurrentSelectedItemQty = 0 Then
                OrdRow.Delete()
            End If
        Else

            OrdRow = Dt.NewRow
            OrdRow("Order_No") = ""
            OrdRow("Product_No") = CurrentSelectedItemKey
            OrdRow("Qty") = CurrentSelectedItemQty
            OrdRow("Description") = CurrentSelectedItem

            OrdRow("Unit_Price") = 0
            OrdRow("Ext_Price") = 0
            OrdRow("UM") = CurrentSelectedItemUM

            Dt.Rows.Add(OrdRow)
        End If
        Dt.AcceptChanges()



        Dim SumQty = 0

        For I = 0 To Dt.Rows.Count - 1

            SumQty += CInt(Dt.Rows(I)("Qty").ToString)


        Next
        If Session("WS") Is Nothing Then
            Session("WS") = New wsToeWeb    'Master.Pages.Root.webServiceCounter
        End If
        DirectCast(Session("WS"), wsToeWeb).SetCounter(SumQty)

        '***** Clear DB Order Values and Save all Again


        'sender.DataSource = Session("GridData")

        '*** REAPPLY FILTERS
        If Session("SearchVariable") <> "FINDALL" Or txtSearch2.Text <> "" Then txtSearch2_TextChanged(Nothing, Nothing)
        'gridView.DataBind()
        SavePersistentOrderChangesToDB()

        'Session("GridData") = LoadData()
        'ASPxGridView1.DataSource = Session("GridData")
        'If Session("GridData") IsNot Nothing Then
        '    If Session("OrderDetails") IsNot Nothing Then
        '        ' Session("OrderDetails")
        '        Dim DT2 As DataTable = Session("OrderDetails")
        '        For Each row1 As DataRow In DT2.Rows
        '            Dim prodSearch As DataRow = DirectCast(Session("GridData"), DataTable).Rows.Find(row1("Product_No"))
        '            If prodSearch IsNot Nothing Then
        '                prodSearch("Quantity") = row1("QTY")
        '            End If
        '        Next

        '    End If
        'End If
        ASPxGridView1.DataBind()








    End Sub

    Protected Sub ASPxGridView1_CustomButtonCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs)
        'ASPxGridView1.UpdateEdit()
        If Session("GlobalQty") Is Nothing Then Session("GlobalQty") = 0
        Dim CurrentSelectedItem = sender.GetRowValues(e.VisibleIndex, "description")
        Dim CurrentSelectedItemKey = sender.GetRowValues(e.VisibleIndex, "product_no")
        Dim CurrentSelectedItemQty As Integer = sender.GetRowValues(e.VisibleIndex, "Quantity")
        Dim CurrentSelectedItemUM As String = sender.GetRowValues(e.VisibleIndex, "UnitOfMeasure")
        'Session("SelectedClass") = CurrentSelectedItem

        'If e.ButtonID = "Add" Then
        '    CurrentSelectedItemQty += 1
        '    Session("GlobalQty") += 1
        'ElseIf e.ButtonID = "Remove" Then
        '    If CurrentSelectedItemQty > 0 Then CurrentSelectedItemQty -= 1
        '    Session("GlobalQty") -= 1
        'End If

        'DirectCast(Session("WS"), wsToeWeb).SetCounter(Session("GlobalQty"))


        Dim s As DevExpress.Web.ASPxGridView

        Dim gridView As DevExpress.Web.ASPxGridView = DirectCast(sender, DevExpress.Web.ASPxGridView)

        Dim dataTable As DataTable = Session("GridData")
        Dim row As DataRow = dataTable.Rows.Find(CurrentSelectedItemKey)

        row(2) = CurrentSelectedItemQty



        '**** Verify Item In OrderDetail Datatable
        Dim Dt = Session("OrderDetails")
        Dim OrdRow As DataRow = Dt.Rows.Find(CurrentSelectedItemKey)
        If OrdRow IsNot Nothing Then
            OrdRow(2) = CurrentSelectedItemQty
            If CurrentSelectedItemQty = 0 Then
                OrdRow.Delete()
            End If
        Else

            OrdRow = Dt.NewRow
            OrdRow("Order_No") = ""
            OrdRow("Product_No") = CurrentSelectedItemKey
            OrdRow("Qty") = CurrentSelectedItemQty
            OrdRow("Description") = CurrentSelectedItem

            OrdRow("Unit_Price") = 0
            OrdRow("Ext_Price") = 0
            OrdRow("UM") = CurrentSelectedItemUM

            Dt.Rows.Add(OrdRow)
        End If
        Dt.AcceptChanges()



        Dim SumQty = 0

        For I = 0 To Dt.Rows.Count - 1

            SumQty += CInt(Dt.Rows(I)("Qty").ToString)


        Next
        If Session("WS") Is Nothing Then
            Session("WS") = New wsToeWeb    'Master.Pages.Root.webServiceCounter
        End If
        DirectCast(Session("WS"), wsToeWeb).SetCounter(SumQty)

        '***** Clear DB Order Values and Save all Again


        'sender.DataSource = Session("GridData")

        '*** REAPPLY FILTERS
        If Session("SearchVariable") <> "FINDALL" Or txtSearch2.Text <> "" Then txtSearch2_TextChanged(Nothing, Nothing)
        sender.DataBind()
        SavePersistentOrderChangesToDB()




        ' Dim lbl As Object = Page.Master.FindControl("itemcount")

        ';var spn = document.getElementById('itemcount');

        'spn.setAttribute('data-count', $title.text());

        'lbl.Value = Session("GlobalQty")

        'lbl.Attributes("data-count") = Session("GlobalQty")

        '//Set the value to CONTENT PAGE label control.  
        'lbl.Attributes("data-count") = Session("GlobalQty")\\\


        'Dim TestScript As String = " 
        'var spn = document.getElementById('itemcount');

        'spn.setAttribute('data-count', '" & Session("GlobalQty") & "' );"
        'Page.ClientScript.RegisterStartupScript(Me.Page.GetType(), "CallMyFunction", TestScript, True)

        'ClientScript.RegisterClientScriptBlock(Page.GetType, "DisableTimer", TestScript)
        ''System.Web.UI.ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Script", "setCartQty(" & Session("GlobalQty") & ")", True)
        'System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page.Master, Page.Master.GetType(), TestScript, True)
        ' System.Web.UI.ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Script", "$('#itemcount').attr('data-count',  " & Session("GlobalQty").ToString & ");", True)
    End Sub

    Public Sub SavePersistentOrderChangesToDB()
        Dim Order As New com.data.ToeWebUserOrders
        Order.DeleteByColumnAndID("Username", Session("CurrentUser"))
        Dim Dt = Session("OrderDetails")
        For Each DR As DataRow In Dt.Rows
            Order = New com.data.ToeWebUserOrders
            Order.Order_No = DR("Order_No").ToString
            Order.Product_No = DR("Product_No").ToString
            Order.QTY = DR("Qty").ToString
            Order.Description = DR("Description").ToString

            Order.Unit_Price = DR("Unit_Price").ToString
            Order.Ext_Price = DR("Ext_Price").ToString
            Order.UM = DR("UM").ToString
            Order.Username = Session("CurrentUser")
            Order.ShipDate = Session("ShipDate")
            Order.ShipTo = Session("ShipTo")
            Order.ShipToName = Session("ShipToName")
            Order.Update()
            Order.Dispose()
        Next
    End Sub


    Public Sub CheckForOrdersDatatable()
        If Session("OrderDetails") Is Nothing Then


            Dim dt As New DataTable
            'dt.Columns.Add(New DataColumn("ID"))
            dt.Columns.Add(New DataColumn("Order_No"))
            dt.Columns.Add(New DataColumn("Product_No"))
            dt.Columns.Add(New DataColumn("QTY"))
            dt.Columns.Add(New DataColumn("Unit_Price"))
            dt.Columns.Add(New DataColumn("Ext_Price"))
            dt.Columns.Add(New DataColumn("UM"))
            dt.Columns.Add(New DataColumn("Description"))
            dt.Columns.Add(New DataColumn("UPC"))
            Dim primaryKey(1) As DataColumn
            primaryKey(0) = dt.Columns("Product_No")
            dt.PrimaryKey = primaryKey
            Session("OrderDetails") = dt
            'For i = 1 To 10
            '    Dim dRow As DataRow
            '    dRow = dt.NewRow
            '    dRow("FechaDeOrden") = Now.ToShortDateString
            '    dRow("NumeroDeOrden") = i
            '    dt.Rows.Add(dRow)
            '    dt.AcceptChanges()
            'Next i


        End If
    End Sub

    Protected Sub txtSearch2_TextChanged(sender As Object, e As EventArgs)
        Session("DivisionCode") = ""
        If Session("GridData") IsNot Nothing And txtSearch2.Text = "" And Session("SelectedProductLine") = "" Then
            Session("SearchVariable") = "FINDALL"
            Session("SelectedProductLine") = ""
            Session("SelectedClass") = ""

            Session("GridData") = LoadData()
            ASPxGridView1.DataSource = Session("GridData") : ASPxGridView1.DataBind()
        ElseIf Session("GridData") IsNot Nothing And txtSearch2.Text <> "" Then
            Session("SelectedClass") = ""
            Session("SearchVariable") = txtSearch2.Text '"FINDALL"
            Session("SelectedProductLine") = ""
            Session("GridData") = LoadData()
            ASPxGridView1.DataSource = Session("GridData") : ASPxGridView1.DataBind()
        End If
        If txtSearch2.Text = "" Then
            ASPxGridView1.DataSource = Session("GridData")
            ASPxGridView1.DataBind()
            If Session("GridData") Is Nothing Then LoadData()
            If Session("OrderDetails") IsNot Nothing Then
                    ' Session("OrderDetails")
                    Dim DT3 As DataTable = Session("OrderDetails")
                    For Each row As DataRow In DT3.Rows
                        Dim prodSearch As DataRow = DirectCast(Session("GridData"), DataTable).Rows.Find(row("Product_No"))
                        If prodSearch IsNot Nothing Then
                            prodSearch("Quantity") = row("QTY")
                        End If
                    Next

                End If
                Exit Sub
            End If

            Dim dt As DataTable = Session("GridData")

        Dim dt2 As New DataTable
        dt2.Columns.Add(New DataColumn("product_no"))
        dt2.Columns.Add(New DataColumn("description"))
        dt2.Columns.Add(New DataColumn("Quantity"))
        dt2.Columns.Add(New DataColumn("UnitOfMeasure"))
        dt2.Columns.Add(New DataColumn("UPC"))
        Dim primaryKey(1) As DataColumn
        primaryKey(0) = dt2.Columns("product_No")
        dt2.PrimaryKey = primaryKey
        If dt IsNot Nothing Then
            For Each row As DataRow In dt.Select("product_no like '%" & txtSearch2.Text & "%' or description like '%" & txtSearch2.Text & "%' or UPC like '%" & txtSearch2.Text & "%'")
                dt2.ImportRow(row)
            Next
        End If
        If Session("OrderDetails") IsNot Nothing Then
            ' Session("OrderDetails")
            Dim DT3 As DataTable = Session("OrderDetails")
            For Each row As DataRow In DT3.Rows
                Dim prodSearch As DataRow = DirectCast(dt2, DataTable).Rows.Find(row("Product_No"))
                If prodSearch IsNot Nothing Then
                    prodSearch("Quantity") = row("QTY")
                End If
            Next

        End If
        Session("SearchVariable") = "FINDALL"
        Session("SelectedProductLine") = ""
        ASPxGridView1.DataSource = dt2 'dt.Select("product_no like '%" & txtSearch2.Text & "%' or description like '%" & txtSearch2.Text & "%'")
        ASPxGridView1.DataBind()
        'Session("GridData") = dt2
    End Sub

    Protected Sub cb_Callback(source As Object, e As DevExpress.Web.CallbackEventArgs)

        ' If txtSearch2.Text = "" Then Session("SearchVariable") = "" : Exit Sub
        Session("SearchVariable") = e.Parameter 'txtSearch2.Text
        'Response.Redirect(My.Settings.Root + "ProductsForOrder.aspx")

        'txtSearch2_TextChanged(Nothing, Nothing)
        DevExpress.Web.ASPxWebControl.RedirectOnCallback(My.Settings.Root + "ProductsForOrder.aspx")
    End Sub

    Protected Sub GridCallbackPanel_Callback(source As Object, e As CallbackEventArgs)

    End Sub

    Protected Sub btnClearFilters_Click(sender As Object, e As EventArgs) Handles btnClearFilters.Click
        Session("SearchVariable") = "FINDALL"
        txtSearch2.Text = ""
        For Each ctrl In MenuDiv.Controls
            If ctrl.GetType.ToString = "DevExpress.Web.ASPxButton" Then
                ctrl.BackColor = System.Drawing.Color.LightGray
                ctrl.ForeColor = System.Drawing.Color.Black
            End If
        Next
        Session("DivisionCode") = ""
        txtSearch2_TextChanged(Nothing, Nothing)

    End Sub

    Private Sub ASPxGridView1_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs) Handles ASPxGridView1.HtmlDataCellPrepared
        If (e.DataColumn.FieldName = "Quantity") Then

            If (e.CellValue > 0) Then
                e.Cell.ForeColor = System.Drawing.Color.Black
                e.Cell.BackColor = System.Drawing.Color.LightGreen
            End If
        End If
    End Sub

    'Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs)
    '    Dim dGrid As ASPxGridView = ASPxGridView1
    '    'dGrid.
    '    'Dim CurrentSelectedItem = sender.GetRowValues(dGrid.FocusedRowIndex, "ProductLine")


    '    If Session("GlobalQty") Is Nothing Then Session("GlobalQty") = 0
    '    Dim CurrentSelectedItem = dGrid.GetRowValues(dGrid.FocusedRowIndex, "description")
    '    Dim CurrentSelectedItemKey = dGrid.GetRowValues(dGrid.FocusedRowIndex, "product_no")
    '    Dim CurrentSelectedItemQty As Integer = dGrid.GetRowValues(dGrid.FocusedRowIndex, "Quantity")
    '    Dim CurrentSelectedItemUM As String = dGrid.GetRowValues(dGrid.FocusedRowIndex, "UnitOfMeasure")
    '    'Session("SelectedClass") = CurrentSelectedItem

    '    'If e.ButtonID = "Add" Then
    '    '    CurrentSelectedItemQty += 1
    '    '    Session("GlobalQty") += 1
    '    'ElseIf e.ButtonID = "Remove" Then
    '    '    If CurrentSelectedItemQty > 0 Then CurrentSelectedItemQty -= 1
    '    '    Session("GlobalQty") -= 1
    '    'End If

    '    DirectCast(Session("WS"), wsToeWeb).SetCounter(Session("GlobalQty"))


    '    Dim s As DevExpress.Web.ASPxGridView

    '    Dim gridView As DevExpress.Web.ASPxGridView = DirectCast(sender, DevExpress.Web.ASPxGridView)

    '    Dim dataTable As DataTable = Session("GridData")
    '    Dim row As DataRow = dataTable.Rows.Find(CurrentSelectedItemKey)

    '    row(2) = CurrentSelectedItemQty



    '    '**** Verify Item In OrderDetail Datatable
    '    Dim Dt = Session("OrderDetails")
    '    Dim OrdRow As DataRow = Dt.Rows.Find(CurrentSelectedItemKey)
    '    If OrdRow IsNot Nothing Then
    '        OrdRow(2) = CurrentSelectedItemQty
    '        If CurrentSelectedItemQty = 0 Then
    '            OrdRow.Delete()
    '        End If
    '    Else

    '        OrdRow = Dt.NewRow
    '        OrdRow("Order_No") = ""
    '        OrdRow("Product_No") = CurrentSelectedItemKey
    '        OrdRow("Qty") = CurrentSelectedItemQty
    '        OrdRow("Description") = CurrentSelectedItem

    '        OrdRow("Unit_Price") = 0
    '        OrdRow("Ext_Price") = 0
    '        OrdRow("UM") = CurrentSelectedItemUM

    '        Dt.Rows.Add(OrdRow)
    '    End If
    '    Dt.AcceptChanges()


    '    '***** Clear DB Order Values and Save all Again


    '    'sender.DataSource = Session("GridData")

    '    '*** REAPPLY FILTERS
    '    If Session("SearchVariable") <> "FINDALL" Or txtSearch2.Text <> "" Then txtSearch2_TextChanged(Nothing, Nothing)
    '    sender.DataBind()
    '    SavePersistentOrderChangesToDB()



    'End Sub
End Class