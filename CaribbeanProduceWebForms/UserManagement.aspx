﻿<%@ Page Language="vb" MasterPageFile="~/Root.master" AutoEventWireup="false" CodeBehind="UserManagement.aspx.vb" Inherits="CaribbeanProduceWebForms.UserManagement" %>


<%@ Register Assembly="DevExpress.Web.Bootstrap.v17.1, Version=17.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>



<asp:content id="Content" contentplaceholderid="Content" runat="server">

    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Historial de Ordenes" ShowCollapseButton="true" Width="100%" HorizontalAlign="Center" Theme="iOS" View="GroupBox" ShowHeader="False">


        <PanelCollection>
            <dx:PanelContent runat="server">
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Theme="iOS" KeyFieldName="username" DataSourceID="SqlDataSource1">
                    <Columns>
                        <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="username" VisibleIndex="1" ReadOnly="False" Caption="Usuario">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="pass"  VisibleIndex="2" Caption="Contraseña" PropertiesTextEdit-Password="true" >
<PropertiesTextEdit Password="True" ClientInstanceName="psweditor">
						</PropertiesTextEdit>
					<%--	<EditItemTemplate>
							<dx:ASPxTextBox ID="pswtextbox" runat="server" Text='<%#Bind("pass")%>'
								Visible='<%#ASPxGridView1.IsNewRowEditing%>' Password="True">
								<ClientSideEvents Validation="function(s,e){e.isValid = s.GetText()>5;}" />
							</dx:ASPxTextBox>
							<asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="popup.ShowAtElement(this); return false;" Visible='<%#Not ASPxGridView1.IsNewRowEditing%>'>Edit password</asp:LinkButton>
						</EditItemTemplate>--%>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="name" VisibleIndex="3" Caption="Nombre">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="lastname" VisibleIndex="4" Caption="Apellido">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="phone" VisibleIndex="5" Caption="Telefono">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="email" VisibleIndex="6" Caption="E-Mail">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataComboBoxColumn FieldName="role" VisibleIndex="7" Caption="Rol">
                            <PropertiesComboBox DataSourceID="sqlRoles" TextField="description" ValueField="roleid">
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataTextColumn Caption="ID Usuario" FieldName="userid" ShowInCustomizationForm="True" VisibleIndex="8">
                        </dx:GridViewDataTextColumn>
                    </Columns>




                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="DetailGrid" OnRowUpdating="DetailGrid_RowUpdating" OnRowInserting="DetailGrid_RowInserting" OnBeforePerformDataSelect="DetailGrid_BeforePerformDataSelect" runat="server" AutoGenerateColumns="False" EnablePagingGestures="False" EnableTheming="True" Theme="iOS" Width="100%" DataSourceID="UserCustomers" KeyFieldName="id">
                                <%--<ClientSideEvents RowClick="function(s, e) {s.PerformCallback(e.visibleIndex);}" />
                                    <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />--%>
                                <SettingsPager EnableAdaptivity="true" />
                                <Columns>


                                    <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>


                                    <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="username" VisibleIndex="2" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="customer_no" VisibleIndex="3" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataComboBoxColumn FieldName="ship_to" VisibleIndex="4" Caption="Ship To">
                                        <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="name" ValueField="ship_to">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>


                                </Columns>
                                <Styles Header-Wrap="True">
                                    <Header Wrap="True">
                                    </Header>
                                </Styles>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="UserCustomers" runat="server" ConflictDetection="CompareAllValues"
                                ConnectionString="<%$ ConnectionStrings:msfservercaribbeanproducetestConnectionString2 %>"
                                DeleteCommand="DELETE FROM [toewebuser_customer] WHERE [id] = @original_id AND (([username] = @original_username) 
                                    OR ([username] IS NULL AND @original_username IS NULL)) 
                                    AND (([customer_no] = @original_customer_no) OR ([customer_no] IS NULL AND @original_customer_no IS NULL))
                                    AND (([ship_to] = @original_ship_to) OR ([ship_to] IS NULL AND @original_ship_to IS NULL))"
                                InsertCommand="INSERT INTO [toewebuser_customer] ( [username], [customer_no], [ship_to]) VALUES ( @SelUsername, @customer_no, @ship_to)"
                                OldValuesParameterFormatString="original_{0}"
                                SelectCommand="SELECT * FROM [toewebuser_customer] WHERE ([username] = @username)"
                                UpdateCommand="UPDATE [toewebuser_customer] SET [username] = @username, [customer_no] = @customer_no, [ship_to] = @ship_to WHERE [id] =
                                    @original_id AND (([username] = @original_username) OR ([username] IS NULL AND @original_username IS NULL)) AND 
                                    (([customer_no] = @original_customer_no) OR ([customer_no] IS NULL AND @original_customer_no IS NULL)) AND 
                                    (([ship_to] = @original_ship_to) OR ([ship_to] IS NULL AND @original_ship_to IS NULL))">
                                <DeleteParameters>
                                    <asp:Parameter Name="original_id" Type="Object" />
                                    <asp:Parameter Name="original_username" Type="String" />
                                    <asp:Parameter Name="original_customer_no" Type="String" />
                                    <asp:Parameter Name="original_ship_to" Type="String" />

                                </DeleteParameters>
                                <InsertParameters>
                                    <%--<asp:Parameter Name="id" Type="Object" DbType="Guid" />--%>
                                    <asp:Parameter Name="username" Type="String" />
                                    <asp:Parameter Name="customer_no" Type="String" />
                                    <asp:Parameter Name="ship_to" Type="String" />
                                    <asp:SessionParameter Name="SelUsername" SessionField="SelectedUsername" Type="String" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:SessionParameter Name="username" SessionField="SelectedUsername" Type="String" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="username" Type="String" />
                                    <asp:Parameter Name="customer_no" Type="String" />
                                    <asp:Parameter Name="ship_to" Type="String" />
                                    <asp:Parameter Name="original_id" Type="Object" />
                                    <asp:Parameter Name="original_username" Type="String" />
                                    <asp:Parameter Name="original_customer_no" Type="String" />
                                    <asp:Parameter Name="original_ship_to" Type="String" />

                                </UpdateParameters>
                            </asp:SqlDataSource>
                        </DetailRow>
                    </Templates>
                    <SettingsDetail AllowOnlyOneMasterRowExpanded="true" ShowDetailRow="true" />


                </dx:ASPxGridView>




                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:msfservercaribbeanproducetestConnectionString2 %>" DeleteCommand="DELETE FROM [toewebusers] WHERE [username] = @original_username AND (([pass] = @original_pass) OR ([pass] IS NULL AND @original_pass IS NULL)) AND (([name] = @original_name) OR ([name] IS NULL AND @original_name IS NULL)) AND (([lastname] = @original_lastname) OR ([lastname] IS NULL AND @original_lastname IS NULL)) AND (([phone] = @original_phone) OR ([phone] IS NULL AND @original_phone IS NULL)) AND (([email] = @original_email) OR ([email] IS NULL AND @original_email IS NULL)) AND (([role] = @original_role) OR ([role] IS NULL AND @original_role IS NULL))  AND (([userid] = @original_userid) OR ([userid] IS NULL AND @original_userid IS NULL))" InsertCommand="INSERT INTO [toewebusers] ([username], [pass], [name], [lastname], [phone], [email], [role], [userid]) VALUES (@username, @pass, @name, @lastname, @phone, @email, @role, @userid)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [toewebusers]" UpdateCommand="UPDATE [toewebusers] SET [pass] = @pass, [name] = @name, [lastname] = @lastname, [phone] = @phone, [email] = @email, [role] = @role, [userid] = @userid WHERE [username] = @original_username AND (([pass] = @original_pass) OR ([pass] IS NULL AND @original_pass IS NULL)) AND (([name] = @original_name) OR ([name] IS NULL AND @original_name IS NULL)) AND (([lastname] = @original_lastname) OR ([lastname] IS NULL AND @original_lastname IS NULL)) AND (([phone] = @original_phone) OR ([phone] IS NULL AND @original_phone IS NULL)) AND (([email] = @original_email) OR ([email] IS NULL AND @original_email IS NULL)) AND (([role] = @original_role) OR ([role] IS NULL AND @original_role IS NULL))  AND (([userid] = @original_userid) OR ([userid] IS NULL AND @original_userid IS NULL))">
                    <DeleteParameters>
                        <asp:Parameter Name="original_username" Type="String" />
                        <asp:Parameter Name="original_pass" Type="String" />
                        <asp:Parameter Name="original_name" Type="String" />
                        <asp:Parameter Name="original_lastname" Type="String" />
                        <asp:Parameter Name="original_phone" Type="String" />
                        <asp:Parameter Name="original_email" Type="String" />
                        <asp:Parameter Name="original_role" Type="String" />
                        <asp:Parameter Name="original_userid" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="username" Type="String" />
                        <asp:Parameter Name="pass" Type="String" />
                        <asp:Parameter Name="name" Type="String" />
                        <asp:Parameter Name="lastname" Type="String" />
                        <asp:Parameter Name="phone" Type="String" />
                        <asp:Parameter Name="email" Type="String" />
                        <asp:Parameter Name="role" Type="String" />
                        <asp:Parameter Name="userid" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="pass" Type="String" />
                        <asp:Parameter Name="name" Type="String" />
                        <asp:Parameter Name="lastname" Type="String" />
                        <asp:Parameter Name="phone" Type="String" />
                        <asp:Parameter Name="email" Type="String" />
                        <asp:Parameter Name="role" Type="String" />
                        <asp:Parameter Name="userid" Type="String" />
                        <asp:Parameter Name="original_username" Type="String" />
                        <asp:Parameter Name="original_pass" Type="String" />
                        <asp:Parameter Name="original_name" Type="String" />
                        <asp:Parameter Name="original_lastname" Type="String" />
                        <asp:Parameter Name="original_phone" Type="String" />
                        <asp:Parameter Name="original_email" Type="String" />
                        <asp:Parameter Name="original_role" Type="String" />
                        <asp:Parameter Name="original_userid" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>

                <asp:SqlDataSource ID="sqlRoles" runat="server" ConnectionString="<%$ ConnectionStrings:msfservercaribbeanproducetestConnectionString2 %>" SelectCommand="SELECT * FROM [toewebuserroles]"></asp:SqlDataSource>


                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:msfservercaribbeanproducetestConnectionString2 %>" SelectCommand="SELECT DISTINCT [ship_to], [customer_no], [name] FROM [shipto] WHERE ([status] = @status) order by Name asc">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="A" Name="status" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
    <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" HeaderText="Edit password" Width="307px" ClientInstanceName="popup">
				<ContentCollection>
					<dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
						<table>
							<tr>
								<td>Enter new password:</td>
								<td>
									<dx:ASPxTextBox ID="npsw" runat="server" Password="True" ClientInstanceName="npsw">
										<ClientSideEvents Validation="function(s, e) {e.isValid = (s.GetText().length&gt;5)}" />
										<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="The password lengt should be more that 6 symbols">
										</ValidationSettings>
									</dx:ASPxTextBox>
								</td>
							</tr>
							<tr>
								<td>Confirm new password:</td>
								<td>
									<dx:ASPxTextBox ID="cnpsw" runat="server" Password="True" ClientInstanceName="cnpsw">
										<ClientSideEvents Validation="function(s, e) {e.isValid = (s.GetText() == npsw.GetText());}" />
										<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="The password is incorrect">
										</ValidationSettings>
									</dx:ASPxTextBox>
								</td>
							</tr>
						</table>
						<dx:ASPxButton ID="confirmButton" runat="server" Text="Ok" AutoPostBack="False" OnClick="confirmButton_Click">
						</dx:ASPxButton>

					</dx:PopupControlContentControl>
				</ContentCollection>
			</dx:ASPxPopupControl>
</asp:content>
