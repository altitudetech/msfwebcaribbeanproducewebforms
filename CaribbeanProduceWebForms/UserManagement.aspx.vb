﻿Imports DevExpress.Web
Imports DevExpress.Web.Data

Public Class UserManagement
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        '*** GET USER ROLE
        Dim dt As DataTable = oDataManagement.Execute("Select Role from Toewebusers where username = '" & Session("CurrentUser") & "'")





        If dt.Rows.Count > 0 Then
            If dt.Rows(0)("Role") <> "D" Then Response.Redirect(My.Settings.Root + "Options.aspx")
            'AJCO - se cambio el Previous Page Login.aspx, para funcionalidad de Logout.

            Session("PreviousPage") = My.Settings.Root + "login.aspx"
        Else
            Response.Redirect(My.Settings.Root + "login.aspx")
        End If




        'If Session("userdata") Is Nothing Then

        '    Session("userdata") = dt
        'Else
        '    dt = TryCast(Session("userdata"), DataTable)
        'End If

        'ASPxGridView1.DataSource = dt
        'ASPxGridView1.DataBind()




    End Sub

    Protected Sub DetailGrid_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("SelectedUsername") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    Protected Sub DetailGrid_RowUpdating(sender As Object, e As Data.ASPxDataUpdatingEventArgs)
        '*** Capturar Record and Change Customer_NO
        Dim dt As DataTable = oDataManagement.Execute("Select dISTINCT customer_no From shipto where ship_to = '" & e.NewValues("ship_to").ToString & "'")
        If dt.Rows.Count > 0 Then e.NewValues("customer_no") = dt.Rows(0)("customer_no").ToString
    End Sub

    Protected Sub DetailGrid_RowInserting(sender As Object, e As Data.ASPxDataInsertingEventArgs)
        '*** Capturar Record and Change Customer_NO
        Dim dt As DataTable = oDataManagement.Execute("Select dISTINCT customer_no From shipto where ship_to = '" & e.NewValues("ship_to").ToString & "'")
        If dt.Rows.Count > 0 Then e.NewValues("customer_no") = dt.Rows(0)("customer_no").ToString

    End Sub

    Protected Sub confirmButton_Click(ByVal sender As Object, ByVal e As EventArgs)
        UpdatePasswordField(cnpsw.Text)
        ASPxPopupControl1.ShowOnPageLoad = False
    End Sub

    Protected Sub UpdatePasswordField(ByVal newPassword As String)
        Dim index As Integer = ASPxGridView1.EditingRowVisibleIndex
        'SqlDataSource1.Data
        Dim dt As DataTable = TryCast(Session("data"), DataTable)
        dt.Rows(index)("Password") = newPassword
        Session("data") = dt

    End Sub

    Private Sub ASPxGridView1_RowUpdating(sender As Object, e As ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating

        If e.NewValues("pass") = "" Or e.NewValues("pass") Is Nothing Then
            e.NewValues("pass") = e.OldValues("pass")
        End If
        'e.Cancel = True
        'Dim index As Integer = DirectCast(sender, ASPxGridView).EditingRowVisibleIndex
        'Dim dt As DataTable = TryCast(Session("data"), DataTable)
        'dt.Rows(index)("ID") = e.NewValues("ID")
        'dt.Rows(index)("Username") = e.NewValues("Username")
        'Session("userdata") = dt
        'DirectCast(sender, ASPxGridView).CancelEdit()
    End Sub
End Class