﻿<%@ Page Language="vb" MasterPageFile="~/Root.master" AutoEventWireup="false" CodeBehind="OrderHeader.aspx.vb" Inherits="CaribbeanProduceWebForms.OrderHeader" %>


<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <br />
    <br />
    <br />
    <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" HeaderText=" " ShowCollapseButton="true" Width="100%" HorizontalAlign="Center" Theme="iOS" View="GroupBox" ShowHeader="False">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <table style="margin-top: 150px">
                    <tr>
                        <td style="width: 100px">
                            <img height="72px" width="80px" src="Images/calendar.png" />
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="ShipDate" runat="server" Theme="iOS" Width="500px" Height="80px" ClientEnabled="True" Font-Size="20pt" HorizontalAlign="Center" NullText="Seleccionar Fecha de Entrega">
                                <clientsideevents Init="function(s, e) {
	
                                     ASPxClientUtils.AttachEventToElement(s.GetInputElement(), 'click', function (event) {
                s.ShowDropDown();
            });
}" 
                                    
                                    />

                            </dx:ASPxDateEdit>
                        </td>
                    </tr>

                </table>
                <br />
                <br />
                <br />
                <br />
                <table>
                    <tr>
                        <td style="width: 100px">
                            <img height="65px" src="Images/store.png" style="padding-right: 15px" />

                        </td>
                        <td>
                            <dx:ASPxComboBox ID="ShipTo" runat="server"  OnSelectedIndexChanged="ShipTo_SelectedIndexChanged1"  Theme="iOS" Width="500px"  Height="80px" Font-Size="20pt" HorizontalAlign="Center" NullText="Seleccionar Tienda" NullTextDisplayMode="Unfocused">
                                <Columns>
                                    <dx:ListBoxColumn FieldName="customer_no" Visible="False">
                                    </dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="ship_to" Visible="False">
                                    </dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="name" Caption="Tienda">
                                    </dx:ListBoxColumn>
                                </Columns>
                            </dx:ASPxComboBox>
                         <%--   <dx:ASPxComboBox ID="ShipTo" runat="server" AutoPostBack="true" Theme="iOS" Width="500px"  OnValueChanged="ShipTo_ValueChanged1" OnSelectedIndexChanged="ShipTo_SelectedIndexChanged1" Height="80px" Font-Size="20pt" HorizontalAlign="Center" NullText="Seleccionar Tienda" NullTextDisplayMode="Unfocused">
                                <Columns>
                                    <dx:ListBoxColumn FieldName="customer_no" Visible="False">
                                    </dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="ship_to" Visible="False">
                                    </dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="name" Caption="Tienda">
                                    </dx:ListBoxColumn>
                                </Columns>
                            </dx:ASPxComboBox>--%>
                        </td>
                    </tr>

                </table>
                <br />
                <br />
                <br />
                <br />
                <%--     <br />

                <br />--%>

                <dx:ASPxButton ID="btnContinue" runat="server" Text="CONTINUAR" Width="200px" Height="80px" Font-Size="20pt" BackColor="#1b458f" AutoPostBack="false"></dx:ASPxButton>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>

    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:msfservercaribbeanproducetestConnectionString %>" SelectCommand="select customer_no, ship_to, name from shipto where customer_no  = '000000750' and name like '%carolina%' and name like '%walmart%' group by customer_no, ship_to, name"></asp:SqlDataSource>--%>

    <dx:ASPxPopupControl ID="ASPxPopupControl1" CloseAction="None" ShowCloseButton="false" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server" Font-Size="20px" HeaderText="Confirmacion" Text="Usted desea borrar la orden o desea modificar parametros?" ClientInstanceName="popup" Width="550px">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <table style="margin-top:30px">
                    <tr>
                        <td>
                            <dx:ASPxButton Style="margin: auto; align-content: center; align-items: center; align-self: center" ID="btnModificar" HorizontalAlign="Center" Width="250px" Font-Size="18px" runat="server" Text="Modificar Orden" AutoPostBack="false">
                            </dx:ASPxButton>
                        </td>
                        <td></td>
                        <td>
                            <dx:ASPxButton Style="margin: auto; align-content: center; align-items: center; align-self: center" ID="btnBorrar" HorizontalAlign="Center" Width="250px" Font-Size="18px" runat="server" Text="Borrar Orden" AutoPostBack="false">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>


            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>

