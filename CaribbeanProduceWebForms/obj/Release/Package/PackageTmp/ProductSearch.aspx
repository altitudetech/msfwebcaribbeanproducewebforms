﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.master" CodeBehind="ProductSearch.aspx.vb" Inherits="CaribbeanProduceWebForms.ProductSearch" %>

<%@ Register Assembly="DevExpress.Web.Bootstrap.v17.1, Version=17.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>


<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
       
    <script>
        var isDetailRowExpanded = new Array();
        function OnRowClick(s, e) {
            if (isDetailRowExpanded[e.visibleIndex] != true)
                s.ExpandDetailRow(e.visibleIndex);
            else
                s.CollapseDetailRow(e.visibleIndex);
        }
        function OnDetailRowExpanding(s, e) {
            isDetailRowExpanded[e.visibleIndex] = true;
        }
        function OnDetailRowCollapsing(s, e) {
            isDetailRowExpanded[e.visibleIndex] = false;
        }
    </script>

    <style type="text/css">
        div.scrollmenu {
            background-color: white;
            overflow: auto;
            white-space: nowrap;
            width: 600px;
            height: 100px;
            align-content: center;
            align-items: center;
            margin: auto;
        }

            div.scrollmenu a {
                display: inline-block;
                color: white;
                text-align: center;
                padding: 14px;
                text-decoration: none;
            }

                div.scrollmenu a:hover {
                    background-color: #777;
                }
        /*#scrolly{
           
             width: 400px; 
  overflow-x: scroll(or auto);
            /*height: 190px;*/
        /*overflow: auto;
            overflow-y: hidden;
            margin: 0 auto;
            white-space: nowrap*/


        /*img{
            width: 300px;
            height: 150px;
            margin: 20px 10px;
            display: inline;
        }*/
        .confirm_selection {
            animation: glow .5s infinite alternate;
        }

        @keyframes glow {
            to {
                text-shadow: 0 0 10px red;
            }
        }

        .confirm_selection {
            font-family: sans-serif;
            font-size: 36px;
            font-weight: bold;
        }
    </style>
   
<%--    <asp:UpdatePanel ID="PopupUpdatePanel" runat="server">
        <ContentTemplate>--%>
         <dx:ASPxPopupControl ID="BarcodePopUp" ClientInstanceName="BarcodePopUp" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server" HeaderText="Lector de Barcodes" Text="Favor de fijar el barcode a la vista de la camara" Width="500px" Height="600px">

            <ClientSideEvents PopUp="function(s, e) { btnGo.SetEnabled(true);
                btnGo.ClientEnabled
                          LoadCamera();
               
                btnOkBarcode.SetEnabled(true);
                      }" />
            <ContentCollection>
                <dx:PopupControlContentControl>
                    <script src="jquery-1.11.3.min.js"></script>
                    <%--<h1>Victor's Barcode Reader - TEST!!</h1>--%>
                    <div>Resultado de Lectura: <span id="dbr" class="confirm_selection"></span></div>
                    <div class="select">
                        <label for="videoSource">Fuente de Video: </label>
                        <select id="videoSource"></select>
                    </div>
                    <%--<button id="go" style="width:300px;height:40px">Leer Barcode</button>--%>
                    <dx:ASPxButton ClientInstanceName="btnGo"  ClientEnabled="false"   Style="margin: auto; align-content: center; align-items: center; align-self: center" ID="buttonGo" HorizontalAlign="Center" runat="server" Text="Leer Barcode" AutoPostBack="false">
                        <ClientSideEvents Click="function () {
    if (isPC) {
        canvas.style.display = 'none';
    } else {
        mobileCanvas.style.display = 'none';
    }

    isPaused = false;
    scanBarcode();
   
}" />
                    </dx:ASPxButton>
                    <div>
                        <video muted autoplay id="video"></video>
                        <canvas id="pcCanvas" width="640" height="480" style="display: none; float: bottom;"></canvas>
                        <canvas id="mobileCanvas" width="240" height="320" style="display: none; float: bottom;"></canvas>
                    </div>
                    <dx:ASPxButton ClientInstanceName="btnOkBarcode" ID="btnOKBarcode"   Style="margin: auto; align-content: center; align-items: center; align-self: center" HorizontalAlign="Center" runat="server" Text="Ok!" AutoPostBack="false">
                        <ClientSideEvents Click="function () { 
                            
                            BarcodePopUp.Hide();
                            var res = document.getElementById('dbr');
                            txtSearch.SetText(res.textContent);
                            btnOkBarcode.SetEnabled(false);
                            btnGo.SetEnabled(false);
                            cb.PerformCallback(res.textContent)
                            }" />
                    </dx:ASPxButton>
                    <%-- <dx:ASPxButton ID="noButton" runat="server" Text="No" AutoPostBack="false">
                            <ClientSideEvents Click="OnClickNo" />
                        </dx:ASPxButton>--%>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
<%--</ContentTemplate>
    </asp:UpdatePanel>--%>

    <table style="width: 630px; margin: auto; align-content: center; align-items: center; align-self: center">
        <tr>
            <td align="center" style="width: 70px">

                <p align="left">
                    <dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Images/barcode.png" ShowLoadingImage="true" Height="100px">
                        <ClientSideEvents Click="function(s, e) {
                        BarcodePopUp.Show();
                      }" />
                    </dx:ASPxImage>

                </p>
            </td>
            <td align="center">

                <p align="Right">
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" ShowCollapseButton="false" ShowHeader="false" Width="100%">
                        <PanelCollection>
                            <dx:PanelContent>
                                <dx:ASPxTextBox runat="server" ClientInstanceName="txtSearch" ID="txtSearch" Font-Size="22px" Width="100%" NullText="Busqueda" AutoPostBack="false" NullTextDisplayMode="UnfocusedAndFocused" 
                                        OnTextChanged="txtSearch_TextChanged">
                                       <ClientSideEvents TextChanged="function (s, e) { cb.PerformCallback(''); }" />

                                    <NullTextStyle>
                                    </NullTextStyle>
                                </dx:ASPxTextBox>
                                <dx:ASPxCallback ID="cb" ClientInstanceName="cb" runat="server" OnCallback="cb_Callback">
                <%--<ClientSideEvents CallbackComplete="function (s, e) { txtName.SetText(e.result); }" />--%>
            </dx:ASPxCallback>

                            </dx:PanelContent>
                        </PanelCollection>

                    </dx:ASPxRoundPanel>

                </p>
            </td>
        </tr>
    </table>
    <p style="align-self: center; align-content: center; align-items: center">
        <div class="scrollmenu" runat="server" id="MenuDiv">
        </div>


        
        </p>
        <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" HeaderText=" " HorizontalAlign="Center" ShowCollapseButton="true" ShowHeader="False" Theme="iOS" View="GroupBox" Width="100%">
            <PanelCollection>
                <dx:PanelContent runat="server">
                    <dx:ASPxGridView ID="MasterGrid" runat="server" AutoGenerateColumns="False" ClientInstanceName="MasterGrid"  EnableCallBacks="True" EnableRowsCache="False" EnableTheming="True" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" ForeColor="Black" KeyFieldName="ClassCode" OnCustomCallback="MasterGrid_CustomCallback" Theme="iOS" Width="600px">
                        <ClientSideEvents RowClick="function(s, e) {OnRowClick(s,e);}" />
                        <Templates>
                            <DetailRow>
                                <dx:ASPxGridView ID="DetailGrid"  OnDataBinding="DetailGrid_DataBinding" OnInit="DetailGrid_Init" runat="server" AutoGenerateColumns="False"  EnablePagingGestures="False" EnableTheming="True" KeyFieldName="ProductLine" OnBeforePerformDataSelect="detailGrid_BeforePerformDataSelect" OnCustomButtonCallback="DetailGrid_CustomButtonCallback" OnCustomCallback="DetailGrid_CustomCallback" Theme="iOS" Width="100%">
                                    <ClientSideEvents RowClick="function(s, e) {s.PerformCallback(e.visibleIndex);}" />
                                    <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                    <Columns>
                                        <%--<dx:GridViewDataTextColumn FieldName="linenumber" Visible="False" VisibleIndex="0">
                                            <CellStyle Font-Bold="True" Font-Names="Segoe UI" Font-Size="12pt" HorizontalAlign="Left">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>--%><%--   <dx:GridViewDataDateColumn FieldName="lastupdate" Visible="False" VisibleIndex="2">
                                        </dx:GridViewDataDateColumn>--%><%-- <dx:GridViewDataTextColumn FieldName="divcode" Visible="False" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>--%><%-- <dx:GridViewDataTextColumn FieldName="classcode1" Visible="False" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="lastupdate1" Visible="False" VisibleIndex="5">
                                        </dx:GridViewDataDateColumn>--%>
                                        <dx:GridViewDataTextColumn FieldName="LineDesc" SortIndex="0" SortOrder="Ascending" VisibleIndex="1">
                                            <CellStyle Font-Bold="True" Font-Names="Segoe UI" Font-Size="12pt" HorizontalAlign="Left">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ProductLine" SortIndex="0" SortOrder="Ascending"  Visible="false">
                                           
                                        </dx:GridViewDataTextColumn>
                                        <%-- <dx:GridViewCommandColumn ButtonType="Image" Caption="IMG" VisibleIndex="10">
                                                    <CustomButtons>
                                                        <dx:GridViewCommandColumnCustomButton Image-Height="20" Image-Url="~/images/add-list1.png" Image-Width="20" Visibility="AllDataRows">
                                                            <Image Height="30px" Width="30px">
                                                            </Image>
                                                        </dx:GridViewCommandColumnCustomButton>
                                                    </CustomButtons>
                                                </dx:GridViewCommandColumn>--%>
                                    </Columns>
                                    <SettingsPager EnableAdaptivity="true" />
                                    <Styles Header-Wrap="True">
                                        <Header Wrap="True">
                                        </Header>
                                    </Styles>
                                </dx:ASPxGridView>
                            </DetailRow>
                        </Templates>
                        <SettingsDetail AllowOnlyOneMasterRowExpanded="true" ShowDetailRow="true" />
                        <Settings ShowColumnHeaders="False" />
                        <SettingsBehavior AllowSort="False" SortMode="DisplayText" />
                        <Columns>
                            <%--<dx:GridViewDataTextColumn FieldName="divcode" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>--%>
                            <%--  <dx:GridViewDataDateColumn FieldName="lastupdate" ShowInCustomizationForm="True" Visible="False" VisibleIndex="2">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn FieldName="divType" ShowInCustomizationForm="True" Visible="False" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>--%>
                            <dx:GridViewDataButtonEditColumn FieldName="ClassDesc" ShowInCustomizationForm="True" VisibleIndex="0">
                                <PropertiesButtonEdit>
                                    <Style HorizontalAlign="Left">
                                            </Style>
                                </PropertiesButtonEdit>
                                <CellStyle BackColor="White" Font-Bold="True" Font-Names="Segoe UI" Font-Size="12pt" HorizontalAlign="Left">
                                </CellStyle>
                            </dx:GridViewDataButtonEditColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
       <%-- <asp:SqlDataSource ID="Master" runat="server" ConnectionString="<%$ ConnectionStrings:msfservercaribbeanproducetestConnectionString %>" SelectCommand="SELECT * FROM [division]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="DropDown" runat="server" ConnectionString="<%$ ConnectionStrings:msfservercaribbeanproducetestConnectionString %>" SelectCommand="SELECT * FROM [division]"></asp:SqlDataSource>


        <asp:SqlDataSource ID="Detail" runat="server" ConnectionString="<%$ ConnectionStrings:msfservercaribbeanproducetestConnectionString %>" SelectCommand="SELECT * FROM [class] inner join Div_class on Class.Classcode = div_class.classcode inner join Division on Division.Divcode = Div_class.DivCode ">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="1" Name="DivisionCode" SessionField="DivisionCode" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="ClassSource" runat="server" ConnectionString="<%$ ConnectionStrings:msfservercaribbeanproducetestConnectionString %>" SelectCommand="Select * from class_productline where classcode = @ClassCode">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="1" Name="ClassCode" SessionField="ClassCode" />
            </SelectParameters>
        </asp:SqlDataSource>--%>


         <script async src="zxing.js"></script>
    <script src="video.js"></script>

        <p>
        </p>


    </p>

  
  

</asp:Content>
