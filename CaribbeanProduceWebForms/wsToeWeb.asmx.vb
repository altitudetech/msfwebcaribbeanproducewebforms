﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class wsToeWeb
    Inherits System.Web.Services.WebService

    <WebMethod(EnableSession:=True)>
    Public Function SetCounter(counter As String)
        HttpContext.Current.Session("GlobalQty") = counter
    End Function


    <WebMethod(EnableSession:=True)>
    Public Function GetCounter() As XmlDocument
        'If Context.Session Is Nothing Then HttpContext.Current.Session.n

        'End If
        Dim curVal = Context.Session("GlobalQty")
        If curVal Is Nothing Then SetCounter(0)
        Dim xmlDoc As XmlDocument = New XmlDocument()
        xmlDoc.LoadXml("<root><session><counter>" & Session("GlobalQty") & "</counter></session></root>")

        Return xmlDoc ' "{""counter"":" & """" & Session("GlobalQty") & """}"
    End Function

End Class